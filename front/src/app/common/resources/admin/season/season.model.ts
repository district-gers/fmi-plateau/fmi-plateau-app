import { Moment } from 'moment';

export interface SeasonDto {
  slug?: string;
  name: string;
  startDate: string;
  endDate: string;
  isCurrent: boolean;
}

export interface Season {
  slug?: string;
  name: string;
  startDate: Moment;
  endDate: Moment;
  isCurrent: boolean;
}
