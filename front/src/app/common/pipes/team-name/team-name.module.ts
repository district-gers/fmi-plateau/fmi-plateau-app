import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TeamNamePipe } from './team-name.pipe';

@NgModule({
  declarations: [TeamNamePipe],
  imports: [
    CommonModule
  ],
  exports: [TeamNamePipe],
  providers: [TeamNamePipe]
})
export class TeamNameModule { }
