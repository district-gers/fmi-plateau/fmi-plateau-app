import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminEducatorRoutingModule } from './admin-educator-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminEducatorRoutingModule
  ]
})
export class AdminEducatorModule { }
