import { Player, PlayerDto } from '@resources/admin/player/player.model';

class PlayerMapper {

  toModel(dto: PlayerDto): Player {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      firstName: dto.firstName,
      lastName: dto.lastName,
      code: dto.code,
      category: dto.category,
      club: dto.clubSlug,
      clubName: dto.clubName,
      clubShortName: dto.clubShortName,
      clubLogoUrl: dto.clubLogoUrl
    };
  }

  toDto(model: Player): PlayerDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('firstName') && {
        firstName: model.firstName
      },
      ...model.hasOwnProperty('lastName') && {
        lastName: model.lastName
      },
      ...model.hasOwnProperty('code') && {
        code: model.code
      },
      ...model.hasOwnProperty('category') && {
        category: model.category
      },
      ...model.hasOwnProperty('club') && {
        clubSlug: model.club
      }
    };
  }

}

export const playerMapper = new PlayerMapper();
