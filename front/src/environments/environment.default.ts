import { version } from '../../package.json';

export const environment = {
  application: {
    name: 'app.name',
    url: 'https://dg-fmi-plateau.appspot.com',
    version
  },
  seo: {
    default: {
      title: 'home.seo.title',
      description: 'home.seo.description',
      image: {
        url: 'home.seo.picture.url',
        description: 'home.seo.picture.description',
        width: 512,
        height: 512
      }
    },
    pageTitlePositioning: 10,
    pageTitleSeparator: ' | ',
  },
  analytics: {
    id: 'UA-163485062-1'
  },
  i18n: {
    defaultLanguage: {
      code: 'fr',
      name: 'Français',
      culture: 'fr-FR'
    },
    availableLanguages: [
      {
        code: 'fr',
        name: 'Français',
        culture: 'fr-FR'
      }
    ]
  },
  cloudFunction: {
    data: {
      url: 'https://europe-west3-dg-fmi-plateau.cloudfunctions.net/fmiPlateauData/api'
    }
  }
};
