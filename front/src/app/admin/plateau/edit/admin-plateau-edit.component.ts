import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page } from '@common/pagination/pagination';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { Day } from '@resources/admin/day/day.model';
import { DayService } from '@resources/admin/day/day.service';
import { Division } from '@resources/admin/division/division.model';
import { DivisionService } from '@resources/admin/division/division.service';
import { Group, GroupTeam } from '@resources/admin/group/group.model';
import { GroupService } from '@resources/admin/group/group.service';
import { ParticipantClub } from '@resources/admin/participant/participant.model';
import { Plateau, PlateauTeam } from '@resources/admin/plateau/plateau.model';
import { PlateauService } from '@resources/admin/plateau/plateau.service';
import { Team } from '@resources/admin/team/team.model';
import { controlValueChange$ } from '@utils/form.util';
import { tapUniqueItem } from '@utils/rxjs.util';
import { flatten, isEqual, uniqBy } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

@Component({
  selector: 'app-admin-plateau-edit',
  templateUrl: './admin-plateau-edit.component.html',
  styleUrls: ['./admin-plateau-edit.component.scss']
})
export class AdminPlateauEditComponent extends AbstractAdminEditComponent<Plateau> implements OnInit {

  formGroup: FormGroup;
  metaLabelsPrefix = 'admin.plateau.edit.seo';

  categories$: Observable<Page<Category>>;
  competitions$: Observable<Page<Competition>>;
  divisions$: Observable<Page<Division>>;
  groups$: Observable<Page<Group>>;
  days$: Observable<Page<Day>>;
  teams$: Observable<GroupTeam[]>;
  screenSize$: Observable<ScreenSize>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private responsive: ResponsiveService,
    private plateau: PlateauService,
    private category: CategoryService,
    private competition: CompetitionService,
    private division: DivisionService,
    private group: GroupService,
    private day: DayService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;

    this.formGroup = this.fb.group({
      slug: [],
      date: ['', Validators.required],
      location: [undefined, Validators.required],
      category: [undefined, Validators.required],
      competition: [undefined, Validators.required],
      division: [undefined, Validators.required],
      group: [undefined, Validators.required],
      day: [undefined, Validators.required],
      teams: [undefined, Validators.required],
      participant: [undefined, Validators.required]
    });
    this.formGroup.controls.competition.disable();
    this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();
    this.formGroup.controls.teams.disable();
    this.formGroup.controls.participant.disable();

    if (this.mode === EditMode.UPDATE) {
      this.formGroup.controls.category.disable();
    }

    const season$ = this.currentSeason.get$().pipe(take(1));

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.category = false))),
        tapUniqueItem(({ slug: category }) => category !== this.formGroup.getRawValue().category
          && this.formGroup.patchValue({ category })),
        finalize(() => this.loading.category = false));

    this.competitions$ = combineLatest([ season$, controlValueChange$<string>(this.formGroup.controls.category) ])
      .pipe(
        map(([season, categorySlug]) => ({ season, categorySlug })),
        filter(({ season, categorySlug }) => season && !!categorySlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.competition = true),
        switchMap(({ season, categorySlug }) => this.competition.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.competition = false))),
        tapUniqueItem(({ slug: competition }) => competition !== this.formGroup.getRawValue().competition
          && this.formGroup.patchValue({ competition })),
        finalize(() => this.loading.competition = false));

    this.divisions$ = combineLatest([ season$, controlValueChange$<string>(this.formGroup.controls.competition) ])
      .pipe(
        map(([season, competitionSlug]) => ({ season, competitionSlug })),
        filter(({ season, competitionSlug }) => season && !!competitionSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.division = true),
        switchMap(({ season, competitionSlug }) => this.division.find(season.slug, competitionSlug)
          .pipe(finalize(() => this.loading.division = false))),
        tapUniqueItem(({ slug: division }) => division !== this.formGroup.getRawValue().division
          && this.formGroup.patchValue({ division })),
        finalize(() => this.loading.division = false));

    this.groups$ = combineLatest([
      season$,
      controlValueChange$<string>(this.formGroup.controls.competition),
      controlValueChange$<string>(this.formGroup.controls.division)
    ])
      .pipe(
        map(([season, competitionSlug, divisionSlug]) => ({ season, competitionSlug, divisionSlug })),
        filter(({ season, competitionSlug, divisionSlug }) => season && !!competitionSlug && !!divisionSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.group = true),
        switchMap(({ season, competitionSlug, divisionSlug }) => this.group.find(season.slug, competitionSlug, divisionSlug)
          .pipe(finalize(() => this.loading.group = false))),
        tapUniqueItem(group => group?.slug !== this.formGroup.getRawValue().group?.slug
          && this.formGroup.patchValue({ group })),
        finalize(() => this.loading.group = false));

    this.days$ = combineLatest([
      season$,
      controlValueChange$<string>(this.formGroup.controls.competition),
      controlValueChange$<string>(this.formGroup.controls.division),
      controlValueChange$<Group>(this.formGroup.controls.group)
    ])
      .pipe(
        map(([season, competitionSlug, divisionSlug, group]) => ({ season, competitionSlug, divisionSlug, groupSlug: group?.slug })),
        filter(({ season, competitionSlug, divisionSlug, groupSlug }) => season && !!competitionSlug && !!divisionSlug && !!groupSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.day = true),
        switchMap(({ season, competitionSlug, divisionSlug, groupSlug }) => this.day
          .find(season.slug, competitionSlug, divisionSlug, groupSlug)
          .pipe(finalize(() => this.loading.day = false))),
        tapUniqueItem(day => day?.slug !== this.formGroup.getRawValue().day?.slug
          && this.formGroup.patchValue({ day })),
        finalize(() => this.loading.day = false));

    controlValueChange$<Group>(this.formGroup.controls.group)
      .pipe(
        takeUntil(this.destroy$),
        filter(() => this.mode !== EditMode.UPDATE))
      .subscribe(group => group ? this.formGroup.controls.teams.enable() : this.formGroup.controls.teams.disable());

    controlValueChange$<Day>(this.formGroup.controls.day)
      .pipe(
        takeUntil(this.destroy$))
      .subscribe(day => day?.date && !this.formGroup.getRawValue().date
        && this.formGroup.patchValue({ date: day.date.set('hour', 10).set('minute', 30) }));

    controlValueChange$<Team[]>(this.formGroup.controls.teams)
      .pipe(
        takeUntil(this.destroy$))
      .subscribe(teams => teams?.length ? this.formGroup.controls.participant.enable() : this.formGroup.controls.participant.disable());

    combineLatest([
      season$.pipe(map(season => season.slug)),
      this.route.queryParams
    ])
      .pipe(
        takeUntil(this.destroy$),
        map(([season, { category, competition, division, group, day }]) => ({ season, category, competition, division, group, day })),
        distinctUntilChanged(isEqual),
        switchMap(({ season, category, competition, division, group, day }) => group
        && group !== this.formGroup.getRawValue().group?.slug
          ? this.group.get(season, competition, division, group)
            // tslint:disable-next-line:no-shadowed-variable
            .pipe(map(group => ({ season, category, competition, division, group, day })))
          : of({ season, category, competition, division, group: undefined, day })),
        switchMap(({ season, category, competition, division, group, day }) => group && day
        && day !== this.formGroup.getRawValue().group?.slug
          ? this.day.get(season, competition, division, group.slug, day)
            // tslint:disable-next-line:no-shadowed-variable
            .pipe(map(day => ({ season, category, competition, division, group, day })))
          : of({ season, category, competition, division, group })))
      .subscribe((value) => this.formGroup.patchValue(value));

    this.teams$ = combineLatest([
      combineLatest([
        season$.pipe(map(season => season.slug)),
        controlValueChange$<string>(this.formGroup.controls.category),
        controlValueChange$<string>(this.formGroup.controls.competition),
        controlValueChange$<string>(this.formGroup.controls.division),
        controlValueChange$<Group>(this.formGroup.controls.group),
        controlValueChange$<Day>(this.formGroup.controls.day),
      ])
        .pipe(
          filter(([season, category, competition, division, group, day ]) => !!(season && category && competition && division
            && group?.slug && day?.slug)),
          map(([season, category, competition, division, group, day ]) => ({
            season, category, competition, division, group: group.slug, day: day.slug })),
          switchMap(({ season, ...params }) => this.plateau.find(season, params)),
          map(({ items }) => items),
          map(plateaux => plateaux.filter(plateau => !this.formGroup.value.slug || plateau.slug !== this.formGroup.value.slug)),
          map((plateaux): PlateauTeam[] => flatten(plateaux.map(plateau => plateau.teams)))),
      controlValueChange$<Group>(this.formGroup.controls.group)
        .pipe(map(group => group?.teams))
    ])
      .pipe(
        map(([busyTeams, groupTeams]: [PlateauTeam[], GroupTeam[]]): [string[], GroupTeam[]] => [
          busyTeams.map(team => team.slug),
          groupTeams
        ]),
        map(([busyTeamSlugs, groupTeams]) => groupTeams?.filter(team => !busyTeamSlugs.length || !busyTeamSlugs.includes(team.slug))),
        tap(teams => !teams.length && this.formGroup.controls.teams.setErrors({ noAvailableTeam: true })));

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => season$.pipe(mergeMap(season => this.plateau.get(season.slug, slug)
          .pipe(
            mergeMap(plateau => combineLatest([
              plateau.group
                ? this.group.get(season.slug, plateau.competition, plateau.division, plateau.group)
                : of(null),
              plateau.day
                ? this.day.get(season.slug, plateau.competition, plateau.division, plateau.group, plateau.day)
                : of(null)
            ])
              .pipe(map(([group, day]) => [plateau, group, day]))))))))
      .subscribe(([plateau, group, day]: [Plateau, Group, Day]) => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? plateau.slug : undefined,
          date: plateau.date,
          location: plateau.location,
          category: plateau.category,
          competition: plateau.competition,
          division: plateau.division,
          group: plateau.group && {
            slug: plateau.group,
            name: plateau.groupName,
            index: plateau.groupIndex,
            teams: group.teams
          },
          day,
          teams: plateau.teams
            ?.map(team => ({
              slug: team.slug,
              index: team.index,
              participant: team.participant,
              name: team.name,
              shortName: team.shortName,
              logoUrl: team.logoUrl
            })),
          participant: plateau.participant && {
            slug: plateau.participant,
            name: plateau.participantName,
            shortName: plateau.participantShortName,
            logoUrl: plateau.participantLogoUrl
          }
        });

        (() => {
          // tslint:disable-next-line:no-shadowed-variable
          const { category, competition, division, group, day } = plateau;
          this.previousRouteCommands = this.getPreviousRouteCommands({ category, competition, division, group, day });
        })();
      });
  }

  protected save$(): Observable<[Plateau, Link]> {
    const value = this.formGroup.getRawValue();
    return this.plateau.save(this.currentSeason.get().slug, {
      slug: value.slug,
      date: value.date,
      location: value.location,
      category: value.category,
      competition: value.competition,
      division: value.division,
      group: value.group?.slug,
      day: value.day?.slug,
      teams: value.teams?.map(({ slug }) => ({ slug })),
      participant: value.participant?.slug
    })
      .pipe(
        tap(({ category, competition, division, group, day }) =>
          this.previousRouteCommands = this.getPreviousRouteCommands({ category, competition, division, group, day })),
        map(plateau => {
          const { category, competition, division, group, day } = plateau;
          return [plateau, {
            commands: ['fmis', plateau.fmiSlug, 'update'],
            queryParams: { category, competition, division, group, day },
            label: this.translate.instant('admin.plateau.edit.action.editFmi')
          }];
        }));
  }

  getParticipants(): ParticipantClub[] {
    const participants = (this.formGroup.getRawValue().teams as Team[])
      ?.map(team => ({
        slug: team.participant,
        name: team.name,
        shortName: team.shortName,
        logoUrl: team.logoUrl
      }));
    return uniqBy(participants || [], 'slug');
  }

  categoryChange(): void {
    this.formGroup.patchValue({
      competition: undefined,
      division: undefined,
      group: undefined,
      day: undefined,
      teams: [],
      participant: undefined
    });
    this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();
    this.formGroup.controls.teams.disable();
    this.formGroup.controls.participant.disable();
  }

  competitionChange(): void {
    this.formGroup.patchValue({
      division: undefined,
      group: undefined,
      day: undefined,
      teams: undefined,
      participant: undefined
    });
    this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();
    this.formGroup.controls.teams.disable();
    this.formGroup.controls.participant.disable();
  }

  divisionChange(): void {
    this.formGroup.patchValue({
      group: undefined,
      day: undefined,
      teams: undefined,
      participant: undefined
    });
    this.formGroup.controls.day.disable();
    this.formGroup.controls.teams.disable();
    this.formGroup.controls.participant.disable();
  }

  groupChange(): void {
    this.formGroup.patchValue({
      day: undefined,
      teams: undefined,
      participant: undefined
    });
    this.formGroup.controls.participant.disable();
  }
}
