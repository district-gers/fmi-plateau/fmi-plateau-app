import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PlateauDetailRoutingModule } from './plateau-detail-routing.module';


@NgModule({
  imports: [
    CommonModule,
    PlateauDetailRoutingModule
  ]
})
export class PlateauDetailModule { }
