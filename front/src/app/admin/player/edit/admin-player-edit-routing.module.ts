import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminPlayerEditComponent } from './admin-player-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminPlayerEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPlayerEditRoutingModule { }
