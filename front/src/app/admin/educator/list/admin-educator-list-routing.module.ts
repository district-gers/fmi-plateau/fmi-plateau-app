import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminEducatorListComponent } from './admin-educator-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminEducatorListComponent,
    data: {
      meta: {
        title: 'admin.educator.list.seo.title',
        description: 'admin.educator.list.seo.description',
        'twitter:title': 'admin.educator.list.seo.title',
        'twitter:description': 'admin.educator.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminEducatorListRoutingModule { }
