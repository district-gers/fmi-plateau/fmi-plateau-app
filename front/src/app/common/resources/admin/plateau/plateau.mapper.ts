import { Plateau, PlateauDto } from '@resources/admin/plateau/plateau.model';
import * as moment from 'moment';

class PlateauMapper {

  toModel(dto: PlateauDto): Plateau {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      date: moment(dto.date),
      location: dto.location,
      day: dto.daySlug,
      dayIndex: dto.dayIndex,
      group: dto.groupSlug,
      groupIndex: dto.groupIndex,
      groupName: dto.groupName,
      division: dto.divisionSlug,
      divisionIndex: dto.divisionIndex,
      divisionName: dto.divisionName,
      competition: dto.competitionSlug,
      competitionName: dto.competitionName,
      category: dto.categorySlug,
      categoryName: dto.categoryName,
      teams: [...(dto.teams || [])]
        .map(team => ({
          slug: team.slug,
          index: team.index,
          participant: team.participantSlug,
          name: team.participantName,
          shortName: team.participantShortName,
          logoUrl: team.participantLogoUrl
        }))
        .sort((t1, t2) => t1.name?.toLocaleLowerCase()?.localeCompare(t2.name?.toLocaleLowerCase()) || t1.index - t2.index),
      participant: dto.participantSlug,
      participantName: dto.participantName,
      participantShortName: dto.participantShortName,
      participantLogoUrl: dto.participantLogoUrl,
      fmiSlug: dto.fmiSlug
    };
  }

  toDto(model: Plateau): PlateauDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('date') && {
        date: model.date?.format()
      },
      ...model.hasOwnProperty('location') && {
        location: model.location
      },
      ...model.hasOwnProperty('day') && {
        daySlug: model.day
      },
      ...model.hasOwnProperty('group') && {
        groupSlug: model.group
      },
      ...model.hasOwnProperty('division') && {
        divisionSlug: model.division
      },
      ...model.hasOwnProperty('competition') && {
        competitionSlug: model.competition
      },
      ...model.hasOwnProperty('category') && {
        categorySlug: model.category
      },
      ...model.hasOwnProperty('teams') && {
        teamSlugs: model.teams?.map(team => team.slug)
      },
      ...model.hasOwnProperty('participant') && {
        participantSlug: model.participant
      },
      ...model.hasOwnProperty('fmiSlug') && {
        fmiSlug: model.fmiSlug
      }
    };
  }

}

export const plateauMapper = new PlateauMapper();
