import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page, PageParams } from '@common/pagination/pagination';
import { ResponsiveService } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Participant } from '@resources/admin/participant/participant.model';
import { ParticipantService } from '@resources/admin/participant/participant.service';
import { tapUniqueItem } from '@utils/rxjs.util';
import { Observable } from 'rxjs';
import { finalize, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

interface FilterPageParams extends PageParams {
  category?: string;
}

@Component({
  selector: 'app-admin-participant-list',
  templateUrl: './admin-participant-list.component.html',
  styleUrls: ['./admin-participant-list.component.scss']
})
export class AdminParticipantListComponent extends AbstractAdminListComponent<Participant> implements OnInit {

  columns = ['logo', 'name', 'shortName', 'categoryName', 'clubs', 'action'];
  categories$: Observable<Page<Category>>;
  formGroup: FormGroup;

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private participant: ParticipantService,
    private category: CategoryService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  ngOnInit(): void {
    super.ngOnInit();

    const season$ = this.currentSeason.get$();

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.category = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().category && this.categoryChange(slug)),
        finalize(() => this.loading.category = false));

    this.formGroup = this.fb.group({
      category: []
    });

    this.route.queryParams
      .pipe(takeUntil(this.destroy$), map(params => params.category))
      .subscribe(category => this.formGroup.patchValue({ category }));
  }

  categoryChange(category: string) {
    this.refresh$.next({ category, page: undefined } as FilterPageParams);
  }

  protected delete$(item: Participant): Observable<void> {
    return this.participant.delete(this.currentSeason.get().slug, item.slug);
  }

  protected find$(params: FilterPageParams): Observable<Page<Participant>> {
    return this.currentSeason.get$().pipe(take(1), mergeMap(season => params.category
      ? this.participant.findByCategory(season.slug, params.category, params)
      : this.participant.find(season.slug, params)));
  }

  protected getItemLabel(item: Participant): string {
    return [
      item.name,
      item.categoryName
    ].join(' ');
  }

  protected getItemSlug(item: Participant): string {
    return item.slug;
  }
}
