import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminDivisionEditComponent } from './admin-division-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminDivisionEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminDivisionEditRoutingModule { }
