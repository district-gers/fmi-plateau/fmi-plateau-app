import { Params } from '@angular/router';
import { Right } from '@resources/user/right';
import { omitBy } from 'lodash';

export class NavMenuLink {
  routerCommands: string[];
  activePath?: string;
  parentActivePath?: string;
  queryParams?: (params: Params) => Params;
  analytics: {
    label: string;
    value?: string;
  };
  icon: string;
  labelSuffix: string;
  conditionUnion?: {
    right?: Right;
    plateau?: string;
  };
}

export const rootLinks: NavMenuLink[] = [
  {
    routerCommands: ['/'],
    activePath: '/',
    analytics: {
      label: 'home',
    },
    icon: 'home',
    labelSuffix: 'home'
  },
  {
    routerCommands: ['/', 'plateaux'],
    activePath: '/plateaux',
    queryParams: (params: Params) => omitBy(
      params,
      (value, key) => !['category', 'competition', 'division', 'group', 'day', 'startDate', 'endDate'].includes(key)),
    analytics: {
      label: 'plateaux'
    },
    icon: 'assignment',
    labelSuffix: 'plateaux'
  }
];

export const adminLinks: NavMenuLink[] = [
  {
    routerCommands: ['/', 'admin', 'seasons'],
    parentActivePath: '/admin/seasons',
    analytics: {
      label: 'admin-seasons',
    },
    icon: 'nature',
    labelSuffix: 'season',
    conditionUnion: {
      right: Right.ADMIN_SEASON
    }
  },
  {
    routerCommands: ['/', 'admin', 'clubs'],
    parentActivePath: '/admin/clubs',
    analytics: {
      label: 'admin-clubs',
    },
    icon: 'security',
    labelSuffix: 'club',
    conditionUnion: {
      right: Right.ADMIN_CLUB
    }
  },
  {
    routerCommands: ['/', 'admin', 'players'],
    parentActivePath: '/admin/players',
    analytics: {
      label: 'admin-players',
    },
    icon: 'directions_run',
    labelSuffix: 'player',
    conditionUnion: {
      right: Right.ADMIN_PLAYER
    }
  },
  {
    routerCommands: ['/', 'admin', 'educators'],
    parentActivePath: '/admin/educators',
    analytics: {
      label: 'admin-educators',
    },
    icon: 'emoji_people',
    labelSuffix: 'educator',
    conditionUnion: {
      right: Right.ADMIN_EDUCATOR
    }
  },
  {
    routerCommands: ['/', 'admin', 'categories'],
    parentActivePath: '/admin/categories',
    analytics: {
      label: 'admin-categories',
    },
    icon: 'dashboard',
    labelSuffix: 'category',
    conditionUnion: {
      right: Right.ADMIN_CATEGORY
    }
  },
  {
    routerCommands: ['/', 'admin', 'participants'],
    parentActivePath: '/admin/participants',
    queryParams: (params: Params) => omitBy(params, (value, key) => !['category'].includes(key)),
    analytics: {
      label: 'admin-participants',
    },
    icon: 'view_module',
    labelSuffix: 'participant',
    conditionUnion: {
      right: Right.ADMIN_PARTICIPANT
    }
  },
  {
    routerCommands: ['/', 'admin', 'teams'],
    parentActivePath: '/admin/teams',
    analytics: {
      label: 'admin-teams',
    },
    icon: 'flag',
    labelSuffix: 'team',
    conditionUnion: {
      right: Right.ADMIN_TEAM
    }
  },
  {
    routerCommands: ['/', 'admin', 'competitions'],
    parentActivePath: '/admin/competitions',
    queryParams: (params: Params) => omitBy(params, (value, key) => !['category'].includes(key)),
    analytics: {
      label: 'admin-competitions',
    },
    icon: 'emoji_events',
    labelSuffix: 'competition',
    conditionUnion: {
      right: Right.ADMIN_COMPETITION
    }
  },
  {
    routerCommands: ['/', 'admin', 'divisions'],
    parentActivePath: '/admin/divisions',
    queryParams: (params: Params) => omitBy(params, (value, key) => !['category', 'competition'].includes(key)),
    analytics: {
      label: 'admin-divisions',
    },
    icon: 'account_tree',
    labelSuffix: 'division',
    conditionUnion: {
      right: Right.ADMIN_DIVISION
    }
  },
  {
    routerCommands: ['/', 'admin', 'groups'],
    parentActivePath: '/admin/groups',
    queryParams: (params: Params) => omitBy(params, (value, key) => !['category', 'competition', 'division'].includes(key)),
    analytics: {
      label: 'admin-groups',
    },
    icon: 'device_hub',
    labelSuffix: 'group',
    conditionUnion: {
      right: Right.ADMIN_GROUP
    }
  },
  {
    routerCommands: ['/', 'admin', 'days'],
    parentActivePath: '/admin/days',
    queryParams: (params: Params) => omitBy(params, (value, key) => !['category', 'competition', 'division', 'group'].includes(key)),
    analytics: {
      label: 'admin-days',
    },
    icon: 'calendar_today',
    labelSuffix: 'day',
    conditionUnion: {
      right: Right.ADMIN_DAY
    }
  },
  {
    routerCommands: ['/', 'admin', 'plateaux'],
    parentActivePath: '/admin/plateaux',
    queryParams: (params: Params) => omitBy(
      params,
      (value, key) => !['category', 'competition', 'division', 'group', 'day', 'startDate', 'endDate'].includes(key)),
    analytics: {
      label: 'admin-plateaux',
    },
    icon: 'group_work',
    labelSuffix: 'plateau',
    conditionUnion: {
      right: Right.ADMIN_PLATEAU
    }
  },
  {
    routerCommands: ['/', 'admin', 'fmis'],
    parentActivePath: '/admin/fmis',
    queryParams: (params: Params) => omitBy(
      params,
      (value, key) => !['category', 'competition', 'division', 'group', 'day', 'startDate', 'endDate'].includes(key)),
    analytics: {
      label: 'admin-fmis',
    },
    icon: 'insert_drive_file',
    labelSuffix: 'fmi',
    conditionUnion: {
      right: Right.ADMIN_FMI
    }
  }
];

export const plateauLinks = (plateauSlug: string, fmiSlug: string): NavMenuLink[] => [
  {
    routerCommands: ['/', 'plateaux', plateauSlug, 'fmi', fmiSlug, 'read'],
    activePath: `/plateaux/${plateauSlug}/fmi/${fmiSlug}/read`,
    analytics: {
      label: 'plateau-fmi-read',
      value: plateauSlug
    },
    icon: 'library_add_check',
    labelSuffix: 'fmi.read',
    conditionUnion: {
      right: Right.FMI_READ
    }
  },
  {
    routerCommands: ['/', 'plateaux', plateauSlug, 'fmi', fmiSlug, 'matchs'],
    activePath: `/plateaux/${plateauSlug}/fmi/${fmiSlug}/matchs`,
    analytics: {
      label: 'plateau-fmi-match',
      value: plateauSlug
    },
    icon: 'screen_rotation',
    labelSuffix: 'fmi.match',
    conditionUnion: {
      right: Right.FMI_EDIT
    }
  },
  {
    routerCommands: ['/', 'plateaux', plateauSlug, 'fmi', fmiSlug, 'team'],
    activePath: `/plateaux/${plateauSlug}/fmi/${fmiSlug}/team`,
    analytics: {
      label: 'plateau-fmi-team',
      value: plateauSlug
    },
    icon: 'recent_actors',
    labelSuffix: 'fmi.team',
    conditionUnion: {
      right: Right.FMI_TEAM
    }
  },
  {
    routerCommands: ['/', 'plateaux', plateauSlug, 'fmi', fmiSlug, 'score'],
    activePath: `/plateaux/${plateauSlug}/fmi/${fmiSlug}/score`,
    analytics: {
      label: 'plateau-fmi-score',
      value: plateauSlug
    },
    icon: 'sports_soccer',
    labelSuffix: 'fmi.result',
    conditionUnion: {
      right: Right.FMI_SCORE,
      plateau: plateauSlug
    }
  }
];
