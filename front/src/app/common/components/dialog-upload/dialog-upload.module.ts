import { APP_BASE_HREF, CommonModule, PlatformLocation } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { DialogUploadService } from '@components/dialog-upload/dialog-upload.service';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialFileInputModule } from 'ngx-material-file-input';

import { DialogUploadComponent } from './dialog-upload.component';

@NgModule({
  declarations: [DialogUploadComponent],
  entryComponents: [DialogUploadComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    TranslateModule,
    MaterialFileInputModule,
    FormsModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    AsyncButtonModule
  ],
  providers: [
    DialogUploadService,
    {
      provide: APP_BASE_HREF,
      useFactory: (s: PlatformLocation) => s.getBaseHrefFromDOM()?.replace(/\/$/, ''),
      deps: [PlatformLocation]
    }
  ]
})
export class DialogUploadModule { }
