import { AfterViewInit, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page } from '@common/pagination/pagination';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Fmi, FmiMatch, FmiMatchStatus } from '@resources/admin/fmi/fmi.model';
import { FmiService } from '@resources/admin/fmi/fmi.service';
import { getFmiMatchStatus, getRequiredMatchCount } from '@resources/admin/fmi/fmi.util';
import { Plateau, PlateauTeam } from '@resources/admin/plateau/plateau.model';
import { PlateauService } from '@resources/admin/plateau/plateau.service';
import { Season } from '@resources/admin/season/season.model';
import * as moment from 'moment';
import { combineLatest, ConnectableObservable, Observable, of } from 'rxjs';
import { distinctUntilChanged, finalize, map, mergeMap, publishBehavior, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

const matchGroupConfig = {
  slug: [],
  date: [undefined, Validators.required],
  team1: ['', Validators.required],
  team2: ['', Validators.required]
};

@Component({
  selector: 'app-admin-fmi-edit',
  templateUrl: './admin-fmi-edit.component.html',
  styleUrls: [
    './admin-fmi-edit.component.scss',
    './admin-fmi-edit.component.responsive.scss'
  ]
})
export class AdminFmiEditComponent  extends AbstractAdminEditComponent<Fmi> implements OnInit, AfterViewInit {

  plateau$: Observable<Plateau>;
  screenSize$: Observable<ScreenSize>;
  formGroup: FormGroup;
  matchFormGroups: FormGroup[];
  metaLabelsPrefix = 'admin.fmi.edit.seo';

  plateaux$: Observable<Page<Plateau>>;
  fmi$: ConnectableObservable<Fmi>;
  requiredMatchCount$: Observable<number>;
  teamCount$: Observable<number>;
  matchStatus$: Observable<FmiMatchStatus>;

  FmiMatchStatus = FmiMatchStatus;

  @ViewChildren(FormGroupDirective) formGroupDirectives: QueryList<FormGroupDirective>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private responsive: ResponsiveService,
    private currentSeason: CurrentSeasonService,
    private fmi: FmiService,
    private plateau: PlateauService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.formGroup = this.fb.group({
      slug: [],
      fmi: [],
      plateau: [undefined, Validators.required],
      matchs: this.fb.array([]),
      matchsToDelete: this.fb.array([])
    });
    this.addMatchFormGroup();
    if (this.mode === EditMode.UPDATE) {
      this.formGroup.controls.plateau.disable();
    }
    this.matchFormGroups = (this.formGroup.controls.matchs as FormArray).controls as FormGroup[];

    const season$ = this.currentSeason.get$().pipe(take(1));
    this.screenSize$ = this.responsive.size$;

    this.plateaux$ = combineLatest([
      season$,
      this.route.params.pipe(takeUntil(this.destroy$), map(params => params.slug))
    ])
      .pipe(
        tap(() => this.loading.plateau = true),
        switchMap(([season, fmiSlug]) => this.plateau.findByFmi(season.slug, fmiSlug || null)
          .pipe(finalize(() => this.loading.plateau = false))),
        finalize(() => this.loading.plateau = false));

    this.fmi$ = super.itemSlug$()
      .pipe(
        switchMap((slug: string) => season$.pipe(mergeMap(season => this.fmi.get(season.slug, slug)))),
        tap(fmi => {
          this.formGroup.patchValue({
            slug: this.mode === EditMode.UPDATE ? fmi.slug : undefined,
            fmi,
            plateau: fmi.plateau
          });
          (this.formGroup.controls.matchs as FormArray).clear();
          fmi.matchs?.forEach((match) => {
            this.addMatchFormGroup(match);
          });
          if (!fmi.matchs?.length) {
            this.addMatchFormGroup({ date: fmi.date.clone() });
          }

          const { category, competition, division, group, day } = fmi;
          this.previousRouteCommands = {
            ...this.getPreviousRouteCommands({ category, competition, division, group, day }),
            commands: [this.getParentUrl()]
          };
        }),
        publishBehavior(null)) as ConnectableObservable<Fmi>;
    this.fmi$.connect();

    const plateauAndMatchCount$ = combineLatest([
      combineLatest([
        season$,
        this.formGroup.controls.plateau.valueChanges.pipe(distinctUntilChanged())
      ])
        .pipe(switchMap(([season, plateauSlug]: [Season, string]) => plateauSlug ? this.plateau.get(season.slug, plateauSlug) : of(null))),
      this.formGroup.controls.matchs.valueChanges.pipe(map(() => this.getValidMatchFormGroupCount()))
    ])
      .pipe(publishBehavior([])) as ConnectableObservable<[Plateau, number]>;
    plateauAndMatchCount$.connect();

    this.teamCount$ = plateauAndMatchCount$
      .pipe(
        map(([plateau]: [Plateau, number]) => plateau?.teams?.length));

    this.matchStatus$ = plateauAndMatchCount$
      .pipe(
        map(([plateau, matchCount]: [Plateau, number]) => plateau?.teams?.length
          && getFmiMatchStatus(plateau.teams?.length, matchCount)));

    this.requiredMatchCount$ = plateauAndMatchCount$
      .pipe(
        map(([plateau, matchCount]: [Plateau, number]) => plateau?.teams?.length
          && getRequiredMatchCount(plateau.teams?.length, matchCount)));
  }

  ngAfterViewInit(): void {
    this.formGroupDirectives.first.ngSubmit
      .pipe(takeUntil(this.destroy$))
      .subscribe(event => {
        this.formGroupDirectives
          .filter(item => !item.submitted)
          .forEach(item => item.onSubmit(event));
      });
  }

  protected save$(): Observable<[Fmi]> {
    const value = this.formGroup.getRawValue();
    return this.fmi.save(this.currentSeason.get().slug, {
      slug: value.slug,
      plateau: value.plateau,
      matchs: [
        ...value.matchs?.map((match): FmiMatch => ({
          slug: match.slug,
          date: match.date,
          team1: {
            slug: match.team1
          },
          team2: {
            slug: match.team2
          }
        })),
        ...value.matchsToDelete?.map((match): FmiMatch => ({
          slug: match.slug,
          toDelete: true
        }))
      ]
    })
      .pipe(
        tap(({ category, competition, division, group, day }) =>
          this.previousRouteCommands = {
            ...this.getPreviousRouteCommands({ category, competition, division, group, day }),
            commands: [this.getParentUrl()]
          }),
        map(fmi => [fmi]));
  }

  addMatchGroup(plateau: Plateau) {
    const lastMatch = this.formGroup.value.matchs[this.formGroup.value.matchs.length - 1];
    const control = this.fb.group(matchGroupConfig);
    control.patchValue({
      date: moment(lastMatch?.date || plateau.date)
    });
    (this.formGroup.controls.matchs as FormArray).push(control);
  }

  deleteMatchGroup(formGroupIndex: number) {
    const matchsFormArray = (this.formGroup.controls.matchs as FormArray);
    const formGroup = matchsFormArray.at(formGroupIndex) as FormGroup;
    if (formGroup) {
      if (formGroup.value.slug) {
        formGroup.patchValue({ toDelete: true });
        Object.values(formGroup.controls).forEach(control => control.setValidators(null));
        formGroup.setValidators(null);
        formGroup.updateValueAndValidity();
        (this.formGroup.controls.matchsToDelete as FormArray).push(formGroup);
      }
      matchsFormArray.removeAt(formGroupIndex);
    }
  }

  trackBySlug(index, item): string {
    return item.slug;
  }

  getTeamBySlug(teams: PlateauTeam[], slug: string): PlateauTeam {
    return teams?.find(team => team.slug === slug);
  }

  private addMatchFormGroup(match?: FmiMatch): FormGroup {
    const control = this.fb.group(matchGroupConfig);
    if (match) {
      control.patchValue({
        slug: match.slug,
        date: match.date,
        team1: match.team1?.slug,
        team2: match.team2?.slug
      });
    }
    (this.formGroup.controls.matchs as FormArray).push(control);
    return control;
  }

  getValidMatchFormGroupCount() {
    return (this.formGroup.controls.matchs as FormArray)?.controls?.filter(matchFormGroup => matchFormGroup.valid)?.length;
  }
}
