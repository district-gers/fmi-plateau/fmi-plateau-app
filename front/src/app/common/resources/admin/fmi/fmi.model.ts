import { Moment } from 'moment';

export enum FmiMatchStatus {
  COMPLETE = 'COMPLETE',
  PARTIAL = 'PARTIAL',
  EMPTY = 'EMPTY'
}


export interface Member {
  slug?: string;
  firstName?: string;
  lastName?: string;
  code?: number | string;
}

export interface FmiPlayer extends Member {
  category?: string;
}

interface Team {
  slug: string;
  index?: number;
  participant?: string;
  name?: string;
  shortName?: string;
  logoUrl?: string;
}

export interface PlayerSquad extends FmiPlayer {
  number?: number;
  injured?: boolean;
}

export interface TeamSquad {
  slug?: string;
  index?: number;
  participant?: string;
  name?: string;
  shortName?: string;
  logoUrl?: string;
  educator?: Member;
  players?: PlayerSquad[];
  signature?: Signature;
}

export interface TeamSquadDto {
  slug: string;
  index?: number;
  participantSlug?: string;
  name?: string;
  shortName?: string;
  logoUrl?: string;
  educator: Member;
  players: PlayerSquad[];
  signature?: Signature;
}

export interface Score {
  goals?: number;
  forfeit?: boolean;
}

export interface FmiMatch {
  slug?: string;
  date?: Moment;
  team1?: Team;
  team2?: Team;
  score1?: Score;
  score2?: Score;
  stopped?: boolean;
  notPlayed?: boolean;
  toDelete?: boolean;
}

interface FmiMatchDto {
  slug?: string;
  date?: string;
  team1?: Team;
  team2?: Team;
  score1?: Score;
  score2?: Score;
  stopped?: boolean;
  notPlayed?: boolean;
  toDelete?: boolean;
}

interface Signature {
  user: Member;
  pictureUrl?: string;
  comment?: string;
}

export interface Fmi {
  slug?: string;
  plateau?: string;
  date?: Moment;
  location?: string;
  day?: string;
  dayIndex?: number;
  group?: string;
  groupIndex?: number;
  groupName?: string;
  division?: string;
  divisionIndex?: number;
  divisionName?: string;
  competition?: string;
  competitionName?: string;
  category?: string;
  categoryName?: string;
  inChargeMember?: Member;
  comment?: string;
  doctor?: string;
  matchs?: FmiMatch[];
  teams?: TeamSquad[];
  participant?: {
    slug: string;
    name: string;
    shortName: string;
    logoUrl: string;
  };
  matchStatus?: FmiMatchStatus;
  matchCount?: number;
  requiredMatchCount?: number;
}

export interface FmiDto {
  slug?: string;
  plateauSlug?: string;
  date?: string;
  location?: string;
  daySlug?: string;
  dayIndex?: number;
  groupSlug?: string;
  groupIndex?: number;
  groupName?: string;
  divisionSlug?: string;
  divisionIndex?: number;
  divisionName?: string;
  competitionSlug?: string;
  competitionName?: string;
  categorySlug?: string;
  categoryName?: string;
  inChargeMember: Member;
  comment?: string;
  doctor: string;
  matchs: FmiMatchDto[];
  teams: TeamSquadDto[];
  participantSlug?: string;
  participantName?: string;
  participantShortName?: string;
  participantLogoUrl?: string;
  matchStatus?: FmiMatchStatus;
  matchCount?: number;
}
