export interface DialogConfirmationData {
  message: string;
}

export enum DialogConfirmationResult {
  CONFIRM = 'CONFIRM',
  DISMISS = 'DISMISS',
  CLOSE = 'CLOSE'
}
