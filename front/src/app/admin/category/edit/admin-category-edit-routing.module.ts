import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminCategoryEditComponent } from './admin-category-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminCategoryEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminCategoryEditRoutingModule { }
