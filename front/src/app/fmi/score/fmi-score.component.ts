import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Loading } from '@common/loading';
import { Fmi } from '@common/resources/admin/fmi/fmi.model';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { FmiService } from '@resources/admin/fmi/fmi.service';
import { combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, mergeMap, skip, switchMap, take, takeUntil, tap } from 'rxjs/operators';

const teamScoreGroupConfig = {
  team: [],
  goals: [undefined, [Validators.min(0), Validators.max(99)]],
  forfeit: [false]
};

@Component({
  selector: 'app-fmi-score',
  templateUrl: './fmi-score.component.html',
  styleUrls: [
    './fmi-score.component.scss',
    './fmi-score.component.mobile.scss'
  ]
})
export class FmiScoreComponent implements OnInit, OnDestroy {

  formGroup: FormGroup;
  previousRouteCommands: Link;
  isXsOrLower$: Observable<boolean>;
  screenSize$: Observable<ScreenSize>;
  loading: Loading = { save: false };

  private destroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private routerExt: RouterExtService,
    private currentSeason: CurrentSeasonService,
    private responsive: ResponsiveService,
    private fmi: FmiService) {
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      slug: [],
      matchs: this.fb.array([])
    });

    this.previousRouteCommands = this.routerExt.getPreviousUrl() || { commands: ['/plateaux'] };
    this.isXsOrLower$ = this.responsive.isXsOrLower$;
    this.screenSize$ = this.responsive.size$;
    const season$ = this.currentSeason.get$().pipe(take(1));

    combineLatest([season$, this.route.params])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, { fmiSlug }]) => !!fmiSlug),
        map(([season, { fmiSlug }]) => ({ season, fmiSlug })),
        distinctUntilChanged(),
        switchMap(({ season, fmiSlug }) => this.fmi.get(season.slug, fmiSlug)))
      .subscribe((fmi: Fmi) => {
        this.formGroup.patchValue({ slug: fmi.slug });
        fmi.matchs?.forEach((match) => {
          const control = this.fb.group({
            match: [],
            team1: this.fb.group(teamScoreGroupConfig),
            team2: this.fb.group(teamScoreGroupConfig),
            stopped: [false],
            notPlayed: [false]
          });
          control.patchValue({
            match,
            team1: {
              team: match.team1,
              goals: match.score1?.goals,
              forfeit: !!match.score1?.forfeit
            },
            team2: {
              team: match.team2,
              goals: match.score2?.goals,
              forfeit: !!match.score2?.forfeit
            },
            stopped: !!match.stopped,
            notPlayed: !!match.notPlayed
          });
          (this.formGroup.controls.matchs as FormArray).push(control);
        });

        if (!this.previousRouteCommands.queryParams) {
          this.previousRouteCommands.queryParams = {
            category: fmi.category,
            competition: fmi.competition,
            division: fmi.division,
            group: fmi.group,
            day: fmi.day
          };
        }
      });

    this.route.queryParams
      .pipe(
        map(({ season }) => season),
        distinctUntilChanged(),
        skip(1))
      .subscribe(() => this.router
        .navigate(['/plateaux'], { queryParamsHandling: 'merge', replaceUrl: true, relativeTo: this.route }));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  save() {
    const value = this.formGroup.getRawValue();
    this.currentSeason.get$()
      .pipe(
        tap(() => this.loading.save = true),
        take(1),
        mergeMap(season => this.fmi.save(season.slug, {
          slug: value.slug,
          matchs: value.matchs?.map(match => ({
            slug: match.match.slug,
            score1: {
              goals: match.team1.goals,
              forfeit: match.team1.forfeit
            },
            score2: {
              goals: match.team2.goals,
              forfeit: match.team2.forfeit
            },
            stopped: match.stopped,
            notPlayed: match.notPlayed
          }))
        })),
        finalize(() => this.loading.save = false))
      .subscribe(() => this.router.navigate(this.previousRouteCommands.commands, { queryParams: this.previousRouteCommands.queryParams }));
  }

  controlAsFormGroup(control: AbstractControl): FormGroup {
    return control as FormGroup;
  }

  controlAsFormGroups(control: AbstractControl): FormGroup[] {
    return (control as FormArray).controls as FormGroup[];
  }

  forfeitChange(matchGroup: FormGroup) {
    const anyForfeit = matchGroup.value.team1.forfeit || matchGroup.value.team2.forfeit;
    [matchGroup.controls.team1, matchGroup.controls.team2]
      .map((control: AbstractControl) => control as FormGroup)
      .forEach((formGroup: FormGroup) => {
        if (anyForfeit) {
          formGroup.controls.score.patchValue(formGroup.value.forfeit ? 0 : 3);
          formGroup.controls.score.disable();
        } else {
          formGroup.controls.score.enable();
          formGroup.controls.score.patchValue(undefined);
        }
      });
  }

  notPlayedChange(matchGroup: FormGroup) {
    [matchGroup.controls.team1, matchGroup.controls.team2]
      .map((control: AbstractControl) => control as FormGroup)
      .forEach((formGroup: FormGroup) => {
        if (matchGroup.value.notPlayed) {
          formGroup.controls.score.patchValue(undefined);
          formGroup.controls.score.disable();
          formGroup.controls.forfeit.patchValue(false);
          formGroup.controls.forfeit.disable();
        } else {
          formGroup.controls.score.enable();
          formGroup.controls.forfeit.enable();
        }
      });
    if (matchGroup.value.notPlayed) {
      matchGroup.controls.stopped.patchValue(false);
      matchGroup.controls.stopped.disable();
    } else {
      matchGroup.controls.stopped.enable();
    }
  }

  stoppedChange(matchGroup: FormGroup) {
    [matchGroup.controls.team1, matchGroup.controls.team2]
      .map((control: AbstractControl) => control as FormGroup)
      .forEach((formGroup: FormGroup) => {
        if (matchGroup.value.stopped) {
          formGroup.controls.forfeit.patchValue(false);
          formGroup.controls.forfeit.disable();
        } else {
          formGroup.controls.forfeit.enable();
        }
      });
    if (matchGroup.value.stopped) {
      matchGroup.controls.notPlayed.patchValue(false);
      matchGroup.controls.notPlayed.disable();
    } else {
      matchGroup.controls.notPlayed.enable();
    }
  }

}
