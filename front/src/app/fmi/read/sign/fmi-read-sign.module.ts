import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { SignatureControlModule } from '@components/signature-control/signature-control.module';
import { TranslateModule } from '@ngx-translate/core';
import { TeamNameModule } from '@pipes/team-name/team-name.module';
import { UserIsInChargeOfTeamModule } from '@pipes/user-is-in-charge-of-team/user-is-in-charge-of-team.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { FmiReadSignComponent } from './fmi-read-sign.component';



@NgModule({
  declarations: [FmiReadSignComponent],
  exports: [
    FmiReadSignComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    MatExpansionModule,
    TeamNameModule,
    LazyLoadImageModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    UserIsInChargeOfTeamModule,
    SignatureControlModule
  ]
})
export class FmiReadSignModule { }
