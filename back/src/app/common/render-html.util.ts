import * as ms from 'ms';
import { Request, Response } from 'express';
import { logger } from './logger/logger';
import { configService } from './config/config.service';
import { memjsClient } from './memcache';

const size = value => {
  if (typeof value === 'string' || value instanceof Buffer) {
    return `${Math.round(value.length / 1024)} KB`;
  }
  return '?';
};

const MEMCACHE_PREFIX = 'dg-fmi-plateau-app';

const getMemcacheKey = (req: Request) =>  `${MEMCACHE_PREFIX}-${req.hostname}${req.originalUrl}`;

export const render = async (req: Request, res: Response): Promise<any> => {
  let cacheValue;
  try {
    cacheValue = await memjsClient.get(getMemcacheKey(req));
  } catch (error) {
    logger.error(`Memcache ${req.originalUrl} get error`, error);
  }
  if (cacheValue && cacheValue.value) {
    const html = (cacheValue.value as Buffer).toString();
    logger.info(`Memcache ${req.originalUrl} fetched (${size(html)})`);
    return html;
  } else {
    return new Promise<any>((resolve, reject) => {
      res.render('index', { req, res }, (renderError, html) => {
        renderError && reject(renderError);
        html && resolve(html);
        // Cache set performed after resolved in order to send response ASAP
        const expires = Math.floor(ms(configService.config.cache.api.duration) / 1000);
        if (res.statusCode === 200) {
          memjsClient.set(getMemcacheKey(req), html, { expires }, (cacheSetError) => {
            cacheSetError && logger.error(`Memcache ${req.originalUrl} set error`, cacheSetError);
            !cacheSetError && logger.info(`Memcache ${req.originalUrl} set (${size(html)})`);
          });
        }
      });
    });
  }
};
