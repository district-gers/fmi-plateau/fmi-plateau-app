import { Observable } from 'rxjs';

export interface DialogUploadData {
  sampleFileUrl: string;
  formSubmitCallback: ({ file: File }) => Observable<any>;
}

export enum DialogUploadResult {
  UPLOADED = 'UPLOADED',
  ERROR = 'ERROR',
  DISMISS = 'DISMISS',
  CLOSE = 'CLOSE'
}
