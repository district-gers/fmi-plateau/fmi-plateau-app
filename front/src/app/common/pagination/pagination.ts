export interface Page<T> extends PageParams {
  items: T[];
  count: number;
}

export interface SortParams {
  field: string;
  direction: 'asc' | 'desc';
}

export interface PageParams {
  page?: number;
  size?: number;
  sort?: SortParams;
  deleted?: string;
  import?: string;
}

export const emptyPage: Page<any> = { page: 0, count: 0, items: [] };
