import { HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';

class CacheSubject<T> extends ReplaySubject<T> {

  private readonly superNext;

  constructor(private timeout: number) {
    super(1);

    // Because ReplaySubject overrides next method in its constructor instead of through its prototype, super.next refers to Subject.next,
    // so a reference of ReplaySubject.next must be stored to be able to reuse it
    this.superNext = this.next;
    this.next = this.nextAndComplete;
  }

  nextAndComplete(value: T) {
    this.superNext(value);
    this.complete();
  }

  next(value: T) {
    return this.nextAndComplete(value);
  }

  complete() {
    setTimeout(() => {
      super.complete();
    }, this.timeout);
  }

}

export const CACHE_DEFAULT_TIMEOUT = 1000;

@Injectable({ providedIn: 'root' })
export class InMemoryCacheService {

  private store = new Map<string, CacheSubject<HttpEvent<any>>>();

  get(key: string): Observable<HttpEvent<any>> {
    const obs = this.store.get(key);
    if (!obs || obs.isStopped) {
      this.store.set(key, new CacheSubject<HttpEvent<any>>(CACHE_DEFAULT_TIMEOUT));
      return null;
    }
    return obs.asObservable().pipe(take(1));
  }

  put(key: string, value: HttpEvent<any>) {
    const obs = this.store.get(key);
    obs.subscribe(
      () => {},
      () => {},
      () => this.store.delete(key));
    obs.next(value);
  }

  putError(key: string, value: HttpErrorResponse) {
    const obs = this.store.get(key);
    obs.subscribe(
      () => {},
      () => this.store.delete(key));
    obs.error(value);
  }

  delete(key) {
    const obs = this.store.get(key);
    if (obs) {
      this.store.delete(key);
      obs.next({ type: -1 });
      obs.complete();
    }
  }

}
