import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Club } from '@resources/admin/club/club.model';
import { ClubService } from '@resources/admin/club/club.service';
import { PictureService } from '@resources/admin/picture/picture.service';
import { Observable } from 'rxjs';
import { map, mergeMap, switchMap, take } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

@Component({
  selector: 'app-admin-club-edit',
  templateUrl: './admin-club-edit.component.html',
  styleUrls: ['./admin-club-edit.component.scss']
})
export class AdminClubEditComponent extends AbstractAdminEditComponent<Club> implements OnInit {

  formGroup: FormGroup;
  logoFormGroup: FormGroup;
  metaLabelsPrefix = 'admin.club.edit.seo';
  logoUrls$: Observable<string[]>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private club: ClubService,
    private picture: PictureService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.logoUrls$ = this.picture.find();

    this.formGroup = this.fb.group({
      slug: [],
      name: ['', Validators.required],
      shortName: [],
      code: [undefined, Validators.required],
      city: ['', Validators.required],
      logo: this.fb.group({
        url: []
      })
    });
    this.logoFormGroup = this.formGroup.controls.logo as FormGroup;

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => this.currentSeason.get$().pipe(take(1), mergeMap(season => this.club.get(season.slug, slug)))))
      .subscribe(club => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? club.slug : undefined,
          name: club.name,
          shortName: club.shortName,
          code: club.code,
          city: club.city,
          logo: {
            url: club.logoUrl
          }
        });
      });
  }

  protected save$(): Observable<[Club]> {
    const value = this.formGroup.getRawValue();
    return this.club.save(this.currentSeason.get().slug, {
      slug: value.slug,
      name: value.name,
      shortName: value.shortName,
      code: value.code,
      city: value.city,
      logoUrl: value.logo.url
    })
      .pipe(map(club => [club]));
  }

}
