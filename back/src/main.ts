import { app } from './app/app';

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  console.log(`SCP storefront app server listening on port ${PORT}!`);
});
