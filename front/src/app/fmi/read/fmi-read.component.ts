import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { CurrentUserService } from '@common/current-user.service';
import { Loading } from '@common/loading';
import { Fmi, PlayerSquad, TeamSquad } from '@common/resources/admin/fmi/fmi.model';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { Auth } from '@resources/admin/auth/auth.model';
import { FmiService } from '@resources/admin/fmi/fmi.service';
import { saveAs  } from 'file-saver';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, mergeMap, skip, switchMap, take, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'app-fmi-read',
  templateUrl: './fmi-read.component.html',
  styleUrls: ['./fmi-read.component.scss']
})
export class FmiReadComponent implements OnInit, OnDestroy {

  fmi$: Observable<Fmi>;
  user$: Observable<Auth>;
  formGroup: FormGroup;
  previousRouteCommands: Link;
  loading: Loading = { download: false };

  private destroy$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private routerExt: RouterExtService,
    private currentUser: CurrentUserService,
    private currentSeason: CurrentSeasonService,
    private fmi: FmiService) { }

  ngOnInit(): void {
    const season$ = this.currentSeason.get$().pipe(take(1));
    this.formGroup = this.fb.group({});
    this.previousRouteCommands = this.routerExt.getPreviousUrl() || { commands: ['/plateaux'] };

    this.fmi$ = combineLatest([season$, this.route.params])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, { fmiSlug }]) => !!fmiSlug),
        map(([season, { fmiSlug }]) => ({ season, fmiSlug })),
        distinctUntilChanged(),
        switchMap(({ season, fmiSlug }) => this.fmi.get(season.slug, fmiSlug)),
        tap(fmi => {
          fmi.teams.forEach(team => team.players.sort((p1, p2) => p1.number - p2.number));

          if (!this.previousRouteCommands.queryParams) {
            this.previousRouteCommands.queryParams = {
              category: fmi.category,
              competition: fmi.competition,
              division: fmi.division,
              group: fmi.group,
              day: fmi.day
            };
          }
        }));

    this.user$ = this.currentUser.get$();

    this.route.queryParams
      .pipe(
        map(({ season }) => season),
        distinctUntilChanged(),
        skip(1))
      .subscribe(() => this.router
        .navigate(['/plateaux'], { queryParamsHandling: 'merge', replaceUrl: true, relativeTo: this.route }));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  sign(fmi: Fmi) {
    const user = this.currentUser.get();
    of(null)
      .pipe(
        tap(() => this.loading.save = true),
        take(1),
        map(() => ({
          slug: fmi.slug,
          teams: Object.values(this.formGroup.controls)
            .filter(control => control.valid)
            .map((teamFormGroup => teamFormGroup.value))
            .map((teamValue: any): TeamSquad => ({
              slug: teamValue.team.slug,
              players: fmi.teams.find(team => team.slug === teamValue.team.slug)?.players
                ?.map((player): PlayerSquad => ({
                  ...player.slug && {
                    slug: player.slug,
                    injured: player.injured
                  },
                  ...!player.slug && { ...player }
                })),
              signature: {
                user: {
                  slug: user.educatorSlug,
                  firstName: user.firstName,
                  lastName: user.lastName
                },
                comment: teamValue.comment,
                pictureUrl: teamValue.pictureUrl
              }
            }))
        })),
        // tslint:disable-next-line:no-shadowed-variable
        mergeMap(fmi => this.fmi.save(this.currentSeason.get().slug, fmi)),
        finalize(() => this.loading.save = false))
      .subscribe(() => this.router.navigate(this.previousRouteCommands.commands, { queryParams: this.previousRouteCommands.queryParams }));
  }

  downloadPdf(fmi: Fmi) {
    const defaultFileName =
      `${fmi.categoryName} ${fmi.competitionName} ${fmi.divisionName} - ${fmi.date.format('DD.MM.YYYY')} - ${fmi.location}.pdf`;
    of(null)
      .pipe(
        tap(() => this.loading.download = true),
        mergeMap(() => this.fmi.pdf(this.currentSeason.get().slug, fmi.slug, defaultFileName)),
        finalize(() => this.loading.download = false))
      .subscribe(({ blob, name }) => saveAs(blob, name));
  }

  asAnyEnabledGroup(formGroup: FormGroup): boolean {
    return Object.values(formGroup.controls)
      .some(control => control.enabled);
  }
}
