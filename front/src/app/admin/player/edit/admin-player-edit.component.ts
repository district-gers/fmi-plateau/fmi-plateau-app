import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page } from '@common/pagination/pagination';
import { PlayerCategoryService } from '@common/player-category.service';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Club } from '@resources/admin/club/club.model';
import { ClubService } from '@resources/admin/club/club.service';
import { Player } from '@resources/admin/player/player.model';
import { PlayerService } from '@resources/admin/player/player.service';
import { combineLatest, Observable } from 'rxjs';
import { filter, finalize, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

@Component({
  selector: 'app-admin-player-edit',
  templateUrl: './admin-player-edit.component.html',
  styleUrls: ['./admin-player-edit.component.scss']
})
export class AdminPlayerEditComponent extends AbstractAdminEditComponent<Player> implements OnInit {

  formGroup: FormGroup;
  metaLabelsPrefix = 'admin.player.edit.seo';
  playerCategories$: Observable<string[]>;
  clubs$: Observable<Page<Club>>;
  screenSize$: Observable<ScreenSize>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private responsive: ResponsiveService,
    private playerCategory: PlayerCategoryService,
    private player: PlayerService,
    private club: ClubService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;
    this.playerCategories$ = this.playerCategory.getPlayerCategoryKeys$();
    this.clubs$ = this.currentSeason.get$()
      .pipe(
        take(1),
        tap(() => this.loading.club = true),
        mergeMap(season => this.club.find(season.slug)),
        finalize(() => this.loading.club = false));

    this.formGroup = this.fb.group({
      slug: [],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      code: ['', Validators.required],
      category: ['', Validators.required],
      club: ['', Validators.required]
    });

    this.route.queryParams
      .pipe(
        takeUntil(this.destroy$),
        filter(({ category }) => !!category))
      .subscribe(({ category }) => this.formGroup.patchValue({ category }));

    combineLatest([
      this.currentSeason.get$(),
      this.route.queryParams,
    ])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, { club }]) => !!club),
        switchMap(([season, { club }]) => this.club.get(season.slug, club)))
      .subscribe((club) => this.formGroup.patchValue({ club }));

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => this.currentSeason.get$().pipe(take(1), mergeMap(season => this.player.get(season.slug, slug)))))
      .subscribe(player => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? player.slug : undefined,
          firstName: player.firstName,
          lastName: player.lastName,
          code: player.code,
          category: player.category,
          club: {
            slug: player.club,
            name: player.clubName,
            shortName: player.clubShortName,
            logoUrl: player.clubLogoUrl
          }
        });
      });
  }

  protected save$(): Observable<[Player]> {
    const value = this.formGroup.getRawValue();
    return this.player.save(this.currentSeason.get().slug, {
          slug: value.slug,
          firstName: value.firstName,
          lastName: value.lastName,
          code: value.code,
          category: value.category,
          club: value.club?.slug
        })
        .pipe(map(player => [player]));
  }

}
