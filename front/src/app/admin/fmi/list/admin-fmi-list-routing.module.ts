import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminFmiListComponent } from './admin-fmi-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminFmiListComponent,
    data: {
      meta: {
        title: 'admin.fmi.list.seo.title',
        description: 'admin.fmi.list.seo.description',
        'twitter:title': 'admin.fmi.list.seo.title',
        'twitter:description': 'admin.fmi.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminFmiListRoutingModule { }
