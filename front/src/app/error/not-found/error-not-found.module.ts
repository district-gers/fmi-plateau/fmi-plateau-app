import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { Angulartics2OnModule } from 'angulartics2';

import { ErrorNotFoundRoutingModule } from './error-not-found-routing.module';
import { ErrorNotFoundComponent } from './error-not-found.component';


@NgModule({
  declarations: [ErrorNotFoundComponent],
  imports: [
    CommonModule,
    ErrorNotFoundRoutingModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    Angulartics2OnModule
  ]
})
export class ErrorNotFoundModule { }
