export enum EditMode {
  CREATE = 'create',
  UPDATE = 'update',
  DUPLICATE = 'duplicate'
}
