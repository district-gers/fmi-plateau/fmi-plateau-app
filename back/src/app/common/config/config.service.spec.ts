import { expect } from 'chai';
import { configService } from './config.service';

describe('ConfigService', () => {

  it('should log without any error', () => {
    const config = configService.config.current;

    expect(config).to.equal('test');
  });

});
