export interface Loading {
  [key: string]: boolean;
}
