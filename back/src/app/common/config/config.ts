export class Config {
  current?: string;
  logger?: {
    level?: string;
  };
  cache: {
    api: {
      duration: string;
    };
    assets: {
      duration: string;
    };
    html: {
      duration: string;
    };
  };
  system: {
    password: string;
  };
}
