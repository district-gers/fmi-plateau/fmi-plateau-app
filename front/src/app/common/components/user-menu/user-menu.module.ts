import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { TranslateModule } from '@ngx-translate/core';
import { Angulartics2OnModule } from 'angulartics2';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { UserMenuComponent } from './user-menu.component';

@NgModule({
  declarations: [UserMenuComponent],
  exports: [
    UserMenuComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatMenuModule,
    TranslateModule,
    MatButtonModule,
    Angulartics2OnModule,
    LazyLoadImageModule
  ]
})
export class UserMenuModule { }
