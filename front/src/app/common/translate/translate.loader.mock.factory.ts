import { TranslateLoader } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
const labelMocks = require('../../../assets/i18n/fr.json');

class TranslateLoaderMock implements TranslateLoader {
  constructor(private translations: { [key: string]: string }) {
  }

  getTranslation(lang: string): Observable<any> {
    return of(this.translations);
  }
}

export function TranslateLoaderMockFactory(translations: { [key: string]: any } = labelMocks): TranslateLoader {
  return new TranslateLoaderMock(translations);
}
