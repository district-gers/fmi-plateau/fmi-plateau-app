import { Auth } from '@resources/admin/auth/auth.model';
import { Right } from '@resources/user/right';

interface Plateau {
  participant?: string;
}

interface Team {
  participant?: string;
}

export const userHasRights = (user: Auth, ...rights: Right[]): boolean => rights
  ?.every((right: Right) => user?.rights?.includes(right));

export const userIsInChargeOfPlateau = (user: Auth, plateau: Plateau): boolean => user?.participantSlugs
  ?.includes(plateau?.participant);

export const userIsInChargeOfTeam = (user: Auth, team: Team): boolean => user?.participantSlugs
  ?.includes(team.participant);
