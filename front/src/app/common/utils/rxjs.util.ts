import { Page } from '@common/pagination/pagination';
import * as moment from 'moment';
import { MonoTypeOperatorFunction, OperatorFunction } from 'rxjs';
import { distinctUntilChanged, map, tap } from 'rxjs/operators';

export const filterUntilTimeout =
  <T>(delay = 500): OperatorFunction<T, T> => source => source.pipe(
      map((x: T) => [x, Math.floor(moment().valueOf() / delay)]),
      distinctUntilChanged(([, t1]: [T, number], [, t2]: [T, number]) => t1 === t2),
      map(([x]: [T, number]): T => x));

export const tapUniqueItem = <T>(patch: (item: T) => any): MonoTypeOperatorFunction<Page<T>> => source => source
  .pipe(tap(({ items }) => items.length === 1 && patch(items[0])));
