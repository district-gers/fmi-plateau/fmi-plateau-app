import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { emptyPage, Page } from '@common/pagination/pagination';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { Division } from '@resources/admin/division/division.model';
import { DivisionService } from '@resources/admin/division/division.service';
import { GroupService } from '@resources/admin/group/group.service';
import { controlValueChange$ } from '@utils/form.util';
import { tapUniqueItem } from '@utils/rxjs.util';
import { isEqual } from 'lodash';
import { combineLatest, ConnectableObservable, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, publishBehavior, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

const COUNT_MAX_OFFSET = 10;

@Component({
  selector: 'app-admin-division-edit',
  templateUrl: './admin-division-edit.component.html',
  styleUrls: ['./admin-division-edit.component.scss']
})
export class AdminDivisionEditComponent extends AbstractAdminEditComponent<Division> implements OnInit {

  formGroup: FormGroup;
  metaLabelsPrefix = 'admin.division.edit.seo';
  minGroupCount = 0;
  maxGroupCount = COUNT_MAX_OFFSET;

  categories$: Observable<Page<Category>>;
  competitions$: Observable<Page<Competition>>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private division: DivisionService,
    private category: CategoryService,
    private group: GroupService,
    private competition: CompetitionService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.formGroup = this.fb.group({
      slug: [],
      name: ['', Validators.required],
      index: [1, [Validators.required, Validators.min(1)]],
      groupCount: [1, Validators.max(COUNT_MAX_OFFSET)],
      category: [undefined, Validators.required],
      competition: [undefined, Validators.required]
    });
    this.formGroup.controls.competition.disable();
    if (this.mode === EditMode.UPDATE) {
      this.formGroup.controls.category.disable();
    }

    const season$ = this.currentSeason.get$().pipe(take(1));

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)),
        tapUniqueItem(({ slug: category }) => category !== this.formGroup.getRawValue().category
          && this.formGroup.patchValue({ category })),
        finalize(() => this.loading.category = false));

    this.competitions$ = combineLatest([ season$, controlValueChange$<string>(this.formGroup.controls.category) ])
      .pipe(
        map(([season, categorySlug]) => ({ season, categorySlug })),
        filter(({ season, categorySlug }) => season && !!categorySlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.competition = true),
        switchMap(({ season, categorySlug }) => this.competition.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.competition = false))),
        tapUniqueItem(({ slug: competition }) => competition !== this.formGroup.getRawValue().competition
          && this.formGroup.patchValue({ competition })),
        finalize(() => this.loading.competition = false));

    this.route.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe(({ competition }) => this.formGroup.patchValue({ competition }));

    combineLatest([ season$, this.route.queryParams ])
      .pipe(
        takeUntil(this.destroy$),
        map(([season, { competition }]) => ({ season, competitionSlug: competition })),
        filter(({ season, competitionSlug }) => season && !!competitionSlug),
        distinctUntilChanged(isEqual),
        switchMap(({ season, competitionSlug }) => this.competition.get(season.slug, competitionSlug)))
      .subscribe(({ category }) => this.formGroup.patchValue({ category }));

    const divisions$: ConnectableObservable<Page<Division>> = combineLatest([
      season$.pipe(map(season => season.slug)),
      controlValueChange$<string>(this.formGroup.controls.competition)
        .pipe(
          map((competition) => ({ competition })),
          filter(({ competition }) => !!competition),
          distinctUntilChanged(isEqual))
    ])
      .pipe(
        takeUntil(this.destroy$),
        map(([season, value]) => ({ ...value, season })),
        switchMap(({ season, competition }) => this.division
          .find(season, competition, { sort: { field: 'index', direction: 'asc' } })),
        filter(({ count }) => !!count))
      .pipe(publishBehavior(emptyPage)) as ConnectableObservable<Page<Division>>;
    divisions$.connect();

    this.formGroup.controls.index.setAsyncValidators(control => divisions$
      .pipe(
        take(1),
        map(({ items }) => items
          .filter(division => division.slug !== this.formGroup.value.slug)
          .map(division => division.index)
          .includes(control.value)
          ? { unicity: { items, current: control.value } }
          : undefined)));

    divisions$
      .pipe(
        takeUntil(this.destroy$),
        filter(() => this.mode !== EditMode.UPDATE),
        map(({ items }): Division => items[items.length - 1]),
        map(division => (division?.index || 0) + 1))
      .subscribe(index => this.formGroup.patchValue({
        index,
        name: `D${index}`
      }));

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => combineLatest([
          this.currentSeason.get$().pipe(take(1)),
          controlValueChange$<string>(this.formGroup.controls.competition),
          of(slug)
        ])),
        map(([season, competitionSlug, slug]) => ({ season, competitionSlug, slug })),
        filter(({ season, competitionSlug }) => season && !!competitionSlug),
        distinctUntilChanged(isEqual),
        switchMap(({ season, competitionSlug, slug }) => combineLatest([
          this.division.get(season.slug, competitionSlug, slug),
          this.group.find(season.slug, competitionSlug, slug)
        ])))
      .subscribe(([division, groups]) => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? division.slug : undefined,
          name: division.name,
          index: division.index,
          groupCount: groups.count
        });
        this.formGroup.controls.groupCount.setValidators([
          Validators.min(groups.count),
          Validators.max(groups.count + COUNT_MAX_OFFSET),
        ]);
        this.minGroupCount = groups.count;
        this.maxGroupCount = groups.count + COUNT_MAX_OFFSET;
        this.formGroup.controls.index.updateValueAndValidity();
      });
  }

  protected save$(): Observable<[Division, Link]> {
    const value = this.formGroup.getRawValue();
    const { category, competition, groupCount } = this.formGroup.getRawValue();
    return this.division.save(this.currentSeason.get().slug, competition, {
      slug: value.slug,
      name: value.name,
      index: value.index
    }, { groupCount })
      .pipe(
        tap(() => this.previousRouteCommands = this.getPreviousRouteCommands({ category, competition })),
        map(division => [division, {
          commands: ['groups'],
          queryParams: { category, competition, division: division.slug },
          label: this.translate.instant('admin.division.edit.action.seeGroups')
        }]));
  }

  categoryChange(): void {
    this.formGroup.patchValue({
      competition: undefined
    });
  }

}
