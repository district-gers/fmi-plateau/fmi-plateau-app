import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';
import { Angulartics2OnModule } from 'angulartics2';

import { FmiReadRoutingModule } from './fmi-read-routing.module';
import { FmiReadComponent } from './fmi-read.component';
import { FmiReadIntroModule } from './intro/fmi-read-intro.module';
import { FmiReadScoresModule } from './scores/fmi-read-scores.module';
import { FmiReadSignModule } from './sign/fmi-read-sign.module';
import { FmiReadTeamsModule } from './teams/fmi-read-teams.module';

@NgModule({
  declarations: [FmiReadComponent],
  imports: [
    CommonModule,
    FmiReadRoutingModule,
    TranslateModule,
    MatIconModule,
    MatButtonModule,
    Angulartics2OnModule,
    PageFormModule,
    FmiReadIntroModule,
    FmiReadTeamsModule,
    FmiReadScoresModule,
    FmiReadSignModule,
    AsyncButtonModule
  ]
})
export class FmiReadModule { }
