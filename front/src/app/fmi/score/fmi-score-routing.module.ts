import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FmiScoreComponent } from './fmi-score.component';

const routes: Routes = [
  {
    path: '',
    component: FmiScoreComponent,
    data: {
      meta: {
        title: 'fmi.score.seo.title',
        description: 'fmi.score.seo.description',
        'twitter:title': 'fmi.score.seo.title',
        'twitter:description': 'fmi.score.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FmiScoreRoutingModule { }
