import { isPlatformBrowser } from '@angular/common';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable()
export class ReadStateInterceptor implements HttpInterceptor {

  constructor(private transferState: TransferState,
              @Inject(PLATFORM_ID) private platformId) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (isPlatformBrowser(this.platformId) && req.method === 'GET' && req.urlWithParams.startsWith(environment.cloudFunction.data.url)) {
      const stateKey = makeStateKey(`G.${req.urlWithParams}`);
      const stateResponse = this.transferState.get(stateKey, null);
      if (stateResponse) {
        const response = new HttpResponse({ body: stateResponse.body, status: 200 });
        return of(response);
      }
    }
    return next.handle(req);
  }
}
