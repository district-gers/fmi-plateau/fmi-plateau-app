import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogConfirmationData, DialogConfirmationResult } from '@components/dialog-confirmation/dialog-confirmation';

@Component({
  selector: 'app-dialog-confirmation',
  templateUrl: './dialog-confirmation.component.html',
  styleUrls: [
    './dialog-confirmation.component.scss',
    './dialog-confirmation.component.mobile.scss'
  ]
})
export class DialogConfirmationComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogConfirmationComponent, DialogConfirmationResult>,
    @Inject(MAT_DIALOG_DATA) public data: DialogConfirmationData) {
  }

  confirm() {
    this.dialogRef.close(DialogConfirmationResult.CONFIRM);
  }

  dismiss() {
    this.dialogRef.close(DialogConfirmationResult.DISMISS);
  }

  close() {
    this.dialogRef.close(DialogConfirmationResult.CLOSE);
  }
}
