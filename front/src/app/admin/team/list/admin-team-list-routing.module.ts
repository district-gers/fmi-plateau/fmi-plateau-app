import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminTeamListComponent } from './admin-team-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminTeamListComponent,
    data: {
      meta: {
        title: 'admin.team.list.seo.title',
        description: 'admin.team.list.seo.description',
        'twitter:title': 'admin.team.list.seo.title',
        'twitter:description': 'admin.team.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminTeamListRoutingModule { }
