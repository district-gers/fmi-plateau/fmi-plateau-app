import { isPlatformServer } from '@angular/common';
import { Component, Inject, OnInit, Optional, PLATFORM_ID } from '@angular/core';
import { RESPONSE } from '@nguniversal/express-engine/tokens';

@Component({
  selector: 'app-error-server-unreachable',
  templateUrl: './error-server-unreachable.component.html',
  styleUrls: ['./error-server-unreachable.component.scss']
})
export class ErrorServerUnreachableComponent implements OnInit {

  constructor(@Optional() @Inject(RESPONSE) private response,
              @Inject(PLATFORM_ID) private platformId) {
  }

  ngOnInit() {
    if (isPlatformServer(this.platformId)) {
      this.response.status(504);
    }
  }

}
