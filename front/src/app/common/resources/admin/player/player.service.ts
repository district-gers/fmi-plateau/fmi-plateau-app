import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { BlobFile, extractTextFileName } from '@common/save-file.util';
import { playerMapper } from '@resources/admin/player/player.mapper';
import { Player, PlayerDto } from '@resources/admin/player/player.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = (seasonSlug: string) => `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/players`;

interface Criteria {
  categories?: string[];
  clubs?: string[];
  playerCategories?: string[];
  participants?: string[];
}

const stringifyCriteria = (criteria: Criteria): { [key: string]: any; } => ({
  ...criteria,
  ...criteria && Object.entries(criteria)
    .filter(([, value]) => Array.isArray(value) && value?.length)
    .map(([key, value]): [string, string] => [key, value.join(',')])
    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {})
});

@Injectable({ providedIn: 'root' })
export class PlayerService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, criteria: Criteria, pageParams?: PageParams): Observable<Page<Player>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>  this.http.get<PlayerDto[]>(resourceUrl(seasonSlug), {
          params: { ...params, ...stringifyCriteria(criteria) },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => playerMapper.toModel(dto)));
  }

  findAllByParticipant(seasonSlug: string, participantSlug: string, pageParams?: PageParams): Observable<Page<Player>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>  this.http.get<PlayerDto[]>(resourceUrl(seasonSlug), {
          params: { ...params, participant: participantSlug },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => playerMapper.toModel(dto)));
  }

  get(seasonSlug: string, slug: string): Observable<Player> {
    return this.http.get<PlayerDto>(`${resourceUrl(seasonSlug)}/${slug}`)
      .pipe(map(dto => playerMapper.toModel(dto)));
  }

  save(seasonSlug: string, model: Player): Observable<Player> {
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => playerMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<PlayerDto>(`${resourceUrl(seasonSlug)}/${dto.slug}`, dto)
          : this.http.post<PlayerDto>(resourceUrl(seasonSlug), dto)),
        map(dto => playerMapper.toModel(dto)));
  }

  delete(seasonSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug)}/${slug}`);
  }

  csv(seasonSlug: string, criteria: Criteria, defaultFileName?: string): Observable<BlobFile> {
    return this.http.get(`${resourceUrl(seasonSlug)}/csv`,
      { responseType: 'text', observe: 'response', params: stringifyCriteria(criteria) })
      .pipe(extractTextFileName(defaultFileName));
  }

  upload(seasonSlug: string, file: File): Observable<void> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post<void>(`${resourceUrl(seasonSlug)}/upload`, formData);
  }

}
