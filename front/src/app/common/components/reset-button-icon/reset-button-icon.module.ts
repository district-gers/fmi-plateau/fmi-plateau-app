import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';

import { ResetButtonIconComponent } from './reset-button-icon.component';



@NgModule({
  declarations: [ResetButtonIconComponent],
  exports: [
    ResetButtonIconComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule
  ]
})
export class ResetButtonIconModule { }
