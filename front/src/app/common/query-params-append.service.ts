import { Injectable } from '@angular/core';
import { NavigationStart, Params, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { CurrentUserService } from '@common/current-user.service';
import { combineLatest, of } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

const extractQueryParams = (url: string): Params => {
  return url
    .split('?')[1]
    ?.split('&')
    .map(param => param.split('='))
    .reduce((params, [key, value]) => ({ ...params, [key]: value }), {});
};

const extractUrlPath = (url: string) => {
  return url.split('?')[0];
};

@Injectable({ providedIn: 'root' })
export class QueryParamsAppendService {

  constructor(
    private router: Router,
    private currentUser: CurrentUserService,
    private currentSeason: CurrentSeasonService) {
    this.init();
  }

  private init() {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationStart),
        map(event => event as NavigationStart),
        map((event): [string, Params] => [extractUrlPath(event.url), extractQueryParams(event.url)]),
        switchMap(([urlPath, queryParams]) => combineLatest([
          of(urlPath),
          of(queryParams),
          of(null)
            .pipe(
              map(() => !queryParams?.season && this.currentSeason.get() && !this.currentSeason.get().isCurrent
                ? { season: this.currentSeason.get()?.slug } : undefined)),
          of(null)
            .pipe(
              map(() => !queryParams?.user && !!this.currentUser.get()?.uuid
                ? { user: this.currentUser.get()?.uuid } : undefined))
        ])),
        filter(([, , season, user]) => !!season || !!user))
      .subscribe(([urlPath, queryParams, season, user]) => {
        this.router.navigate([urlPath], { queryParams: { ...queryParams, ...season, ...user } });
      });
  }

}
