import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { Angulartics2OnModule } from 'angulartics2';

import { ErrorInternalRoutingModule } from './error-internal-routing.module';
import { ErrorInternalComponent } from './error-internal.component';

@NgModule({
  declarations: [ErrorInternalComponent],
  imports: [
    CommonModule,
    ErrorInternalRoutingModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    Angulartics2OnModule
  ]
})
export class ErrorInternalModule { }
