import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminParticipantRoutingModule } from './admin-participant-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminParticipantRoutingModule
  ]
})
export class AdminParticipantModule { }
