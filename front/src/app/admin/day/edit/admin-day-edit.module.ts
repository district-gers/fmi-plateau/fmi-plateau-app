import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { AsyncSelectModule } from '@components/async-select/async-select.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';

import { AdminDayEditRoutingModule } from './admin-day-edit-routing.module';
import { AdminDayEditComponent } from './admin-day-edit.component';


@NgModule({
  declarations: [AdminDayEditComponent],
  imports: [
    CommonModule,
    AdminDayEditRoutingModule,
    PageFormModule,
    ReactiveFormsModule,
    MatSelectModule,
    TranslateModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    AsyncButtonModule,
    AsyncSelectModule,
    MatDatepickerModule
  ]
})
export class AdminDayEditModule { }
