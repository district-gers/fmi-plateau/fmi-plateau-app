import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminCompetitionEditComponent } from './admin-competition-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminCompetitionEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminCompetitionEditRoutingModule { }
