import { Component, Input } from '@angular/core';
import { Auth } from '@resources/admin/auth/auth.model';
import { Plateau } from '@resources/admin/plateau/plateau.model';
import { Right } from '@resources/user/right';

@Component({
  selector: 'app-plateau-list-item',
  templateUrl: './plateau-list-item.component.html',
  styleUrls: [
    './plateau-list-item.component.scss',
    './plateau-list-item.component.responsive.scss'
  ]
})
export class PlateauListItemComponent {

  @Input() plateau: Plateau;
  @Input() user: Auth;
  rightEnum = Right;

  trackBySlug(index, item): string {
    return item.slug;
  }

}
