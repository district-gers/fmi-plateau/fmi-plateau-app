import { BreakPoint } from '@angular/flex-layout';

export const BREAKPOINTS: BreakPoint[] = [
  {
    alias: 'xxs',
    mediaQuery: 'screen and (min-width: 0px) and (max-width: 399.9px)',
    priority: 1100,
  },
  {
    alias: 'xs',
    mediaQuery: 'screen and (min-width: 400px) and (max-width: 599.9px)',
    priority: 1000,
  },
  {
    alias: 'sm',
    mediaQuery: 'screen and (min-width: 600px) and (max-width: 959.9px)',
    priority: 900,
  },
  {
    alias: 'md',
    mediaQuery: 'screen and (min-width: 960px) and (max-width: 1279.9px)',
    priority: 800,
  },
  {
    alias: 'lg',
    mediaQuery: 'screen and (min-width: 1280px) and (max-width: 1919.9px)',
    priority: 700,
  },
  {
    alias: 'xl',
    mediaQuery: 'screen and (min-width: 1920px) and (max-width: 4999.9px)',
    priority: 600,
  }
];
