import { Role } from '@resources/user/role';

export interface UserDto {
  slug?: string;
  uuid?: string;
  educatorSlug?: string;
  roles?: Role[];
  firstName?: string;
  lastName?: string;
  clubLogoUrl?: string;
}

export interface User {
  slug?: string;
  uuid?: string;
  educatorSlug?: string;
  roles?: Role[];
  firstName?: string;
  lastName?: string;
  clubLogoUrl?: string;
}
