import { Observable } from 'rxjs';

import { CookiesStatus } from './cookies-status';

export abstract class CookiesService {

  abstract allow(): void;

  abstract deny(): void;

  abstract get cookiesStatus$(): Observable<CookiesStatus>;

}
