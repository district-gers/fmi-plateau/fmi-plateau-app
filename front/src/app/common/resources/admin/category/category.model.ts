export interface CategoryDto {
  slug?: string;
  code?: string;
  name: string;
  rules?: {
    playerCategories: {
      [playerCategory: string]: number;
    }
  };
}

export interface Category {
  slug?: string;
  code?: string;
  name: string;
  rules?: {
    playerCategories: {
      [playerCategory: string]: number;
    }
  };
}
