import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminTeamRoutingModule } from './admin-team-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminTeamRoutingModule
  ]
})
export class AdminTeamModule { }
