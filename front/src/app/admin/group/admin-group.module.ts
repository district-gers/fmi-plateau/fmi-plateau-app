import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminGroupRoutingModule } from './admin-group-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminGroupRoutingModule
  ]
})
export class AdminGroupModule { }
