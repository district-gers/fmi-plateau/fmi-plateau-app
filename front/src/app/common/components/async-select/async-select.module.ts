import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { AsyncSelectComponent } from './async-select.component';



@NgModule({
  declarations: [AsyncSelectComponent],
  exports: [
    AsyncSelectComponent
  ],
  imports: [
    CommonModule,
    MatProgressSpinnerModule
  ]
})
export class AsyncSelectModule { }
