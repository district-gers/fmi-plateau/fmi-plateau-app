import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FooterFormComponent } from './footer-form.component';



@NgModule({
  declarations: [FooterFormComponent],
  exports: [
    FooterFormComponent
  ],
  imports: [
    CommonModule
  ]
})
export class FooterFormModule { }
