import { OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Loading } from '@common/loading';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, skip, switchMap, takeUntil, tap } from 'rxjs/operators';

import { EditMode } from './edit-mode.enum';

export abstract class AbstractAdminEditComponent<T> implements OnInit, OnDestroy {

  mode = EditMode.CREATE;
  editModeEnum = EditMode;
  previousRouteCommands: Link;
  loading: Loading = { save: false };

  abstract metaLabelsPrefix: string;

  protected destroy$ = new Subject<void>();

  protected constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected snackBar: MatSnackBar,
    protected translate: TranslateService,
    protected meta: MetaService,
    protected routerExt: RouterExtService) {
  }

  ngOnInit(): void {
    this.mode = (() => {
      switch (this.route.snapshot?.parent?.routeConfig?.path.split('/')[1]) {
        case 'update':
          return EditMode.UPDATE;
        case 'duplicate':
          return EditMode.DUPLICATE;
        default:
          return EditMode.CREATE;
      }
    })();

    this.previousRouteCommands = this.getPreviousRouteCommands(this.route.snapshot.queryParams);

    this.meta.update(this.router.routerState.snapshot.url, {
      title: `${this.metaLabelsPrefix}.${this.mode.toString().toLocaleLowerCase()}.title`,
      description: `${this.metaLabelsPrefix}.${this.mode.toString().toLocaleLowerCase()}.description`,
      'twitter:title': `${this.metaLabelsPrefix}.${this.mode.toString().toLocaleLowerCase()}.title`,
      'twitter:description': `${this.metaLabelsPrefix}.${this.mode.toString().toLocaleLowerCase()}.description`
    });

    this.route.queryParams
      .pipe(
        map(({ season }) => season),
        distinctUntilChanged(),
        skip(1),
        map(() => ['category', 'competition', 'division', 'group', 'day', 'participant', 'team']
          .reduce((acc, key) => ({ ...acc, [key]: undefined }), {})))
      .subscribe(queryParams => this.router
        .navigate([this.getParentUrl()], { queryParams, queryParamsHandling: 'merge', replaceUrl: true, relativeTo: this.route }));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  protected abstract save$(): Observable<[T, Link?]>;

  protected itemSlug$(): Observable<string> {
    return this.route.params
      .pipe(
        takeUntil(this.destroy$),
        filter(({ slug }) => !!slug),
        map(({ slug }) => slug),
        distinctUntilChanged());
  }

  save() {
    const adminUrl = this.getParentUrl('..');
    of(null)
      .pipe(
        tap(() => this.loading.save = true),
        switchMap(() => this.save$()),
        tap(([, link]) => this.snackBar
          .open(this.translate.instant('admin.common.notification.save'), link?.label, { duration: link ? 10000 : 3000 })
          .onAction()
          .subscribe(() => this.router.navigate(
            [adminUrl, ...link?.commands],
            { queryParams: link.queryParams }))),
        finalize(() => this.loading.save = false))
      .subscribe(() => this.router.navigate(this.previousRouteCommands.commands, { queryParams: this.previousRouteCommands.queryParams }));
  }

  protected getPreviousRouteCommands(queryParams?: Params): Link {
    const previousUrl = this.routerExt.getPreviousUrl();
    if (previousUrl) {
      return {
        ...previousUrl,
        ...queryParams && { queryParams: { ...previousUrl?.queryParams, ...queryParams } }
      };
    }
    const listUrl = this.getParentUrl();
    return { commands: [listUrl], queryParams };
  }

  protected getParentUrl(suffix?: string): string {
    const path = '../../..' + (suffix ? `/${suffix}` : '');
    const commands = this.mode === EditMode.CREATE ? [path] : [`../${path}`];
    const tree = this.router.createUrlTree(commands, { relativeTo: this.route });
    return this.router.serializeUrl(tree);
  }

  trackBySlug(index, item): string {
    return item.slug;
  }

  compareBySlug(item1, item2): boolean {
    return item1 && item2 && item1.slug === item2.slug;
  }
}
