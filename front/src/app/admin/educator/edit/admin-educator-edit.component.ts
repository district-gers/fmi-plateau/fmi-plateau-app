import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page } from '@common/pagination/pagination';
import { RefreshEvent, RefreshEventService } from '@common/refresh-event.service';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Club } from '@resources/admin/club/club.model';
import { ClubService } from '@resources/admin/club/club.service';
import { Educator } from '@resources/admin/educator/educator.model';
import { EducatorService } from '@resources/admin/educator/educator.service';
import { combineLatest, Observable } from 'rxjs';
import { filter, finalize, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

@Component({
  selector: 'app-admin-educator-edit',
  templateUrl: './admin-educator-edit.component.html',
  styleUrls: ['./admin-educator-edit.component.scss']
})
export class AdminEducatorEditComponent extends AbstractAdminEditComponent<Educator> implements OnInit {

  formGroup: FormGroup;
  metaLabelsPrefix = 'admin.educator.edit.seo';
  clubs$: Observable<Page<Club>>;
  screenSize$: Observable<ScreenSize>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private responsive: ResponsiveService,
    private educator: EducatorService,
    private club: ClubService,
    private refreshEvent: RefreshEventService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;
    this.clubs$ = this.currentSeason.get$()
      .pipe(
        take(1),
        tap(() => this.loading.club = true),
        mergeMap(season => this.club.find(season.slug)),
        finalize(() => this.loading.club = false));

    this.formGroup = this.fb.group({
      slug: [],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      code: ['', Validators.required],
      club: ['', Validators.required]
    });

    combineLatest([
      this.currentSeason.get$(),
      this.route.queryParams,
    ])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, { club }]) => !!club),
        switchMap(([season, { club }]) => this.club.get(season.slug, club)))
      .subscribe((club) => this.formGroup.patchValue({ club }));

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => this.currentSeason.get$().pipe(take(1), mergeMap(season => this.educator.get(season.slug, slug)))))
      .subscribe(educator => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? educator.slug : undefined,
          firstName: educator.firstName,
          lastName: educator.lastName,
          code: educator.code,
          club: {
            slug: educator.club,
            name: educator.clubName,
            shortName: educator.clubShortName,
            logoUrl: educator.clubLogoUrl
          }
        });
      });
  }

  protected save$(): Observable<[Educator]> {
    const value = this.formGroup.getRawValue();
    return this.educator.save(this.currentSeason.get().slug, {
      slug: value.slug,
      firstName: value.firstName,
      lastName: value.lastName,
      code: value.code,
      club: value.club?.slug
    })
      .pipe(
        map(educator => [educator]),
        tap<[Educator]>(() => this.refreshEvent.pushEvent(RefreshEvent.USER)));
  }

}
