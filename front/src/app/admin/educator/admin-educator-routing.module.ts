import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-educator-list.module').then(module => module.AdminEducatorListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-educator-edit.module').then(module => module.AdminEducatorEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-educator-edit.module').then(module => module.AdminEducatorEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-educator-edit.module').then(module => module.AdminEducatorEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminEducatorRoutingModule {
}
