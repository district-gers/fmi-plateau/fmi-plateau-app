import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { emptyPage, Page } from '@common/pagination/pagination';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { Day } from '@resources/admin/day/day.model';
import { DayService } from '@resources/admin/day/day.service';
import { Division } from '@resources/admin/division/division.model';
import { DivisionService } from '@resources/admin/division/division.service';
import { Group } from '@resources/admin/group/group.model';
import { GroupService } from '@resources/admin/group/group.service';
import { controlValueChange$ } from '@utils/form.util';
import { tapUniqueItem } from '@utils/rxjs.util';
import { isEqual } from 'lodash';
import { Moment } from 'moment';
import { combineLatest, ConnectableObservable, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, publishBehavior, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

@Component({
  selector: 'app-admin-day-edit',
  templateUrl: './admin-day-edit.component.html',
  styleUrls: ['./admin-day-edit.component.scss']
})
export class AdminDayEditComponent  extends AbstractAdminEditComponent<Day> implements OnInit {

  formGroup: FormGroup;
  metaLabelsPrefix = 'admin.day.edit.seo';

  categories$: Observable<Page<Category>>;
  competitions$: Observable<Page<Competition>>;
  divisions$: Observable<Page<Division>>;
  groups$: Observable<Page<Group>>;
  screenSize$: Observable<ScreenSize>;
  dateStartAt$: Observable<Moment>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private responsive: ResponsiveService,
    private day: DayService,
    private category: CategoryService,
    private competition: CompetitionService,
    private division: DivisionService,
    private group: GroupService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;

    this.formGroup = this.fb.group({
      slug: [],
      index: [1, [Validators.required, Validators.min(1), ]],
      date: [undefined, Validators.required],
      category: [undefined, Validators.required],
      competition: [undefined, Validators.required],
      division: [undefined, Validators.required],
      group: [undefined, Validators.required]
    });
    this.formGroup.controls.competition.disable();
    this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
    if (this.mode === EditMode.UPDATE) {
      this.formGroup.controls.category.disable();
    }

    const season$ = this.currentSeason.get$().pipe(take(1));

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)),
        tapUniqueItem(({ slug: category }) => category !== this.formGroup.getRawValue().category
          && this.formGroup.patchValue({ category })),
        finalize(() => this.loading.category = false));

    this.competitions$ = combineLatest([ season$, controlValueChange$<string>(this.formGroup.controls.category) ])
      .pipe(
        map(([season, categorySlug]) => ({ season, categorySlug })),
        filter(({ season, categorySlug }) => season && !!categorySlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.competition = true),
        switchMap(({ season, categorySlug }) => this.competition.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.competition = false))),
        tapUniqueItem(({ slug: competition }) => competition !== this.formGroup.getRawValue().competition
          && this.formGroup.patchValue({ competition })),
        finalize(() => this.loading.competition = false));

    this.divisions$ = combineLatest([ season$, controlValueChange$<string>(this.formGroup.controls.competition) ])
      .pipe(
        map(([season, competitionSlug]) => ({ season, competitionSlug })),
        filter(({ season, competitionSlug }) => season && !!competitionSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.division = true),
        switchMap(({ season, competitionSlug }) => this.division.find(season.slug, competitionSlug)
          .pipe(finalize(() => this.loading.division = false))),
        tapUniqueItem(({ slug: division }) => division !== this.formGroup.getRawValue().division
          && this.formGroup.patchValue({ division })),
        finalize(() => this.loading.division = false));

    this.groups$ = combineLatest([
      season$,
      controlValueChange$<string>(this.formGroup.controls.competition),
      controlValueChange$<string>(this.formGroup.controls.division)
    ])
      .pipe(
        map(([season, competitionSlug, divisionSlug]) => ({ season, competitionSlug, divisionSlug })),
        filter(({ season, competitionSlug, divisionSlug }) => season && !!competitionSlug && !!divisionSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.group = true),
        switchMap(({ season, competitionSlug, divisionSlug }) => this.group.find(season.slug, competitionSlug, divisionSlug)
          .pipe(finalize(() => this.loading.group = false))),
        tapUniqueItem(({ slug: group }) => group !== this.formGroup.getRawValue().group
          && this.formGroup.patchValue({ group })),
        finalize(() => this.loading.group = false));

    this.route.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe(({ competition, division, group }) => this.formGroup.patchValue({ competition, division, group }));

    combineLatest([ season$, this.route.queryParams ])
      .pipe(
        takeUntil(this.destroy$),
        map(([season, { competition }]) => ({ season, competitionSlug: competition })),
        filter(({ season, competitionSlug }) => season && !!competitionSlug),
        distinctUntilChanged(isEqual),
        switchMap(({ season, competitionSlug }) => this.competition.get(season.slug, competitionSlug)))
      .subscribe(({ category }) => this.formGroup.patchValue({ category }));

    const days$: ConnectableObservable<Page<Day>> = combineLatest([
        season$.pipe(map(season => season.slug)),
        combineLatest([
          controlValueChange$<string>(this.formGroup.controls.competition),
          controlValueChange$<string>(this.formGroup.controls.division),
          controlValueChange$<string>(this.formGroup.controls.group)
        ])
          .pipe(
            map(([competition, division, group ]) => ({ competition, division, group })),
            filter(({ competition, division, group }) => !!(competition && division && group)),
            distinctUntilChanged(isEqual))
      ])
        .pipe(
          takeUntil(this.destroy$),
          map(([season, value]) => ({ ...value, season })),
          switchMap(({ season, competition, division, group }) => this.day
            .find(season, competition, division, group, { sort: { field: 'index', direction: 'asc' } })),
          filter(({ count }) => !!count))
      .pipe(publishBehavior(emptyPage)) as ConnectableObservable<Page<Day>>;
    days$.connect();

    this.formGroup.controls.index.setAsyncValidators(control => days$
      .pipe(
        take(1),
        map(({ items }) => items
          .filter(day => day.slug !== this.formGroup.value.slug)
          .map(day => day.index)
          .includes(control.value)
          ? { unicity: { items, current: control.value } }
          : undefined)));

    days$
      .pipe(
        takeUntil(this.destroy$),
        filter(() => this.mode !== EditMode.UPDATE),
        map(({ items }): Day => items[items.length - 1]),
        map(day => (day?.index || 0) + 1))
      .subscribe(index => this.formGroup.patchValue({ index }));

    this.dateStartAt$ = combineLatest([
      days$,
      controlValueChange$<number>(this.formGroup.controls.index)
        .pipe(distinctUntilChanged()),
      controlValueChange$<Moment>(this.formGroup.controls.date)
        .pipe(distinctUntilChanged((m1, m2) => m1?.isSame(m2)), filter(date => !date))])
      .pipe(
        takeUntil(this.destroy$),
        map(([{ items }, index]): [Day[], number] => [
          items?.filter(day => day?.date).sort((d1, d2) => d1.index - d2.index),
          index,
        ]),
        map(([days, index]): [Day, number] => [
          index && days[days.findIndex(day => day.index >= index) - 1] || days[days.length - 1],
          index
        ]),
        map(([day]) => day?.date?.clone()?.add(1, 'week') || this.currentSeason.get().startDate.clone()));

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => combineLatest([
          this.currentSeason.get$().pipe(take(1)),
          controlValueChange$<string>(this.formGroup.controls.competition),
          controlValueChange$<string>(this.formGroup.controls.division),
          controlValueChange$<string>(this.formGroup.controls.group),
          of(slug)
        ])),
        map(([season, competitionSlug, divisionSlug, groupSlug, slug]) => ({ season, competitionSlug, divisionSlug, groupSlug, slug })),
        filter(({ season, competitionSlug, divisionSlug, groupSlug }) => season && !!competitionSlug && !!divisionSlug && !!groupSlug),
        distinctUntilChanged(isEqual),
        switchMap(({ season, competitionSlug, divisionSlug, groupSlug, slug }) => this.day
          .get(season.slug, competitionSlug, divisionSlug, groupSlug, slug)))
      .subscribe(day => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? day.slug : undefined,
          index: day.index,
          date: day.date
        });
        this.formGroup.controls.index.updateValueAndValidity();
      });
  }

  protected save$(): Observable<[Day, Link]> {
    const value = this.formGroup.getRawValue();
    const { category, competition, division, group } = this.formGroup.getRawValue();
    return this.day.save(
      this.currentSeason.get().slug, competition, division, group,
      {
        slug: value.slug,
        index: value.index,
        date: value.date
      })
      .pipe(
        tap(() => this.previousRouteCommands = this.getPreviousRouteCommands({ category, competition, division, group })),
        map(day => [day, {
          commands: ['plateaux'],
          queryParams: { category, competition, division, group, day: day.slug },
          label: this.translate.instant('admin.day.edit.action.seePlateaux')
        }]));
  }

  categoryChange(): void {
    this.formGroup.patchValue({
      competition: undefined,
      division: undefined,
      group: undefined
    });
    this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
  }

  competitionChange(): void {
    this.formGroup.patchValue({
      division: undefined,
      group: undefined
    });
    this.formGroup.controls.group.disable();
  }

  divisionChange(): void {
    this.formGroup.patchValue({
      group: undefined
    });
  }
}

