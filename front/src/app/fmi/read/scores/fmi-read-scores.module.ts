import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatListModule } from '@angular/material/list';
import { TranslateModule } from '@ngx-translate/core';
import { TeamNameModule } from '@pipes/team-name/team-name.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { FmiReadScoresComponent } from './fmi-read-scores.component';

@NgModule({
  declarations: [FmiReadScoresComponent],
  imports: [
    CommonModule,
    MatListModule,
    TranslateModule,
    TeamNameModule,
    LazyLoadImageModule
  ],
  exports: [FmiReadScoresComponent]
})
export class FmiReadScoresModule { }
