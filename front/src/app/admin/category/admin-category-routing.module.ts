import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-category-list.module').then(module => module.AdminCategoryListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-category-edit.module').then(module => module.AdminCategoryEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-category-edit.module').then(module => module.AdminCategoryEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-category-edit.module').then(module => module.AdminCategoryEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminCategoryRoutingModule { }
