import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@common/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'seasons',
        loadChildren: () => import('./season/admin-season.module').then(module => module.AdminSeasonModule)
      },
      {
        path: 'clubs',
        loadChildren: () => import('./club/admin-club.module').then(module => module.AdminClubModule)
      },
      {
        path: 'players',
        loadChildren: () => import('./player/admin-player.module').then(module => module.AdminPlayerModule)
      },
      {
        path: 'educators',
        loadChildren: () => import('./educator/admin-educator.module').then(module => module.AdminEducatorModule)
      },
      {
        path: 'categories',
        loadChildren: () => import('./category/admin-category.module').then(module => module.AdminCategoryModule)
      },
      {
        path: 'competitions',
        loadChildren: () => import('./competition/admin-competition.module').then(module => module.AdminCompetitionModule)
      },
      {
        path: 'divisions',
        loadChildren: () => import('./division/admin-division.module').then(module => module.AdminDivisionModule)
      },
      {
        path: 'groups',
        loadChildren: () => import('./group/admin-group.module').then(module => module.AdminGroupModule)
      },
      {
        path: 'days',
        loadChildren: () => import('./day/admin-day.module').then(module => module.AdminDayModule)
      },
      {
        path: 'participants',
        loadChildren: () => import('./participant/admin-participant.module').then(module => module.AdminParticipantModule)
      },
      {
        path: 'teams',
        loadChildren: () => import('./team/admin-team.module').then(module => module.AdminTeamModule)
      },
      {
        path: 'plateaux',
        loadChildren: () => import('./plateau/admin-plateau.module').then(module => module.AdminPlateauModule)
      },
      {
        path: 'fmis',
        loadChildren: () => import('./fmi/admin-fmi.module').then(module => module.AdminFmiModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
