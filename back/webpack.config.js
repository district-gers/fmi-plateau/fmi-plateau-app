const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: 'none',
  entry: './src/main.ts',
  target: 'node',
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json']
  },
  externals: [
    /^@firebase\/firestore/
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'fmi-plateau-app-back',
    libraryTarget: 'commonjs',
    publicPath: '/dist/'
  },
  plugins: [
    new CleanWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  }
};
