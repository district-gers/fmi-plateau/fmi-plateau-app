import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminPlateauRoutingModule } from './admin-plateau-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminPlateauRoutingModule
  ]
})
export class AdminPlateauModule { }
