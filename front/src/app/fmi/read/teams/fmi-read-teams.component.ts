import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Fmi } from '@common/resources/admin/fmi/fmi.model';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { Auth } from '@resources/admin/auth/auth.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-fmi-read-teams',
  templateUrl: './fmi-read-teams.component.html',
  styleUrls: [
    './fmi-read-teams.component.scss',
    './fmi-read-teams.component.mobile.scss'
  ]
})
export class FmiReadTeamsComponent implements OnInit {

  @Input() fmi: Fmi;
  @Input() user: Auth;
  @Input() formGroup: FormGroup;
  screenSize$: Observable<ScreenSize>;

  constructor(
    private responsive: ResponsiveService) { }

  ngOnInit(): void {
    this.screenSize$ = this.responsive.size$;
  }

  trackBySlug(index, item): string {
    return item.slug;
  }

}
