import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { CurrentSeasonService } from '@common/current-season.service';
import { CurrentUserService } from '@common/current-user.service';
import { emptyPage } from '@common/pagination/pagination';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { Educator } from '@resources/admin/educator/educator.model';
import { EducatorService } from '@resources/admin/educator/educator.service';
import { Fmi, Member, PlayerSquad, TeamSquad } from '@resources/admin/fmi/fmi.model';
import { FmiService } from '@resources/admin/fmi/fmi.service';
import { Player } from '@resources/admin/player/player.model';
import { PlayerService } from '@resources/admin/player/player.service';
import { Season } from '@resources/admin/season/season.model';
import { Right } from '@resources/user/right';
import { userHasRights, userIsInChargeOfTeam } from '@utils/user-right.util';
import { flatten } from 'lodash';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, map, switchMap, take, takeUntil } from 'rxjs/operators';

import { FmiTeamMemberConfig } from '../member/fmi-team-member.config';

const extraFilterGenerator = (teamFormGroup: FormGroup, playerFormGroup: FormGroup) => () => {
  const selectedMembers: Player[] = [
    ...(teamFormGroup.value.players as { auto: { member: Member; }; }[])
      .filter(({ auto }) => auto)
      .map(({ auto: { member: player } }) => player as Player)
      .filter(player => !!player),
    ...flatten(
      (teamFormGroup.value.participantTeamFormGroups as { value: { players: { auto: { member: Member; }; }[] } }[])
        ?.map(({ value }) => value)
        ?.map(({ players }) => (players as { auto: { member: Member; }; }[])
          .filter(({ auto }) => auto)
          .map(({ auto: { member: player } }) => player as Player)
          .filter(player => !!player))),
    ...teamFormGroup.value.unavailablePlayers as Player[] || []
  ];
  const unavailablePlayerSlugs = selectedMembers.map(player => player.slug);
  return (member: Member) => {
    return playerFormGroup.value.player?.slug === member.slug
      || !unavailablePlayerSlugs.includes(member.slug);
  };
};

const allAllowedCategories = (categories$: Observable<string[]>, teamFormGroup: FormGroup): Observable<string[]> => categories$
  .pipe(
    map(categories => categories.filter(playerCategory => {
      const category = teamFormGroup.value.category as Category;
      return !category?.rules?.playerCategories || category.rules.playerCategories[playerCategory];
    })));

@Component({
  selector: 'app-fmi-team-item',
  templateUrl: './fmi-team-item.component.html',
  styleUrls: ['./fmi-team-item.component.scss']
})
export class FmiTeamItemComponent implements OnInit, OnChanges, OnDestroy {

  @Input() form: NgForm;
  @Input() formGroup: FormGroup;

  educators$ = new BehaviorSubject<Educator[]>([]);
  players$ = new BehaviorSubject<Player[]>([]);
  categories$: Observable<string[]>;
  screenSize$: Observable<ScreenSize>;

  maxPlayers = 12;

  private destroy$ = new Subject<void>();
  private selectedPlayersChange$ = new BehaviorSubject<void>(null);
  private formGroupChange$ = new BehaviorSubject<FormGroup>(null);

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private currentSeason: CurrentSeasonService,
    private currentUser: CurrentUserService,
    private responsive: ResponsiveService,
    private educator: EducatorService,
    private player: PlayerService,
    private fmi: FmiService) {
  }

  ngOnInit(): void {
    this.screenSize$ = this.responsive.size$;
    const user$ = this.currentUser.get$();
    const season$ = this.currentSeason.get$().pipe(take(1));

    const init$ = combineLatest([season$, user$, this.formGroupChange$])
      .pipe(
        takeUntil(this.destroy$),
        map(([season, user, formGroup]) => ({ season, user, formGroup })),
        distinctUntilChanged((i1, i2) => i1.season.slug === i2.season.slug
          && i1.user.uuid === i2.user.uuid && i1.formGroup === i2.formGroup));

    const team: TeamSquad = this.formGroup.value.team;

    const educatorConfig: FmiTeamMemberConfig = {
      member: team.educator,
      members$: this.educators$,
      analyticsLabel: 'educator',
      labelKeys: {
        member: 'fmi.team.form.educator',
        firstName: 'fmi.team.form.firstName',
        lastName: 'fmi.team.form.lastName',
        code: 'fmi.team.form.code'
      }
    };

    this.categories$ = init$
      .pipe(
        switchMap(({ formGroup }) => this.translate.get('common.category')
          .pipe(
            map(((categoriesObj: { [key: string]: string; }) => Object.keys(categoriesObj)
              .filter(category => !(formGroup.value.category as Category)?.rules?.playerCategories
                || (formGroup.value.category as Category)?.rules?.playerCategories[category])
              .sort((c1, c2) => Number(c1.split(/u|-f/)[1]) - Number(c2.split(/u|-f/)[1]) || c1.localeCompare(c2))))
          )));

    init$
      .pipe(
        switchMap(({ season, formGroup }) => this.fmi.find(season.slug, {
          participant: (formGroup.value.team as TeamSquad).participant,
          startDate: (formGroup.value.fmi as Fmi).date.clone().subtract(2, 'days').utc().format(),
          endDate: (formGroup.value.fmi as Fmi).date.clone().add(2, 'days').utc().format(),
        }).pipe(
          map((participantFmiPage) => participantFmiPage.items?.filter(fmi => fmi.slug !== (formGroup.value.fmi as Fmi).slug)),
          map(participantFmis => [ season, participantFmis ]))),
        switchMap(([season, participantFmis]: [Season, Fmi[]]) => participantFmis.length
          ? combineLatest(participantFmis.map(fmi => this.fmi.get(season.slug, fmi.slug)))
          : of([])))
      .subscribe((participantFmis: Fmi[]) => {
        this.formGroup.patchValue({
          unavailablePlayers: flatten(flatten(participantFmis.map(fmi => fmi.teams
            .filter(t => t.participant === this.formGroup.value.team?.participant)
            .map(t => t.players))))
        });
        team.players
          .sort((p1, p2) => p1.number - p2.number)
          .forEach(player => this.addPlayerGroup(this.formGroup, player));
        this.addPlayerGroup(this.formGroup);
        this.selectedPlayersChange$.next();
      });

    init$
      .subscribe(() => {
        this.formGroup.patchValue({ educator: { config: educatorConfig } });
      });

    init$
      .pipe(
        switchMap(({ season, user }) => userHasRights(user, Right.FMI_TEAM) && userIsInChargeOfTeam(user, team)
        || userHasRights(user, Right.FMI_EDIT)
          ? this.educator.findAllByParticipant(season.slug, team.participant)
          : of(emptyPage)),
        map(({ items }) => items))
      .subscribe(x => this.educators$.next(x));

    init$
      .pipe(
        switchMap(({ season, user }) => userHasRights(user, Right.FMI_TEAM) && userIsInChargeOfTeam(user, team)
        || userHasRights(user, Right.FMI_EDIT)
          ? this.player.findAllByParticipant(season.slug, team.participant)
          : of(emptyPage)),
        map(({ items }) => items))
      .subscribe(x => this.players$.next(x));
  }

  ngOnChanges(changes: SimpleChanges): void {
    const formGroupChange = changes.formGroup;
    if (formGroupChange.previousValue !== formGroupChange.currentValue) {
      this.formGroupChange$.next(formGroupChange.currentValue);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.selectedPlayersChange$.complete();
    this.formGroupChange$.complete();
  }

  addPlayerGroup(teamFormGroup: FormGroup, player?: PlayerSquad) {
    const playersFormArray = (teamFormGroup.controls.players as FormArray);

    if (playersFormArray.length < this.maxPlayers) {
      const playerFormGroup = this.fb.group({
        id: [Symbol()],
        config: []
      });
      const config: FmiTeamMemberConfig = {
        member: player,
        members$: this.players$,
        membersRefresh$: this.selectedPlayersChange$,
        extraFilterFn: extraFilterGenerator(teamFormGroup, playerFormGroup),
        categories$: this.selectedPlayersChange$
          .pipe(
            switchMap(() => allAllowedCategories(this.categories$, teamFormGroup))),
        analyticsLabel: 'player',
        labelKeys: {
          member: 'fmi.team.form.player',
          firstName: 'fmi.team.form.firstName',
          lastName: 'fmi.team.form.lastName',
          code: 'fmi.team.form.code',
          category: 'fmi.team.form.category'
        }
      };
      playerFormGroup.patchValue({ config });
      playersFormArray.push(playerFormGroup);
    }
  }

  deletePlayerGroup(teamFormGroup: FormGroup, index: number) {
    (teamFormGroup.controls.players as FormArray).removeAt(index);
    this.selectedPlayersChange$.next(null);
  }

  onPlayerSelected(member: Member, teamFormGroup: FormGroup, isLast: boolean) {
    const player = member as Player;
    if (player?.slug) {
      this.selectedPlayersChange$.next(null);
    }
    if (isLast) {
      setTimeout(() => this.addPlayerGroup(teamFormGroup), 0);
    }
  }

  dropPlayer(event: CdkDragDrop<AbstractControl[]>, teamFormGroup: FormGroup) {
    const formGroups = (teamFormGroup.controls.players as FormArray).controls;
    moveItemInArray(formGroups, event.previousIndex, event.currentIndex);
  }

  trackByPlayerValue(index, formGroup: FormGroup) {
    return formGroup.value.id;
  }

  controlAsFormGroups(control: AbstractControl): FormGroup[] {
    return (control as FormArray).controls as FormGroup[];
  }

  controlAsFormGroup(control: AbstractControl): FormGroup {
    return control as FormGroup;
  }

}
