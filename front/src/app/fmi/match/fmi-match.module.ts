import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { ResetButtonIconModule } from '@components/reset-button-icon/reset-button-icon.module';
import { MatDatetimepickerModule } from '@mat-datetimepicker/core';
import { MatMomentDatetimeModule } from '@mat-datetimepicker/moment';
import { TranslateModule } from '@ngx-translate/core';
import { TeamNameModule } from '@pipes/team-name/team-name.module';
import { Angulartics2OnModule } from 'angulartics2';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { FmiMatchRoutingModule } from './fmi-match-routing.module';
import { FmiMatchComponent } from './fmi-match.component';


@NgModule({
  declarations: [FmiMatchComponent],
  imports: [
    CommonModule,
    FmiMatchRoutingModule,
    TranslateModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDatetimeModule,
    MatDatetimepickerModule,
    MatInputModule,
    MatCardModule,
    PageFormModule,
    TeamNameModule,
    Angulartics2OnModule,
    LazyLoadImageModule,
    AsyncButtonModule,
    ResetButtonIconModule
  ]
})
export class FmiMatchModule { }
