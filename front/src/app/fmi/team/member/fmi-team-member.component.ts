import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  QueryList,
  SimpleChanges, ViewChildren
} from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { licenseNumberValidator } from '@common/validators/license-number.validator';
import { FmiPlayer, Member } from '@resources/admin/fmi/fmi.model';
import { Player } from '@resources/admin/player/player.model';
import { combineLatest, Observable, of, OperatorFunction, Subject } from 'rxjs';
import { debounceTime, delay, map, startWith, switchMap, takeUntil } from 'rxjs/operators';

import { FmiTeamMemberConfig } from './fmi-team-member.config';

const filterMember =
  (members$: Observable<Member[]>, extraFilterGen?: () => (member: Member) => boolean): OperatorFunction<Member | string, Member[]> => {
    return (source: Observable<Member | string>): Observable<Member[]> => {
      return source
        .pipe(
          debounceTime(200),
          map((filterValue: Member | string) => typeof filterValue === 'string' ? filterValue : ''),
          switchMap((filterValue: string): Observable<Member[]> => members$
            .pipe(
              map((members: Member[]): Member[] => {
                const extraFilter = extraFilterGen?.();
                return extraFilter ? members.filter(extraFilter) : members;
              }),
              map((members: Member[]): Member[] => members.filter(member => filterValue.split(' ')
                .some(filterFragment => [member.firstName, member.lastName, `${member.code}`, (member as Player).category]
                  .filter(x => !!x)
                  .map((property: string) => property.toLocaleLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ''))
                  .some((property: string) => property
                    .includes(filterFragment?.toLocaleLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '')))))))));
    };
  };

@Component({
  selector: 'app-fmi-team-member',
  templateUrl: './fmi-team-member.component.html',
  styleUrls: [
    './fmi-team-member.component.scss',
    './fmi-team-member.component.mobile.scss'
  ]
})
export class FmiTeamMemberComponent implements OnChanges, OnDestroy {

  @Input() formGroup: FormGroup;
  @Input() index: number;
  formGroups: {
    auto: FormGroup;
    manual: FormGroup;
  };
  @Input() config: FmiTeamMemberConfig;

  @Output() selected = new EventEmitter<Member>();
  @Output() deleted = new EventEmitter<void>();

  @ViewChildren(FormGroupDirective) forms: QueryList<FormGroupDirective>;

  private destroy$ = new Subject<void>();

  constructor(
    private fb: FormBuilder) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    const formGroupChange = changes.formGroup;
    const configChange = changes.config;
    if (formGroupChange && formGroupChange.previousValue !== formGroupChange.currentValue
      || configChange && configChange.previousValue !== configChange.currentValue) {
      if (this.formGroup?.value.config) {
        this.config = this.formGroup.value.config;
        Object.entries({
          auto: this.fb.group({
            members$: [],
            member: []
          }),
          manual: this.fb.group({
            firstName: [],
            lastName: [],
            code: [undefined],
            category: []
          }),
          hideForm: this.fb.control(false),
          showManual: this.fb.control(false)
        })
          .forEach(([controlName, control]: [string, AbstractControl]) => this.formGroup.addControl(controlName, control));
        this.formGroups = {
          auto: this.formGroup.controls.auto as FormGroup,
          manual: this.formGroup.controls.manual as FormGroup
        };

        const member = this.config.member;
        this.formGroup.patchValue({
          auto: {
            members$: combineLatest([
              (this.formGroup.controls.auto as FormGroup).controls.member.valueChanges
                .pipe(
                  takeUntil(this.destroy$),
                  startWith('')),
              ...this.config.membersRefresh$ ? [
                this.config.membersRefresh$.pipe(takeUntil(this.destroy$), startWith(''))
              ] : []
            ])
              .pipe(
                delay(0),
                map(([filterValue]: [string, any]) => filterValue),
                filterMember(
                  this.config.members$
                    .pipe(switchMap(members => this.config.categories$
                      ? this.config.categories$
                        .pipe(map(categories => categories
                          ? members.filter(player => categories?.includes((player as FmiPlayer).category))
                          : members))
                      : of(members))),
                  this.config.extraFilterFn)),
            ...member?.slug && {
              member
            }
          },
          ...member && {
            hideForm: true,
            ...!member.slug && {
              showManual: true
            }
          },
          manual: {
            ...member && !member.slug && {
              firstName: member.firstName,
              lastName: member.lastName,
              code: typeof member.code === 'string' && parseInt((member.code as string).replace(/\s/g, ''), 10) || member.code,
              category: (member as Player).category
            }
          }
        });
        this.formGroup.setValidators(() => null);
        this.formGroup.updateValueAndValidity();

        of(null)
          .pipe(delay(0))
          .subscribe(() => this.switchManualRequiredValidator(member && !member.slug));
      }
    }
  }

  switchManualRequiredValidator(required: boolean): void {
    if (required) {
      this.formGroups.manual.controls.firstName.setValidators(Validators.required);
      this.formGroups.manual.controls.lastName.setValidators(Validators.required);
      this.formGroups.manual.controls.code.setValidators([Validators.required, licenseNumberValidator]);
      if (this.config.labelKeys.category) {
        this.formGroups.manual.controls.category.setValidators(Validators.required);
      }
    } else {
      this.formGroups.manual.controls.firstName.clearValidators();
      this.formGroups.manual.controls.lastName.clearValidators();
      this.formGroups.manual.controls.code.setValidators(licenseNumberValidator);
      this.formGroups.manual.controls.category.clearValidators();
    }
    this.formGroups.auto.controls.member.updateValueAndValidity();
    this.formGroups.manual.controls.firstName.updateValueAndValidity();
    this.formGroups.manual.controls.lastName.updateValueAndValidity();
    this.formGroups.manual.controls.code.updateValueAndValidity();
    this.formGroups.manual.controls.category.updateValueAndValidity();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  resetValues(switchShowManual?: boolean) {
    this.formGroup.patchValue({
      showManual: switchShowManual && !this.formGroup.value.showManual,
      hideForm: false
    });
  }

  onMemberSelected() {
    this.onMemberValidate();
  }

  onMemberValidate() {
    if (this.formGroup.value.showManual && this.formGroup.controls.manual.valid) {
      this.formGroup.patchValue({
        auto: {
          member: ''
        },
        hideForm: true
      });
      this.formGroup.controls.auto.markAsUntouched();
      this.selected.emit(this.formGroup.value.manual);
    } else if (!this.formGroup.value.showManual && this.formGroup.controls.auto.valid) {
      this.formGroup.patchValue({
        manual: {
          firstName: '',
          lastName: '',
          code: '',
          category: ''
        },
        hideForm: true
      });
      this.formGroup.controls.manual.markAsUntouched();
      this.selected.emit(this.formGroup.value.auto.member);
    }
  }

  deleteGroup() {
    this.resetValues();
    this.deleted.emit();
  }

  memberDisplay(member: Member): string {
    return member ? `${member.firstName} ${member.lastName}` : '';
  }

  trackBySlug(index, item): string {
    return item.slug;
  }

  private getAutoForm(): FormGroupDirective {
    return this.forms.find(form => form.form === this.formGroups.auto);
  }

  private getManualForm(): FormGroupDirective {
    return this.forms.find(form => form.form === this.formGroups.manual);
  }

  submitAutoForm(event: Event): void {
    this.getAutoForm()?.onSubmit(event);
  }

  submitManualForm(event: Event): void {
    this.getManualForm()?.onSubmit(event);
  }
}
