import { Pipe, PipeTransform } from '@angular/core';
import { Auth } from '@resources/admin/auth/auth.model';
import { userIsInChargeOfTeam } from '@utils/user-right.util';

interface Team {
  participant?: string;
}

@Pipe({
  name: 'userIsInChargeOfTeam'
})
export class UserIsInChargeOfTeamPipe implements PipeTransform {

  transform(user: Auth, team: Team): boolean {
    return userIsInChargeOfTeam(user, team);
  }

}
