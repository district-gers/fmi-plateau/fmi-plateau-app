import { User, UserDto } from '@resources/admin/user/user.model';

class UserMapper {

  toModel(dto: UserDto): User {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      uuid: dto.uuid,
      educatorSlug: dto.educatorSlug,
      roles: dto.roles,
      firstName: dto.firstName,
      lastName: dto.lastName,
      clubLogoUrl: dto.clubLogoUrl
    };
  }

  toDto(model: User): UserDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('uuid') && {
        uuid: model.uuid
      },
      ...model.hasOwnProperty('educatorSlug') && {
        educatorSlug: model.educatorSlug
      },
      ...model.hasOwnProperty('roles') && {
        roles: model.roles
      },
      ...model.hasOwnProperty('firstName') && {
        firstName: model.firstName
      },
      ...model.hasOwnProperty('lastName') && {
        lastName: model.lastName
      },
      ...model.hasOwnProperty('clubLogoUrl') && {
        clubLogoUrl: model.clubLogoUrl
      }
    };
  }

}

export const userMapper = new UserMapper();
