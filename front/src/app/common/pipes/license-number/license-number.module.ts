import { CommonModule, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';

import { LicenseNumberPipe } from './license-number.pipe';


@NgModule({
  declarations: [LicenseNumberPipe],
  imports: [
    CommonModule
  ],
  exports: [LicenseNumberPipe],
  providers: [
    LicenseNumberPipe,
    DecimalPipe
  ]
})
export class LicenseNumberModule { }
