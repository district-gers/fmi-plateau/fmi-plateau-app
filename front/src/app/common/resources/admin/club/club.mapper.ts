import { Club, ClubDto } from '@resources/admin/club/club.model';

class ClubMapper {

  toModel(dto: ClubDto): Club {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      name: dto.name,
      shortName: dto.shortName,
      code: dto.code,
      city: dto.city,
      logoUrl: dto.logoUrl
    };
  }

  toDto(model: Club): ClubDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('name') && {
        name: model.name
      },
      ...model.hasOwnProperty('shortName') && {
        shortName: model.shortName
      },
      ...model.hasOwnProperty('code') && {
        code: model.code
      },
      ...model.hasOwnProperty('city') && {
        city: model.city
      },
      ...model.hasOwnProperty('logoUrl') && {
        logoUrl: model.logoUrl
      }
    };
  }

}

export const clubMapper = new ClubMapper();
