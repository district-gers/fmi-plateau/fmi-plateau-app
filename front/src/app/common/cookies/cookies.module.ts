import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule, Type } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

import { CookiesService } from './cookies.service';

@NgModule({
  imports: [
    CommonModule
  ]
})
export class CookiesModule {
  static forRoot(service: Type<CookiesService>): ModuleWithProviders {
    return {
      ngModule: CookiesModule,
      providers: [
        {
          provide: CookiesService,
          useClass: service
        },
        CookieService
      ]
    };
  }
}
