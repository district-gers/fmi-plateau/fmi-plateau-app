import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page, PageParams } from '@common/pagination/pagination';
import { ResponsiveService } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { DialogUploadResult } from '@components/dialog-upload/dialog-upload';
import { DialogUploadService } from '@components/dialog-upload/dialog-upload.service';
import { TranslateService } from '@ngx-translate/core';
import { Club } from '@resources/admin/club/club.model';
import { ClubService } from '@resources/admin/club/club.service';
import { saveAs  } from 'file-saver';
import * as moment from 'moment';
import { Observable, of } from 'rxjs';
import { filter, finalize, mergeMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

@Component({
  selector: 'app-admin-club-list',
  templateUrl: './admin-club-list.component.html',
  styleUrls: ['./admin-club-list.component.scss']
})
export class AdminClubListComponent extends AbstractAdminListComponent<Club> {

  columns = ['logo', 'name', 'shortName', 'code', 'city', 'action'];

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private currentSeason: CurrentSeasonService,
    private club: ClubService,
    private dialogUpload: DialogUploadService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  downloadCsv() {
    const defaultFileName = `clubs-${moment().format('YYYY-MM-DD-hh-mm-ss')}.csv`;
    of(null)
      .pipe(
        tap(() => this.loading.download = true),
        mergeMap(() => this.club.csv(this.currentSeason.get().slug, defaultFileName)),
        finalize(() => this.loading.download = false))
      .subscribe(({ blob, name }) => saveAs(blob, name));
  }

  protected delete$(item: Club): Observable<void> {
    return this.club.delete(this.currentSeason.get().slug, item.slug);
  }

  protected find$(params: PageParams): Observable<Page<Club>> {
    return this.currentSeason.get$().pipe(take(1), mergeMap(season => this.club.find(season.slug, params)));
  }

  protected getItemLabel(item: Club): string {
    return item.name;
  }

  protected getItemSlug(item: Club): string {
    return item.slug;
  }

  fileUpload(): void {
    this.dialogUpload.open({
      sampleFileUrl: 'assets/club-import.csv',
      formSubmitCallback: ({ file }) => this.club.upload(this.currentSeason.get().slug, file)
    })
      .pipe(
        takeUntil(this.destroy$),
        mergeMap(dialogRef => dialogRef.afterClosed()),
        filter(result => result === DialogUploadResult.UPLOADED))
      .subscribe(() => this.refresh$.next({ import: moment().format() }));
  }

}
