import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminClubEditComponent } from './admin-club-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminClubEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminClubEditRoutingModule { }
