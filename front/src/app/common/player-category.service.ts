import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class PlayerCategoryService {

  constructor(
    private translate: TranslateService) {
  }

  getPlayerCategoryKeys$(): Observable<string[]> {
    return this.translate.get('common.category')
      .pipe(
        map(obj => Object.keys(obj)
          .sort((c1, c2) => {
            const n1 = Number(c1.match(/\d+/)?.[0]);
            const n2 = Number(c1.match(/\d+/)?.[0]);
            switch (true) {
              case typeof n1 === 'number' && typeof n2 === 'number':
                return n1 - n2;
              case typeof n1 === 'number' || !c1:
                return -1;
              case typeof n2 === 'number' || !c2:
                return 1;
              default:
                return c1?.localeCompare(c2);
            }
          })));
  }

}
