import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { LicenseNumberModule } from '@pipes/license-number/license-number.module';
import { UserIsInChargeOfPlateauModule } from '@pipes/user-is-in-charge-of-plateau/user-is-in-charge-of-plateau.module';

import { FmiReadIntroComponent } from './fmi-read-intro.component';

@NgModule({
  declarations: [FmiReadIntroComponent],
  imports: [
    CommonModule,
    TranslateModule,
    LicenseNumberModule,
    MatIconModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    UserIsInChargeOfPlateauModule
  ],
  exports: [FmiReadIntroComponent]
})
export class FmiReadIntroModule { }
