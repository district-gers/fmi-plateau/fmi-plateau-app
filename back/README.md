# FMI Plateau app back

## Project architecture
- [NodeJS][1]
- [Express][2]
- [Angular Universal][3]
- [Winston][4]
- [Webpack][5]
- [TSLint][6]
- [Mocha][7]
- [Compress][8]
- [MemJs][9]
- [Redis][10]
- [isBot][11]

## Project tasks 

### Development server
`npm run start`

Application is served at [http://localhost:4000](http://localhost:4000)

### Build
`npm run build`

Resulting artifact is located at `dist\`

### Unit tests
`npm run test`

### Code quality
`npm run lint`

[1]: https://nodejs.org/dist/latest-v8.x/docs/api/
[2]: http://expressjs.com/fr/starter/installing.html
[3]: https://github.com/winstonjs/winston
[4]: https://webpack.js.org/concepts/
[5]: https://github.com/remy/nodemon
[6]: https://palantir.github.io/tslint/
[7]: https://mochajs.org/
[8]: https://github.com/expressjs/compression
[9]: https://github.com/memcachier/memjs
[10]: https://github.com/NodeRedis/node_redis
[11]: https://github.com/gorangajic/isbot
