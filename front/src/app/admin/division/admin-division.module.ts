import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminDivisionRoutingModule } from './admin-division-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminDivisionRoutingModule
  ]
})
export class AdminDivisionModule { }
