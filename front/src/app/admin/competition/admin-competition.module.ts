import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminCompetitionRoutingModule } from './admin-competition-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminCompetitionRoutingModule
  ]
})
export class AdminCompetitionModule { }
