import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminPlayerRoutingModule } from './admin-player-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminPlayerRoutingModule
  ]
})
export class AdminPlayerModule { }
