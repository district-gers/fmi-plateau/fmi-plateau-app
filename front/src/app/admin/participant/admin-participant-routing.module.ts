import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
  {
    path: '',
    loadChildren: () => import('./list/admin-participant-list.module').then(module => module.AdminParticipantListModule)
  },
  {
    path: 'create',
    loadChildren: () => import('./edit/admin-participant-edit.module').then(module => module.AdminParticipantEditModule)
  },
  {
    path: ':slug/update',
    loadChildren: () => import('./edit/admin-participant-edit.module').then(module => module.AdminParticipantEditModule)
  },
  {
    path: ':slug/duplicate',
    loadChildren: () => import('./edit/admin-participant-edit.module').then(module => module.AdminParticipantEditModule)
  }
]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminParticipantRoutingModule { }
