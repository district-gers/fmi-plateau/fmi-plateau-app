import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { emptyPage, Page, PageParams } from '@common/pagination/pagination';
import { PlayerCategoryService } from '@common/player-category.service';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { DialogUploadResult } from '@components/dialog-upload/dialog-upload';
import { DialogUploadService } from '@components/dialog-upload/dialog-upload.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Club } from '@resources/admin/club/club.model';
import { ClubService } from '@resources/admin/club/club.service';
import { Participant } from '@resources/admin/participant/participant.model';
import { ParticipantService } from '@resources/admin/participant/participant.service';
import { Player } from '@resources/admin/player/player.model';
import { PlayerService } from '@resources/admin/player/player.service';
import { controlValueChange$ } from '@utils/form.util';
import { saveAs  } from 'file-saver';
import { isEqual } from 'lodash';
import * as moment from 'moment';
import { combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, mergeMap, startWith, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

interface FilterPageParams extends PageParams {
  categories?: string;
  clubs?: string;
  playerCategories?: string;
  participants?: string;
}

const parseParamsArrayStr = (...arrayFields: string[]) => (params: { [key: string]: any; }): { [key: string]: string[]; } => ({
  ...params && Object.entries(params)
    .filter(([key]) => arrayFields.includes(key))
    .map(([key, value]): [string, string[]] => [key, value?.split(',')])
    .map(([key, value]): [string, string[]] => [key, value?.length ? value : undefined])
    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {})
});

@Component({
  selector: 'app-admin-player-list',
  templateUrl: './admin-player-list.component.html',
  styleUrls: [
    './admin-player-list.component.scss',
    './admin-player-list.component.responsive.scss'
  ]
})
export class AdminPlayerListComponent extends AbstractAdminListComponent<Player> implements OnInit {

  columns = ['firstName', 'lastName', 'code', 'category', 'clubName', 'action'];
  screenSize$: Observable<ScreenSize>;
  clubs$: Observable<Page<Club>>;
  playerCategories$: Observable<string[]>;
  categories$: Observable<Page<Category>>;
  participants$: Observable<Page<Participant>>;
  formGroup: FormGroup;

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private playerCategory: PlayerCategoryService,
    private player: PlayerService,
    private dialogUpload: DialogUploadService,
    private club: ClubService,
    private category: CategoryService,
    private participant: ParticipantService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;

    const season$ = this.currentSeason.get$();

    this.clubs$ = season$
      .pipe(
        tap(() => this.loading.clubs = true),
        mergeMap(season => this.club.find(season.slug)
          .pipe(finalize(() => this.loading.clubs = false))),
        finalize(() => this.loading.clubs = false));

    this.playerCategories$ = this.playerCategory.getPlayerCategoryKeys$();

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.categories = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.categories = false))),
        finalize(() => this.loading.categories = false));

    this.participants$ = combineLatest([ season$, this.route.queryParams.pipe(map(parseParamsArrayStr('categories'))) ])
      .pipe(
        map(([season, { categories }]) => ({ season, categorySlugs: categories })),
        filter(({ season, categorySlugs }) => season && !!categorySlugs?.length),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.participants = true),
        switchMap(({ season, categorySlugs }) => this.participant.findByCategories(season.slug, categorySlugs)
          .pipe(finalize(() => this.loading.participants = false))),
        finalize(() => this.loading.participants = false));

    this.formGroup = this.fb.group({
        raw: this.fb.group({
          clubs: [],
          playerCategories: []
        }, {
          validators: (control: AbstractControl) => {
            const filledFieldsCount = Object.values(control.value).filter(value => Array.isArray(value) && value?.length).length < 2;
            return filledFieldsCount ? { required: filledFieldsCount } : null;
          }
        }),
        competition: this.fb.group({
          categories: [],
          participants: []
        }, {
          validators: (control: AbstractControl) => {
            const filledFieldsCount = Object.values(control.value).filter(value => Array.isArray(value) && value?.length).length < 2;
            return filledFieldsCount ? { required: filledFieldsCount } : null;
          }
        })
      },
      {
        validators: (control: AbstractControl) => {
          const hasAnySubControlValid = Object.values((control as FormGroup).controls).some(subControl => subControl.valid);
          return hasAnySubControlValid ? null : { required: !hasAnySubControlValid };
        }
      });
    const rawFormGroup = this.formGroup.controls.raw as FormGroup;
    const competitionFormGroup = this.formGroup.controls.competition as FormGroup;

    this.route.queryParams
      .pipe(
        takeUntil(this.destroy$),
        map(parseParamsArrayStr('clubs', 'playerCategories', 'categories', 'participants')))
      .subscribe(({ clubs, playerCategories, categories, participants }) => this.formGroup
        .patchValue({ raw: { clubs, playerCategories }, competition: { categories, participants } }));

    this.formGroup.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        startWith(this.formGroup.value as object),
        map(() => this.formGroup.getRawValue()),
        distinctUntilChanged(isEqual))
      .subscribe(({ raw: { clubs, playerCategories }, competition: { categories } }: { [_: string]: { [_: string]: string[] }; }) => {
        switch (true) {
          case !!(clubs?.length || playerCategories?.length):
            rawFormGroup.enable();
            competitionFormGroup.patchValue({ categories: undefined, participants: undefined });
            competitionFormGroup.disable();
            break;
          case !!categories?.length:
            rawFormGroup.patchValue({ clubs: undefined, playerCategories: undefined });
            rawFormGroup.disable();
            competitionFormGroup.enable();
            break;
          default:
            rawFormGroup.enable();
            competitionFormGroup.enable();
        }
      });

    controlValueChange$<string[]>(competitionFormGroup.controls.categories)
      .pipe(takeUntil(this.destroy$))
      .subscribe(categories => categories?.length
        ? competitionFormGroup.controls.participants.enable()
        : competitionFormGroup.controls.participants.disable());
  }

  downloadCsv() {
    const defaultFileName = `players-${moment().format('YYYY-MM-DD-hh-mm-ss')}.csv`;
    of(null)
      .pipe(
        tap(() => this.loading.download = true),
        map(() => parseParamsArrayStr('clubs', 'playerCategories', 'categories', 'participants')(this.route.snapshot.queryParams)),
        mergeMap(criteria => this.player.csv(this.currentSeason.get().slug, criteria, defaultFileName)),
        finalize(() => this.loading.download = false))
      .subscribe(({ blob, name }) => saveAs(blob, name));
  }

  clubsChange(clubs: string[]) {
    this.refresh$.next({ clubs: clubs?.join(',') || undefined, page: undefined } as FilterPageParams);
  }

  playerCategoriesChange(playerCategories: string[]) {
    this.refresh$.next({ playerCategories: playerCategories?.join(',') || undefined, page: undefined } as FilterPageParams);
  }

  categoriesChange(categories: string[]) {
    this.refresh$.next({ categories: categories?.join(',') || undefined, participants: undefined, page: undefined } as FilterPageParams);
  }

  participantsChange(participants: string[]) {
    this.refresh$.next({ participants: participants?.join(',') || undefined, page: undefined } as FilterPageParams);
  }

  protected delete$(item: Player): Observable<void> {
    return this.player.delete(this.currentSeason.get().slug, item.slug);
  }

  protected find$(params: FilterPageParams): Observable<Page<Player>> {
    const criteria = parseParamsArrayStr('clubs', 'playerCategories', 'categories', 'participants')(params);
    if (Object.keys(criteria).length < 2) {
      return of(emptyPage);
    }
    return this.currentSeason.get$().pipe(
      take(1),
      mergeMap(season => this.player.find(season.slug, criteria, params)));
  }

  protected getItemLabel(item: Player): string {
    return `${item.firstName} ${item.lastName}`;
  }

  protected getItemSlug(item: Player): string {
    return item.slug;
  }

  fileUpload(): void {
    this.dialogUpload.open({
      sampleFileUrl: 'assets/player-import.csv',
      formSubmitCallback: ({ file }) => this.player.upload(this.currentSeason.get().slug, file)
    })
      .pipe(
        takeUntil(this.destroy$),
        mergeMap(dialogRef => dialogRef.afterClosed()),
        filter(result => result === DialogUploadResult.UPLOADED))
      .subscribe(() => this.refresh$.next({ import: moment().format() }));
  }

  asFormGroup(control: AbstractControl): FormGroup {
    return control as FormGroup;
  }
}
