import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { AsyncSelectModule } from '@components/async-select/async-select.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';

import { AdminCompetitionEditRoutingModule } from './admin-competition-edit-routing.module';
import { AdminCompetitionEditComponent } from './admin-competition-edit.component';


@NgModule({
  declarations: [AdminCompetitionEditComponent],
  imports: [
    CommonModule,
    AdminCompetitionEditRoutingModule,
    PageFormModule,
    TranslateModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    AsyncButtonModule,
    AsyncSelectModule
  ]
})
export class AdminCompetitionEditModule { }
