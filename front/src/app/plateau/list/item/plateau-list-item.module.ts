import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { UserHasRightModule } from '@pipes/user-has-right/user-has-right.module';
import { UserIsInChargeOfPlateauModule } from '@pipes/user-is-in-charge-of-plateau/user-is-in-charge-of-plateau.module';
import { Angulartics2Module } from 'angulartics2';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { PlateauListItemComponent } from './plateau-list-item.component';



@NgModule({
  declarations: [PlateauListItemComponent],
  exports: [
    PlateauListItemComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    LazyLoadImageModule,
    MatChipsModule,
    MatIconModule,
    UserHasRightModule,
    RouterModule,
    MatButtonModule,
    Angulartics2Module,
    TranslateModule,
    UserIsInChargeOfPlateauModule
  ]
})
export class PlateauListItemModule { }
