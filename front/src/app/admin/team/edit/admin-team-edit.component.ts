import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page } from '@common/pagination/pagination';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Participant } from '@resources/admin/participant/participant.model';
import { ParticipantService } from '@resources/admin/participant/participant.service';
import { Team } from '@resources/admin/team/team.model';
import { TeamService } from '@resources/admin/team/team.service';
import { controlValueChange$ } from '@utils/form.util';
import { isEqual } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, distinctUntilChanged, filter, finalize, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

@Component({
  selector: 'app-admin-team-edit',
  templateUrl: './admin-team-edit.component.html',
  styleUrls: ['./admin-team-edit.component.scss']
})
export class AdminTeamEditComponent extends AbstractAdminEditComponent<Team> implements OnInit {

  formGroup: FormGroup;
  logoFormGroup: FormGroup;
  metaLabelsPrefix = 'admin.participant.edit.seo';

  categories$: Observable<Page<Category>>;
  participants$: Observable<Page<Participant>>;
  screenSize$: Observable<ScreenSize>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private responsive: ResponsiveService,
    private team: TeamService,
    private category: CategoryService,
    private participant: ParticipantService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;

    this.formGroup = this.fb.group({
      slug: [],
      index: [undefined, Validators.required],
      category: [undefined, Validators.required],
      participant: [undefined, Validators.required]
    });
    this.logoFormGroup = this.formGroup.controls.logo as FormGroup;
    this.formGroup.controls.participant.disable();
    if (this.mode === EditMode.UPDATE) {
      this.formGroup.controls.category.disable();
    }

    const season$ = this.currentSeason.get$().pipe(take(1));

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.category = false))),
        finalize(() => this.loading.category = false));

    this.participants$ = combineLatest([ season$, controlValueChange$<string>(this.formGroup.controls.category) ])
      .pipe(
        map(([season, categorySlug]) => ({ season, categorySlug })),
        filter(({ season, categorySlug }) => season && !!categorySlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.participant = true),
        switchMap(({ season, categorySlug }) => this.participant.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.participant = false))),
        finalize(() => this.loading.participant = false));

    combineLatest([ season$, this.route.queryParams ])
      .pipe(
        takeUntil(this.destroy$),
        switchMap(([season, { category, participant: participantSlug }]) => participantSlug
          ? this.participant.get(season.slug, participantSlug)
            .pipe(
              map(participant => ({ category: participant.category, participant })),
              catchError(() => of({} as any)))
          : of({ category })))
      .subscribe(({ category, participant }) => this.formGroup.patchValue({ category, participant }));

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => this.currentSeason.get$().pipe(take(1), mergeMap(season => this.team.get(season.slug, slug)))))
      .subscribe(team => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? team.slug : undefined,
          index: team.index,
          category: team.category,
          participant: {
            slug: team.participant,
            name: team.name,
            shortName: team.shortName,
            logoUrl: team.logoUrl
          }
        });
      });
  }

  protected save$(): Observable<[Team]> {
    const value = this.formGroup.getRawValue();
    return this.team.save(this.currentSeason.get().slug, {
      slug: value.slug,
      index: value.index,
      participant: value.participant?.slug
    })
      .pipe(map(team => [team]));
  }

  categoryChange(): void {
    this.formGroup.patchValue({
      participant: undefined
    });
  }

}
