# FMI plateau app

## Technical architecture

### Client
- [Angular][1]
- [Angular Material][2]
- [Angular Universal][3]

### Server
- [NodeJS][4]
- [Express][5]

### CI
- [GitLab CI][6]
- [Gitlab pages][7]
- [Google Cloud Platform][8]

## Project tasks

> Requirements :
>
> - Node 12
> - NPM 6
>

### Build

#### Front
```
npm run build:front
```

Resulting artifact is located at `back/static/`

#### Back
```
npm run build:back
```

Resulting artifact is located at `back/dist/`

#### Both front & back
```
npm run build
```

Resulting artifacts are located at
- `back/static/`
- `back/dist/`

### Start
```
npm start
```

Application is served at [http://localhost:4000](http://localhost:4000)

### Deploy

> Requirements :
>
> - GCloud SDK
>

```
gcloud app deploy
```

Application is deploy at [https://dg-fmi-plateau.appspot.com](https://dg-fmi-plateau.appspot.com)

## Version control system

Application sources are versioned under GitLab repository [https://gitlab.com/district-gers/fmi-plateau/fmi-plateau-app.git](gitlab.com/district-gers/fmi-plateau/fmi-plateau-app.git)

[1]: https://angular.io/guide/quickstart
[2]: https://angular.io/guide/universal
[3]: https://material.angular.io/guide/getting-started
[4]: https://nodejs.org/dist/latest-v8.x/docs/api/
[5]: http://expressjs.com/fr/starter/installing.html
[6]: https://docs.gitlab.com/ee/ci/quick_start/
[7]: https://docs.gitlab.com/ee/user/project/pages/
[8]: https://cloud.google.com/nodejs/docs/
