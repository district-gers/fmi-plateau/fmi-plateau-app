import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-club-list.module').then(module => module.AdminClubListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-club-edit.module').then(module => module.AdminClubEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-club-edit.module').then(module => module.AdminClubEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-club-edit.module').then(module => module.AdminClubEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminClubRoutingModule { }
