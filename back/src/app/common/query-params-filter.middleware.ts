import { NextFunction, Request, Response } from 'express';

const replaceQueryParams = (url: string, queryParams: string) => {
  const [urlPath] = url.split('?');
  return [urlPath, queryParams].filter(urlPart => urlPart).join('?');
};

const defaultQueryParams = [];
export const queryParamsMiddleware = (...whitelist: string[]) => (req: Request, res: Response, next: NextFunction) => {
  const extendedWhiteList = [...defaultQueryParams, ...(whitelist || [])];
  const queryParamsStr = Object.keys(req.query)
    .filter(key => extendedWhiteList.includes(key))
    .map(key => [key, req.query[key]].join('='))
    .join('&');
  req.originalUrl = replaceQueryParams(req.originalUrl, queryParamsStr);
  req.url = replaceQueryParams(req.url, queryParamsStr);
  next();
};
export const defaultQueryParamsMiddleware = queryParamsMiddleware();
