import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { seasonMapper } from '@resources/admin/season/season.mapper';
import { Season, SeasonDto } from '@resources/admin/season/season.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = `${environment.cloudFunction.data.url}/seasons`;

@Injectable({ providedIn: 'root' })
export class SeasonService {

  constructor(
    private http: HttpClient) {
  }

  find(pageParams?: PageParams): Observable<Page<Season>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>  this.http.get<SeasonDto[]>(resourceUrl, { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => seasonMapper.toModel(dto)));
  }

  get(slug: string): Observable<Season> {
    return this.http.get<SeasonDto>(`${resourceUrl}/${slug}`)
      .pipe(map(dto => seasonMapper.toModel(dto)));
  }

  getCurrent(): Observable<Season> {
    return this.http.get<SeasonDto>(`${resourceUrl}/current`)
      .pipe(map(dto => seasonMapper.toModel(dto)));
  }

  save(model: Season): Observable<Season> {
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => seasonMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<SeasonDto>(`${resourceUrl}/${dto.slug}`, dto)
          : this.http.post<SeasonDto>(resourceUrl, dto)),
        map(dto => seasonMapper.toModel(dto)));
  }

  delete(slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl}/${slug}`);
  }

}
