import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-division-list.module').then(module => module.AdminDivisionListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-division-edit.module').then(module => module.AdminDivisionEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-division-edit.module').then(module => module.AdminDivisionEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-division-edit.module').then(module => module.AdminDivisionEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminDivisionRoutingModule {
}
