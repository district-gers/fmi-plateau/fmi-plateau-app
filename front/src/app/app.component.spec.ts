import { async, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { ServiceWorkerModule } from '@angular/service-worker';
import { CookiesBrowserService } from '@common/cookies/cookies.browser.service';
import { CookiesModule } from '@common/cookies/cookies.module';
import { QueryParamsAppendService } from '@common/query-params-append.service';
import { RouterExtService } from '@common/routing/router-ext.service';
import { TranslateLoaderMockFactory } from '@common/translate/translate.loader.mock.factory';
import { LayoutModule } from '@components/layout/layout.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Angulartics2Module } from 'angulartics2';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NoopAnimationsModule,
        ServiceWorkerModule.register('', { enabled: false }),
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useFactory: TranslateLoaderMockFactory }
        }),
        MatSnackBarModule,
        Angulartics2Module.forRoot(),
        CookiesModule.forRoot(CookiesBrowserService),
        LayoutModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        RouterExtService,
        {
          provide: QueryParamsAppendService,
          useValue: {}
        }
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
