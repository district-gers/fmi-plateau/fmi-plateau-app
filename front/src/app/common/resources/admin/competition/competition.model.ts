export interface CompetitionDto {
  slug?: string;
  name: string;
  categorySlug: string;
  categoryName?: string;
}

export interface Competition {
  slug?: string;
  name: string;
  category: string;
  categoryName?: string;
}
