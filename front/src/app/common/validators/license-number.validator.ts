import { Validators } from '@angular/forms';

export const licenseNumberValidator = Validators.pattern(/^(\d{1,3}\s?)(\d{3}\s?){2,}$/);
