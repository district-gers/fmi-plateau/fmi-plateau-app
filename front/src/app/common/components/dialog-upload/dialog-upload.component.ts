import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogUploadData, DialogUploadResult } from '@components/dialog-upload/dialog-upload';
import { FileValidator } from 'ngx-material-file-input';
import { of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-dialog-upload',
  templateUrl: './dialog-upload.component.html',
  styleUrls: [
    './dialog-upload.component.scss',
    './dialog-upload.component.mobile.scss'
  ]
})
export class DialogUploadComponent implements OnInit {

  formGroup: FormGroup;
  loading: boolean;

  constructor(
    public dialogRef: MatDialogRef<DialogUploadComponent, DialogUploadResult>,
    @Inject(MAT_DIALOG_DATA) public data: DialogUploadData,
    @Inject(APP_BASE_HREF) public href: string,
    private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      file: [undefined, [Validators.required, FileValidator.maxContentSize(Math.pow(1024, 2))]]
    });
  }

  upload() {
    const files = this.formGroup.value.file?.files as File[];
    this.loading = true;
    of(files[0])
      .pipe(
        tap(() => this.loading = true),
        switchMap(file => this.data.formSubmitCallback({ file })),
        tap(() => this.loading = false))
      .subscribe(() => this.dialogRef.close(DialogUploadResult.UPLOADED));
  }

  dismiss() {
    this.dialogRef.close(DialogUploadResult.DISMISS);
  }

  close() {
    this.dialogRef.close(DialogUploadResult.CLOSE);
  }

}
