import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Season } from '@resources/admin/season/season.model';
import { SeasonService } from '@resources/admin/season/season.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, filter, map, skip, switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CurrentSeasonService {

  private readonly season$ = new BehaviorSubject<Season>(null);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private season: SeasonService) {
    this.init();
  }

  private init() {
    this.route.queryParams.pipe(map(({ season }) => season))
      .pipe(
        distinctUntilChanged(),
        switchMap((seasonSlug: string) => seasonSlug ? this.season.get(seasonSlug) : this.season.getCurrent()))
      .subscribe(auth => this.season$.next(auth));

    this.get$()
      .pipe(
        skip(1),
        filter(season => season.isCurrent
          ? !!this.route.snapshot.queryParams.season
          : this.route.snapshot.queryParams.season !== season.slug ))
      .subscribe(season => {
        this.router
          .navigate([], { queryParams: { ...this.route.snapshot.queryParams, season: season.isCurrent ? undefined : season.slug } });
      });
  }

  switch(season: Season) {
    this.season$.next(season);
  }

  get$(): Observable<Season> {
    return this.season$.asObservable()
      .pipe(
        filter((season => !!season)),
        distinctUntilChanged((s1, s2) => s1?.slug === s2?.slug));
  }

  get(): Season {
    return this.season$.getValue();
  }

}
