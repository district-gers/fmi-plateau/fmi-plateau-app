import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule } from '@angular/router';
import { AuthInterceptor } from '@common/http-interceptor/auth.interceptor';
import { ErrorNotificationInterceptor } from '@common/http-interceptor/error-notification.interceptor';
import { ErrorRedirectInterceptor } from '@common/http-interceptor/error-redirect.interceptor';
import { MicroCacheInterceptor } from '@common/http-interceptor/micro-cache.interceptor';
import { ReadStateInterceptor } from '@common/http-interceptor/read-state.interceptor';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatSnackBarModule,
    TranslateModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorNotificationInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorRedirectInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ReadStateInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MicroCacheInterceptor,
      multi: true
    }
  ]
})
export class HttpInterceptorModule { }
