import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateHasKeyModule } from '@common/translate/has-key/translate-has-key.module';
import { TranslateModule } from '@ngx-translate/core';

import { CategoryLabelPipe } from './category-label.pipe';

@NgModule({
  declarations: [CategoryLabelPipe],
  imports: [
    CommonModule,
    TranslateModule,
    TranslateHasKeyModule
  ],
  exports: [CategoryLabelPipe],
  providers: [CategoryLabelPipe]
})
export class CategoryLabelModule { }
