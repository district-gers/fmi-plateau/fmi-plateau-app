import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  Input,
  OnChanges, OnDestroy,
  Renderer2,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { Subject } from 'rxjs';
import { distinctUntilChanged, filter, pairwise } from 'rxjs/operators';

@Component({
  selector: 'app-async-select',
  templateUrl: './async-select.component.html',
  styleUrls: ['./async-select.component.scss']
})
export class AsyncSelectComponent implements AfterViewInit, OnChanges, OnDestroy {

  @ViewChild('spinnerRef', { static: true, read: ElementRef }) spinnerElement: ElementRef;
  @ContentChild(MatSelect, { static: true, read: ElementRef }) matSelectElement: ElementRef;

  @Input() loading: boolean;
  @Input() control: AbstractControl;
  @Input() updateDisable = true;

  private loadingChange$ = new Subject<boolean>();

  constructor(private r2: Renderer2) {
    this.loadingChange$
      .pipe(
        distinctUntilChanged(),
        pairwise(),
        filter(([previousLoading, currentLoading]) => previousLoading && !currentLoading))
      .subscribe(() => {
        this.control?.enable();
      });

    this.loadingChange$
      .pipe(
        distinctUntilChanged(),
        filter(loading => loading))
      .subscribe(() => {
        this.control?.disable();
      });
  }

  ngAfterViewInit(): void {
    this.r2.appendChild(this.matSelectElement.nativeElement, this.spinnerElement.nativeElement);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.loading && this.updateDisable) {
      this.loadingChange$.next(this.loading);
    }
  }

  ngOnDestroy(): void {
    this.loadingChange$.complete();
  }

}
