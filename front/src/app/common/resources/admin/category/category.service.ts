import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { categoryMapper } from '@resources/admin/category/category.mapper';
import { Category, CategoryDto } from '@resources/admin/category/category.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = (seasonSlug: string) => `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/categories`;

@Injectable({ providedIn: 'root' })
export class CategoryService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, pageParams?: PageParams): Observable<Page<Category>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>  this.http.get<CategoryDto[]>(resourceUrl(seasonSlug), { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => categoryMapper.toModel(dto)));
  }

  get(seasonSlug: string, slug: string): Observable<Category> {
    return this.http.get<CategoryDto>(`${resourceUrl(seasonSlug)}/${slug}`)
      .pipe(map(dto => categoryMapper.toModel(dto)));
  }

  save(seasonSlug: string, model: Category): Observable<Category> {
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => categoryMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<CategoryDto>(`${resourceUrl(seasonSlug)}/${dto.slug}`, dto)
          : this.http.post<CategoryDto>(resourceUrl(seasonSlug), dto)),
        map(dto => categoryMapper.toModel(dto)));
  }

  delete(seasonSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug)}/${slug}`);
  }

}
