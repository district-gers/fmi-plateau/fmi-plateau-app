import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AsyncSelectModule } from '@components/async-select/async-select.module';
import { DialogConfirmationModule } from '@components/dialog-confirmation/dialog-confirmation.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { ResetButtonIconModule } from '@components/reset-button-icon/reset-button-icon.module';
import { TranslateModule } from '@ngx-translate/core';

import { AdminDivisionListRoutingModule } from './admin-division-list-routing.module';
import { AdminDivisionListComponent } from './admin-division-list.component';


@NgModule({
  declarations: [AdminDivisionListComponent],
  imports: [
    CommonModule,
    AdminDivisionListRoutingModule,
    PageFormModule,
    TranslateModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    MatPaginatorModule,
    DialogConfirmationModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    AsyncSelectModule,
    ResetButtonIconModule
  ]
})
export class AdminDivisionListModule { }
