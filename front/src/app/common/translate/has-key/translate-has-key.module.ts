import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateHasKeyPipe } from './translate-has-key.pipe';

@NgModule({
  declarations: [
    TranslateHasKeyPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TranslateHasKeyPipe
  ],
  providers: [
    TranslateHasKeyPipe
  ]
})
export class TranslateHasKeyModule { }
