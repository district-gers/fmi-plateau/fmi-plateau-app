import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { CurrentUserService } from '@common/current-user.service';
import { Loading } from '@common/loading';
import { difference } from '@common/object.util';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapPageToRouteParams, mapRouteToPageParams } from '@common/pagination/pagination.util';
import { ResponsiveService, SCREEN_SIZES, ScreenSize } from '@common/responsive/responsive.service';
import { Week } from '@common/week';
import { Auth } from '@resources/admin/auth/auth.model';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { Day } from '@resources/admin/day/day.model';
import { DayService } from '@resources/admin/day/day.service';
import { Division } from '@resources/admin/division/division.model';
import { DivisionService } from '@resources/admin/division/division.service';
import { Group } from '@resources/admin/group/group.model';
import { GroupService } from '@resources/admin/group/group.service';
import { Plateau } from '@resources/admin/plateau/plateau.model';
import { PlateauService } from '@resources/admin/plateau/plateau.service';
import { tapUniqueItem } from '@utils/rxjs.util';
import { isEqual } from 'lodash';
import { Moment } from 'moment';
import * as moment from 'moment';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  finalize,
  map,
  pairwise,
  startWith,
  switchMap,
  takeUntil,
  tap
} from 'rxjs/operators';

interface FilterPageParams extends PageParams {
  category?: string;
  competition?: string;
  division?: string;
  group?: string;
  day?: string;
  startDate?: string;
  endDate?: string;
}

@Component({
  selector: 'app-plateau-list',
  templateUrl: './plateau-list.component.html',
  styleUrls: [
    './plateau-list.component.scss',
    './plateau-list.component.mobile.scss'
  ]
})
export class PlateauListComponent implements OnInit, OnDestroy {

  plateaux$: Observable<Page<Plateau>>;
  ghostElements$: Observable<number[]>;
  user$: Observable<Auth>;

  categories$: Observable<Page<Category>>;
  competitions$: Observable<Page<Competition>>;
  divisions$: Observable<Page<Division>>;
  groups$: Observable<Page<Group>>;
  days$: Observable<Page<Day>>;
  formGroup: FormGroup;
  screenSize$: Observable<ScreenSize>;
  weeks$: Observable<Week[]>;
  SCREEN_SIZES = SCREEN_SIZES;
  loading: Loading = { list: false };

  protected destroy$ = new Subject<void>();
  protected refresh$ = new Subject<PageParams>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private currentUser: CurrentUserService,
    private currentSeason: CurrentSeasonService,
    private plateau: PlateauService,
    private category: CategoryService,
    private competition: CompetitionService,
    private division: DivisionService,
    private group: GroupService,
    private day: DayService,
    private responsive: ResponsiveService) { }

  ngOnInit(): void {
    this.user$ = this.currentUser.get$();
    this.screenSize$ = this.responsive.size$;

    const season$ = this.currentSeason.get$();

    this.plateaux$ = combineLatest([
      this.route.queryParams.pipe(
        map(({ page, size, sort }) => ({ page, size, sort })),
        mapRouteToPageParams(20),
        distinctUntilChanged(isEqual)),
      this.route.queryParams.pipe(
        map(({ category, competition, division, group, day, startDate, endDate }) => ({
          category, competition, division, group, day,
          startDate: startDate && moment(startDate).isValid() ? moment(startDate).utc().format() : undefined,
          endDate: endDate && moment(endDate).isValid() ? moment(endDate).utc().format() : undefined
        })),
        map((filterParams) => Object.entries(filterParams)
          .filter(([, value]) => !!value)
          .reduce((acc, [key, value]) => ({ ...acc, [key]: value}), {})),
        distinctUntilChanged(isEqual)),
      this.currentSeason.get$(),
      this.currentUser.get$()
    ])
      .pipe(
        debounceTime(500),
        tap(() => this.loading.list = true),
        switchMap(([pageParams, otherParams, season]) => this.plateau.find(season.slug, { ...pageParams, ...otherParams })),
        tap(() => this.loading.list = false),
        finalize(() => this.loading.list = false));

    this.ghostElements$ = this.screenSize$
      .pipe(
        map(screenSize => {
          switch (true) {
            case screenSize.index <= SCREEN_SIZES.xs.index:
              return Array(2);
            case screenSize === SCREEN_SIZES.sm:
              return Array(4);
            case screenSize === SCREEN_SIZES.md:
              return Array(6);
            default:
              return Array(8);
          }
        }),
        map(array => array.fill(null).map((v, index) => index)));

    this.refresh$
      .pipe(debounceTime(50), mapPageToRouteParams())
      .subscribe(queryParams => {
        this.router.navigate([], { queryParams, queryParamsHandling: 'merge', replaceUrl: true, relativeTo: this.route });
      });

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.category = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().category && this.categoryChange(slug)),
        finalize(() => this.loading.category = false));

    this.competitions$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { category }]) => ({ season, categorySlug: category })),
        filter(({ season, categorySlug }) => season && categorySlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.competition = true),
        switchMap(({ season, categorySlug }) => this.competition.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.competition = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().competition && this.competitionChange(slug)),
        finalize(() => this.loading.competition = false));

    this.divisions$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { competition }]) => ({ season, competitionSlug: competition })),
        filter(({ season, competitionSlug }) => season && competitionSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.division = true),
        switchMap(({ season, competitionSlug }) => this.division.find(season.slug, competitionSlug)
          .pipe(finalize(() => this.loading.division = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().division && this.divisionChange(slug)),
        finalize(() => this.loading.division = false));

    this.groups$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { competition, division }]) => ({ season, competitionSlug: competition, divisionSlug: division })),
        filter(({ season, competitionSlug, divisionSlug }) => season && competitionSlug && divisionSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.group = true),
        switchMap(({ season, competitionSlug, divisionSlug }) => this.group.find(season.slug, competitionSlug, divisionSlug)
          .pipe(finalize(() => this.loading.group = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().group && this.groupChange(slug)),
        finalize(() => this.loading.group = false));

    this.days$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { competition, division, group }]) => ({
          season, competitionSlug: competition, divisionSlug: division, groupSlug: group })),
        filter(({ season, competitionSlug, divisionSlug, groupSlug }) => season && competitionSlug && divisionSlug && groupSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.day = true),
        switchMap(({ season, competitionSlug, divisionSlug, groupSlug }) => this.day
          .find(season.slug, competitionSlug, divisionSlug, groupSlug)
          .pipe(finalize(() => this.loading.day = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().day && this.dayChange(slug)),
        finalize(() => this.loading.day = false));


    const weeksWindowSize = 64;
    this.weeks$ = of(
      Array(weeksWindowSize).fill(null)
        .reduce(
          (weeks: Moment[]): Moment[] => [...weeks, weeks[weeks.length - 1].clone().add(1, 'week')],
          [moment().startOf('isoWeek').add(- weeksWindowSize + 12, 'weeks')])
        .map((week: Moment) => ({
          week: week.isoWeek(),
          start: week,
          end: week.clone().endOf('isoWeek')
        }))
        .sort((week1, week2) => week2.start - week1.start));

    this.formGroup = this.fb.group({
      category: [],
      competition: [],
      division: [],
      group: [],
      day: [],
      week: [],
      startDate: [],
      endDate: []
    });
    this.formGroup.controls.competition.disable();
    this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();

    this.route.queryParams
      .pipe(
        takeUntil(this.destroy$),
        startWith({} as FilterPageParams),
        map(({ category, competition, division, group, day, startDate, endDate }) => ({
          category, competition, division, group, day,
          startDate: startDate && moment(startDate).isValid() ? moment(startDate) : undefined,
          endDate: endDate && moment(endDate).isValid() ? moment(endDate) : undefined
        })),
        pairwise(),
        map(([previous, current]) => difference(current, previous)),
        distinctUntilChanged(isEqual))
      .subscribe(changes => this.formGroup.patchValue({
        ...changes,
        ...(changes.hasOwnProperty('startDate') || changes.hasOwnProperty('endDate')) && {
          week: changes.startDate?.isSame(changes.startDate.clone().startOf('isoWeek'), 'second')
          && changes.endDate?.isSame(changes.endDate.clone().endOf('isoWeek'), 'second')
            ? { start: changes.startDate, end: changes.endDate }
            : undefined
        }
      }));

    this.formGroup.controls.startDate.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        map((date: Moment) => date?.isValid() ? date.clone().utc().format() : undefined))
      .subscribe((startDate: string) => this.refresh$.next({ startDate } as FilterPageParams));

    this.formGroup.controls.endDate.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        map((date: Moment) => date?.isValid() ? date.clone().endOf('day').utc().format() : undefined))
      .subscribe((endDate: string) => this.refresh$.next({ endDate } as FilterPageParams));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.refresh$.complete();
  }

  categoryChange(category: string) {
    this.refresh$.next({ category, competition: undefined, division: undefined, group: undefined, day: undefined } as FilterPageParams);
    category ? this.formGroup.controls.competition.enable() : this.formGroup.controls.competition.disable();
    this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();
  }

  competitionChange(competition: string) {
    this.refresh$.next({ competition, division: undefined, group: undefined, day: undefined } as FilterPageParams);
    competition ? this.formGroup.controls.division.enable() : this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();
  }

  divisionChange(division: string) {
    this.refresh$.next({ division, group: undefined, day: undefined } as FilterPageParams);
    division ? this.formGroup.controls.group.enable() : this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();
  }

  groupChange(group: string) {
    this.refresh$.next({ group, day: undefined } as FilterPageParams);
    group ? this.formGroup.controls.day.enable() : this.formGroup.controls.day.disable();
  }

  dayChange(day: string) {
    this.refresh$.next({ day } as FilterPageParams);
  }

  weekChange(week: Week) {
    this.refresh$.next({
      startDate: week && week.start.clone().utc().format(),
      endDate: week && week.end.clone().utc().format() } as FilterPageParams);
  }

  trackBySlug(index, item): string {
    return item.slug;
  }

  compareByWeek(w1: Week, w2: Week): boolean {
    return !w1 && !w2 || w1?.start?.isSame(w2?.start, 'second') && w1?.end?.isSame(w2?.end, 'second');
  }

}
