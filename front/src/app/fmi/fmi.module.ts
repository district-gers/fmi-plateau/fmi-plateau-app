import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FmiRoutingModule } from './fmi-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FmiRoutingModule
  ]
})
export class FmiModule { }
