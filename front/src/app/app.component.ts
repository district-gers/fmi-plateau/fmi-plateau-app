import { CdkScrollable, ScrollDispatcher } from '@angular/cdk/overlay';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Component, Inject, NgZone, OnDestroy, OnInit, Optional, PLATFORM_ID, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Event, NavigationEnd, Router } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { CookiesService } from '@common/cookies/cookies.service';
import { QueryParamsAppendService } from '@common/query-params-append.service';
import { RouterExtService } from '@common/routing/router-ext.service';
import { LayoutService } from '@components/layout/layout.service';
import { TranslateService } from '@ngx-translate/core';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { combineLatest, fromEvent, merge, Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, mapTo, mergeMap, pairwise, startWith, take, takeUntil, throttleTime } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  @ViewChild(CdkScrollable) cdkScrollable: CdkScrollable;
  private destroy$ = new Subject<void>();

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    @Inject(DOCUMENT) private document: Document,
    @Optional() private swUpdate: SwUpdate,
    private scrollDispatcher: ScrollDispatcher,
    private ngZone: NgZone,
    private router: Router,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    private angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics,
    private layout: LayoutService,
    private cookies: CookiesService,
    private routerExt: RouterExtService,
    private queryParamsAppend: QueryParamsAppendService) {

    // just to instantiate service on startup
    this.routerExt.getPreviousUrl();
    this.queryParamsAppend.valueOf();
  }

  ngOnInit(): void {
    this.angulartics2GoogleAnalytics.startTracking();

    const navigationEnd$ = this.router.events
      .pipe(
        takeUntil(this.destroy$),
        filter((event: Event) => event instanceof NavigationEnd),
        map((event: Event) => event as NavigationEnd));

    if (isPlatformBrowser(this.platformId)) {
      this.swUpdate.available
        .pipe(
          takeUntil(this.destroy$),
          take(1),
          mergeMap(({ available: { appData } }) => this.swUpdate.activateUpdate().then(() => appData)),
          mergeMap((appData) => combineLatest([
            // tslint:disable-next-line:no-string-literal
            this.translate.get('app.pwa.notification.update.message', { version: appData['version'] }).pipe(take(1)),
            this.translate.get('app.pwa.notification.update.action').pipe(take(1))
          ])),
          mergeMap(([message, action]) => this.snackBar.open(message, action, { duration: 10000 }).onAction()))
        .subscribe(() => this.document.location.reload());

      this.cookies.allow();

      navigationEnd$
        .pipe(
          map((event: NavigationEnd) => event.url.split('?')[0]),
          distinctUntilChanged(),
          map(() => this.cdkScrollable.getElementRef().nativeElement))
        .subscribe((element: Element) => element.scrollTo ? element.scrollTo(0, 0) : element.scrollTop = 0);

      navigationEnd$
        .subscribe(() => this.layout.setConfig({ hideToolBar: false }));

      this.scrollDispatcher.scrolled()
        .pipe(
          takeUntil(this.destroy$),
          map((event: CdkScrollable) => event.getElementRef().nativeElement.scrollTop),
          startWith(0),
          throttleTime(50),
          pairwise(),
          filter(([previousY, currentY]: number[]) => Math.abs(currentY - previousY) > 16),
          map(([previousY, currentY]: number[]) => Math.sign(currentY - previousY)),
          distinctUntilChanged(),
          filter(scrollingDown => scrollingDown < 0))
        .subscribe(() => {
          this.ngZone.run(() => {
            this.layout.setConfig({ hideToolBar: false });
          });
        });

      merge(
        this.layout.getConfig()
          .pipe(
            takeUntil(this.destroy$),
            filter(({ hideToolBar }) => !hideToolBar),
            mapTo(-1)),
        this.scrollDispatcher.scrolled()
          .pipe(
            takeUntil(this.destroy$),
            map((event: CdkScrollable) => event.getElementRef().nativeElement.scrollTop),
            throttleTime(200),
            startWith(0),
            pairwise(),
            filter(([previousY, currentY]: number[]) => Math.abs(currentY - previousY) > 16 * 4),
            map(([previousY, currentY]: number[]) => Math.sign(currentY - previousY))))
          .pipe(
            distinctUntilChanged(),
            filter(scrollingDown => scrollingDown > 0))
          .subscribe(() => {
            this.ngZone.run(() => {
              this.layout.setConfig({ hideToolBar: true });
            });
          });

      fromEvent(document, 'mousemove')
        .pipe(
          takeUntil(this.destroy$),
          throttleTime(50),
          filter((event: MouseEvent) => event.clientY < 2 * 16))
        .subscribe(() => this.layout.setConfig({ hideToolBar: false }));
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
