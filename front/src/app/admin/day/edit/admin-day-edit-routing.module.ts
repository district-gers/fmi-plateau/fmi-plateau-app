import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminDayEditComponent } from './admin-day-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminDayEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminDayEditRoutingModule { }
