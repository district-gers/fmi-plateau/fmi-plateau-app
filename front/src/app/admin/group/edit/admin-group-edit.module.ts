import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { AsyncSelectModule } from '@components/async-select/async-select.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';
import { TeamNameModule } from '@pipes/team-name/team-name.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { AdminGroupEditRoutingModule } from './admin-group-edit-routing.module';
import { AdminGroupEditComponent } from './admin-group-edit.component';


@NgModule({
  declarations: [AdminGroupEditComponent],
  imports: [
    CommonModule,
    AdminGroupEditRoutingModule,
    PageFormModule,
    TranslateModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    LazyLoadImageModule,
    TeamNameModule,
    AsyncButtonModule,
    AsyncSelectModule
  ]
})
export class AdminGroupEditModule { }
