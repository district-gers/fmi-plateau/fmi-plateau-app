import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminClubRoutingModule } from './admin-club-routing.module';


@NgModule({
  imports: [
    CommonModule,
    AdminClubRoutingModule
  ]
})
export class AdminClubModule { }
