import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminSeasonRoutingModule } from './admin-season-routing.module';


@NgModule({
  imports: [
    CommonModule,
    AdminSeasonRoutingModule
  ]
})
export class AdminSeasonModule { }
