import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Observable, Subject } from 'rxjs';
import { map, mergeMap, startWith, switchMap, take } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

const extraFilterGenerator = (formGroup: FormGroup, playerCategoryFormGroup: FormGroup) => (playerCategory: string) => {
  const selectedCategories: string[] = (formGroup.value.playerCategories as { category }[])
    .map(({ category }) => category)
    .filter(category => !!category);
  return playerCategoryFormGroup.value.category === playerCategory
    || !selectedCategories.some(selectedCategory => selectedCategory === playerCategory);
};

@Component({
  selector: 'app-admin-category-edit',
  templateUrl: './admin-category-edit.component.html',
  styleUrls: [
    './admin-category-edit.component.scss',
    './admin-category-edit.component.mobile.scss'
  ]
})
export class AdminCategoryEditComponent extends AbstractAdminEditComponent<Category> implements OnInit, OnDestroy {

  formGroup: FormGroup;
  metaLabelsPrefix = 'admin.category.edit.seo';
  limits = Array(10).fill(null).map((_, index) => index - 1).filter(limit => !!limit);
  private playerCategories$: Observable<string[]>;

  private playerCategoryChange$ = new Subject<void>();

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private category: CategoryService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.playerCategories$ = this.translate.get('common.category')
      .pipe(map(obj => Object.keys(obj).sort()));

    this.formGroup = this.fb.group({
      slug: [],
      name: ['', Validators.required],
      playerCategories: this.fb.array([])
    });
    this.addPlayerCategory(this.formGroup);

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => this.currentSeason.get$().pipe(take(1), mergeMap(season => this.category.get(season.slug, slug)))))
      .subscribe(category => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? category.slug : undefined,
          name: category.name
        });

        const playerCategoriesFormArray = this.formGroup.controls.playerCategories as FormArray;
        playerCategoriesFormArray.clear();
        Object.entries(category.rules.playerCategories)
          .sort(([playerCategory1], [playerCategory2]) => playerCategory1.toLocaleLowerCase()
            .localeCompare(playerCategory2.toLocaleLowerCase()))
          .forEach(([playerCategory, limit]) => this.addPlayerCategory(this.formGroup, playerCategory, limit));
        this.addPlayerCategory(this.formGroup);
        this.playerCategoryChange$.next(null);
      });
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.playerCategoryChange$.complete();
  }

  protected save$(): Observable<[Category]> {
    const value = this.formGroup.getRawValue();
    return this.category.save(this.currentSeason.get().slug, {
          slug: value.slug,
          name: value.name,
          rules: {
            playerCategories: value.playerCategories
              .filter(playerCategory => playerCategory.category)
              .reduce((acc, playerCategory) => ({ ...acc, [playerCategory.category]: playerCategory.limit }), {})
          }
        })
      .pipe(
        map(category => [category]));
  }

  addPlayerCategory(formGroup: FormGroup, category?: string, limit?: number) {
    const playerCategoriesFormArray = formGroup.controls.playerCategories as FormArray;
    const playerCategoryFormGroup = this.fb.group({
      categories: [],
      category: ['', Validators.required],
      limit: [-1, Validators.required]
    });
    const extraFilterFn = extraFilterGenerator(this.formGroup, playerCategoryFormGroup);
    playerCategoryFormGroup.patchValue({
      categories: this.playerCategoryChange$
        .pipe(
          startWith(''),
          switchMap(() => this.playerCategories$),
          map(playerCategories => playerCategories.filter(extraFilterFn))
        ),
      ...category && { category, limit }
    });
    playerCategoriesFormArray.push(playerCategoryFormGroup);
  }

  controlAsFormGroups(control: AbstractControl): FormGroup[] {
    return (control as FormArray).controls as FormGroup[];
  }

  deletePlayerCategory(formGroup: FormGroup, index: number): void {
    (formGroup.controls.playerCategories as FormArray).removeAt(index);
    this.playerCategoryChange$.next(null);
  }

  trackByCategoryValue(index: number, playerCategoryFormGroup: FormGroup) {
    return playerCategoryFormGroup.value.category;
  }
}
