import { Inject, Injectable } from '@angular/core';
import { LocalStorage } from '@common/local-storage/local-storage';

const storageKey = 'token';

@Injectable({ providedIn: 'root' })
export class AuthStorageService {

  constructor(
    @Inject(LocalStorage) private localStorage: Storage) {
  }

  storeToken(token: string): void {
    this.localStorage?.setItem?.('token', token);
  }

  getToken(): string {
    return this.localStorage?.getItem?.(storageKey);
  }

}
