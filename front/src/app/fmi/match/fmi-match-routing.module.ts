import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FmiMatchComponent } from './fmi-match.component';

const routes: Routes = [
  {
    path: '',
    component: FmiMatchComponent,
    data: {
      meta: {
        title: 'fmi.match.seo.title',
        description: 'fmi.match.seo.description',
        'twitter:title': 'fmi.match.seo.title',
        'twitter:description': 'fmi.match.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FmiMatchRoutingModule { }
