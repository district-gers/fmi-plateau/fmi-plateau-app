import * as winston from 'winston';
import * as expressWinston from 'express-winston';
import { configService } from '../config/config.service';
const { combine, timestamp, colorize, simple } = winston.format;

export const logger = winston.createLogger({
  level: configService.config.logger.level,
  format: combine(
    colorize(),
    timestamp(),
    simple()
  ),
  transports: [
    new winston.transports.Console()
  ]
});

export const expressLogger = expressWinston.logger({
  winstonInstance: logger,
  msg: 'HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}'
});
