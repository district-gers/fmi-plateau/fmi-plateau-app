import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminPlayerListComponent } from './admin-player-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminPlayerListComponent,
    data: {
      meta: {
        title: 'admin.player.list.seo.title',
        description: 'admin.player.list.seo.description',
        'twitter:title': 'admin.player.list.seo.title',
        'twitter:description': 'admin.player.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPlayerListRoutingModule { }
