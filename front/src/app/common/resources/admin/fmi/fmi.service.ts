import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { BlobFile, extractBlobFileName } from '@common/save-file.util';
import { fmiMapper } from '@resources/admin/fmi/fmi.mapper';
import { Fmi, FmiDto } from '@resources/admin/fmi/fmi.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = (seasonSlug: string) => `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/fmis`;

export interface QueryPageParams extends PageParams {
  category?: string;
  competition?: string;
  division?: string;
  group?: string;
  day?: string;
  participant?: string;
  startDate?: string;
  endDate?: string;
}

@Injectable({ providedIn: 'root' })
export class FmiService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, pageParams?: QueryPageParams): Observable<Page<Fmi>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(true),
        mergeMap(params =>  this.http.get<FmiDto[]>(resourceUrl(seasonSlug), { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => fmiMapper.toModel(dto)));
  }

  get(seasonSlug: string, slug: string): Observable<Fmi> {
    return this.http.get<FmiDto>(`${resourceUrl(seasonSlug)}/${slug}`)
      .pipe(map(dto => fmiMapper.toModel(dto)));
  }

  getByPlateau(seasonSlug: string, plateauSlug: string): Observable<Fmi> {
    return this.http.get<FmiDto[]>(`${resourceUrl(seasonSlug)}`, { params: { plateau: plateauSlug } })
      .pipe(
        map(([dto]) => fmiMapper.toModel(dto)));
  }

  save(seasonSlug: string, model: Fmi): Observable<Fmi> {
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => fmiMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<FmiDto>(`${resourceUrl(seasonSlug)}/${dto.slug}`, dto)
          : this.http.post<FmiDto>(resourceUrl(seasonSlug), dto)),
        map(dto => fmiMapper.toModel(dto)));
  }

  delete(seasonSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug)}/${slug}`);
  }

  pdf(seasonSlug: string, slug: string, defaultFileName?: string): Observable<BlobFile> {
    return this.http.get(`${resourceUrl(seasonSlug)}/${slug}/pdf`, { responseType: 'blob', observe: 'response' })
      .pipe(extractBlobFileName(defaultFileName));
  }

}
