import { Pipe, PipeTransform } from '@angular/core';
import { Auth } from '@resources/admin/auth/auth.model';
import { Right } from '@resources/user/right';
import { userHasRights } from '@utils/user-right.util';

@Pipe({
  name: 'userHasRight'
})
export class UserHasRightPipe implements PipeTransform {

  transform(user: Auth, ...rights: Right[]): boolean {
    return userHasRights(user, ...rights);
  }

}
