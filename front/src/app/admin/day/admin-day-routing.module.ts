import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-day-list.module').then(module => module.AdminDayListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-day-edit.module').then(module => module.AdminDayEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-day-edit.module').then(module => module.AdminDayEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-day-edit.module').then(module => module.AdminDayEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminDayRoutingModule {
}
