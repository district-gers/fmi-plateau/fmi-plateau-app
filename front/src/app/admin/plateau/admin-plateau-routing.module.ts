import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-plateau-list.module').then(module => module.AdminPlateauListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-plateau-edit.module').then(module => module.AdminPlateauEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-plateau-edit.module').then(module => module.AdminPlateauEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-plateau-edit.module').then(module => module.AdminPlateauEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPlateauRoutingModule {
}
