export interface PlayerDto {
  slug?: string;
  firstName: string;
  lastName: string;
  code: number;
  category: string;
  clubSlug: string;
  clubName?: string;
  clubShortName?: string;
  clubLogoUrl?: string;
}

export interface Player {
  slug?: string;
  firstName: string;
  lastName: string;
  code: number;
  category: string;
  club: string;
  clubName?: string;
  clubShortName?: string;
  clubLogoUrl?: string;
}
