import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ErrorNotFoundComponent } from './error-not-found.component';

const routes: Routes = [
  {
    path: '',
    component: ErrorNotFoundComponent,
    data: {
      meta: {
        title: 'error.notFound.seo.title',
        description: 'error.notFound.seo.description',
        'twitter:title': 'error.notFound.seo.title',
        'twitter:description': 'error.notFound.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorNotFoundRoutingModule { }
