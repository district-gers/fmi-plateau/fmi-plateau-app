import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { educatorMapper } from '@resources/admin/educator/educator.mapper';
import { Educator, EducatorDto } from '@resources/admin/educator/educator.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = (seasonSlug: string) => `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/educators`;

interface Criteria {
  clubs?: string[];
  participants?: string[];
}

const stringifyCriteria = (criteria: Criteria): { [key: string]: any; } => ({
  ...criteria,
  ...criteria && Object.entries(criteria)
    .filter(([, value]) => Array.isArray(value) && value?.length)
    .map(([key, value]): [string, string] => [key, value.join(',')])
    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {})
});

@Injectable({ providedIn: 'root' })
export class EducatorService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, criteria: Criteria, pageParams?: PageParams): Observable<Page<Educator>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>  this.http.get<EducatorDto[]>(resourceUrl(seasonSlug), {
          params: { ...params, ...stringifyCriteria(criteria) },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => educatorMapper.toModel(dto)));
  }

  findAllByParticipant(seasonSlug: string, participantSlug: string, pageParams?: PageParams): Observable<Page<Educator>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>  this.http.get<EducatorDto[]>(resourceUrl(seasonSlug), {
          params: { ...params, participant: participantSlug },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => educatorMapper.toModel(dto)));
  }

  get(seasonSlug: string, slug: string): Observable<Educator> {
    return this.http.get<EducatorDto>(`${resourceUrl(seasonSlug)}/${slug}`)
      .pipe(map(dto => educatorMapper.toModel(dto)));
  }

  save(seasonSlug: string, model: Educator): Observable<Educator> {
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => educatorMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<EducatorDto>(`${resourceUrl(seasonSlug)}/${dto.slug}`, dto)
          : this.http.post<EducatorDto>(resourceUrl(seasonSlug), dto)),
        map(dto => educatorMapper.toModel(dto)));
  }

  delete(seasonSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug)}/${slug}`);
  }

}
