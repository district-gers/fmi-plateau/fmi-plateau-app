import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { dayMapper } from '@resources/admin/day/day.mapper';
import { Day, DayDto } from '@resources/admin/day/day.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = (seasonSlug: string, competitionSlug: string, divisionSlug: string, groupSlug: string) =>
  // tslint:disable-next-line:max-line-length
  `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/competitions/${competitionSlug}/divisions/${divisionSlug}/groups/${groupSlug}/days`;

@Injectable({ providedIn: 'root' })
export class DayService {

  constructor(
    private http: HttpClient) {
  }

  // tslint:disable-next-line:max-line-length
  find(seasonSlug: string, competitionSlug: string, divisionSlug: string, groupSlug: string, pageParams?: PageParams): Observable<Page<Day>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params => this.http
          .get<DayDto[]>(resourceUrl(seasonSlug, competitionSlug, divisionSlug, groupSlug), { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => dayMapper.toModel(dto)));
  }

  get(seasonSlug: string, competitionSlug: string, divisionSlug: string, groupSlug: string, slug: string): Observable<Day> {
    return this.http.get<DayDto>(`${resourceUrl(seasonSlug, competitionSlug, divisionSlug, groupSlug)}/${slug}`)
      .pipe(map(dto => dayMapper.toModel(dto)));
  }

  save(seasonSlug: string, competitionSlug: string, divisionSlug: string, groupSlug: string, model: Day): Observable<Day> {
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => dayMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<DayDto>(`${resourceUrl(seasonSlug, competitionSlug, divisionSlug, groupSlug)}/${dto.slug}`, dto)
          : this.http.post<DayDto>(resourceUrl(seasonSlug, competitionSlug, divisionSlug, groupSlug), dto)),
        map(dto => dayMapper.toModel(dto)));
  }

  delete(seasonSlug: string, competitionSlug: string, divisionSlug: string, groupSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug, competitionSlug, divisionSlug, groupSlug)}/${slug}`);
  }

}
