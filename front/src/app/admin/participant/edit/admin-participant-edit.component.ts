import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page } from '@common/pagination/pagination';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Club } from '@resources/admin/club/club.model';
import { ClubService } from '@resources/admin/club/club.service';
import { Participant } from '@resources/admin/participant/participant.model';
import { ParticipantService } from '@resources/admin/participant/participant.service';
import { PictureService } from '@resources/admin/picture/picture.service';
import { TeamService } from '@resources/admin/team/team.service';
import { combineLatest, Observable } from 'rxjs';
import { filter, finalize, map, mergeMap, pairwise, startWith, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

const COUNT_MAX_OFFSET = 10;

@Component({
  selector: 'app-admin-participant-edit',
  templateUrl: './admin-participant-edit.component.html',
  styleUrls: ['./admin-participant-edit.component.scss']
})
export class AdminParticipantEditComponent extends AbstractAdminEditComponent<Participant> implements OnInit {

  formGroup: FormGroup;
  logoFormGroup: FormGroup;
  metaLabelsPrefix = 'admin.participant.edit.seo';
  minTeamCount = 0;
  maxTeamCount = COUNT_MAX_OFFSET;

  clubs$: Observable<Page<Club>>;
  categories$: Observable<Page<Category>>;
  screenSize$: Observable<ScreenSize>;
  logoUrls$: Observable<string[]>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private responsive: ResponsiveService,
    private participant: ParticipantService,
    private team: TeamService,
    private club: ClubService,
    private category: CategoryService,
    private picture: PictureService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;

    this.clubs$ = this.currentSeason.get$()
      .pipe(
        take(1),
        tap(() => this.loading.club = true),
        mergeMap(season => this.club.find(season.slug)),
        finalize(() => this.loading.club = false));

    this.categories$ = this.currentSeason.get$()
      .pipe(
        take(1),
        tap(() => this.loading.category = true),
        mergeMap(season => this.category.find(season.slug)),
        finalize(() => this.loading.category = false));

    this.logoUrls$ = this.picture.find();

    this.formGroup = this.fb.group({
      slug: [],
      name: ['', Validators.required],
      shortName: [],
      logo: this.fb.group({
        url: []
      }),
      clubs: [[], [Validators.required, Validators.minLength(1)]],
      category: [undefined, Validators.required],
      teamCount: [1, Validators.max(COUNT_MAX_OFFSET)]
    });
    this.logoFormGroup = this.formGroup.controls.logo as FormGroup;

    this.route.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe(({ category }) => this.formGroup.patchValue({ category }));

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => this.currentSeason.get$()
          .pipe(
            take(1),
            mergeMap(season => combineLatest([
              this.participant.get(season.slug, slug),
              this.team.findByParticipant(season.slug, slug)
            ])))))
      .subscribe(([participant, teams]) => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? participant.slug : undefined,
          name: participant.name,
          shortName: participant.shortName,
          logo: {
            url: participant.logoUrl
          },
          clubs: participant.clubs,
          category: participant.category,
          teamCount: teams.count
        });
        this.formGroup.controls.teamCount.setValidators([
          Validators.min(teams.count),
          Validators.max(teams.count + COUNT_MAX_OFFSET)
        ]);
        this.minTeamCount = teams.count;
        this.maxTeamCount = teams.count + COUNT_MAX_OFFSET;
      });

    if (this.mode === EditMode.UPDATE) {
      this.formGroup.controls.category.disable();
    }

    if (this.mode === EditMode.CREATE) {
      this.formGroup.controls.clubs.valueChanges
        .pipe(
          startWith<Club[], Club[]>(this.formGroup.controls.clubs.value),
          map(clubs => clubs as Club[]),
          pairwise(),
          filter(([previousValue, currentValue]) => !previousValue?.length && currentValue.length > 0),
          filter(() => !this.formGroup.value.name && !this.formGroup.value.shortName && !this.formGroup.value.logo?.url),
          map(([, clubs]) => clubs),
          map(clubs => clubs[0]))
        .subscribe(club => this.formGroup.patchValue({
          name: club.name,
          shortName: club.shortName,
          logo: {
            url: club.logoUrl
          }
        }));
    }
  }

  protected save$(): Observable<[Participant, Link]> {
    const value = this.formGroup.getRawValue();
    const { category, teamCount } = this.formGroup.getRawValue();
    return this.participant.save(this.currentSeason.get().slug, {
      slug: value.slug,
      name: value.name,
      shortName: value.shortName,
      logoUrl: value.logo.url,
      clubs: (value.clubs as Club[])?.map(({ slug }) => ({ slug })),
      category: value.category
    }, { teamCount })
      .pipe(
        tap(() => this.previousRouteCommands = this.getPreviousRouteCommands({ category })),
        map(participant => [participant, {
          commands: ['teams'],
          queryParams: { category, participant: participant.slug },
          label: this.translate.instant('admin.participant.edit.action.seeTeams')
        }]));
  }

}
