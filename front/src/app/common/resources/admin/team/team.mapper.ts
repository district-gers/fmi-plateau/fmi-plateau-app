import { Team, TeamDto } from '@resources/admin/team/team.model';

class TeamMapper {

  toModel(dto: TeamDto): Team {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      index: dto.index,
      participant: dto.participantSlug,
      name: dto.participantName,
      shortName: dto.participantShortName,
      logoUrl: dto.participantLogoUrl,
      category: dto.categorySlug,
      categoryName: dto.categoryName
    };
  }

  toDto(model: Team): TeamDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('index') && {
        index: model.index
      },
      ...model.hasOwnProperty('participant') && {
        participantSlug: model.participant
      }
    };
  }

}

export const teamMapper = new TeamMapper();
