import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { SignaturePadModule } from 'angular2-signaturepad';

import { SignatureControlComponent } from './signature-control.component';



@NgModule({
  declarations: [SignatureControlComponent],
  exports: [
    SignatureControlComponent
  ],
  imports: [
    CommonModule,
    SignaturePadModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class SignatureControlModule { }
