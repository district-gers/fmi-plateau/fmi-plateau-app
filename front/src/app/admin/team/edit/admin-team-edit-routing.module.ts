import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminTeamEditComponent } from './admin-team-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminTeamEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminTeamEditRoutingModule { }
