import { NgModule } from '@angular/core';
import { FlexLayoutServerModule } from '@angular/flex-layout/server';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';
import { CookiesModule } from '@common/cookies/cookies.module';
import { CookiesServerService } from '@common/cookies/cookies.server.service';
import { LocalStorage } from '@common/local-storage/local-storage';
import { inMemoryLocalStorage } from '@common/local-storage/local-storage.server';
import { TranslateLoaderServerService } from '@common/translate/translate-loader.server.service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { AppComponent } from './app.component';
import { AppModule } from './app.module';

@NgModule({
  imports: [
    AppModule,
    ServerModule,
    ServerTransferStateModule,
    TranslateModule.forRoot({
      defaultLanguage: 'fr',
      loader: {
        provide: TranslateLoader,
        useClass: TranslateLoaderServerService
      }
    }),
    FlexLayoutServerModule,
    CookiesModule.forRoot(CookiesServerService)
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: LocalStorage,
      useValue: inMemoryLocalStorage
    }
  ]
})
export class AppServerModule {}
