import { Pipe, PipeTransform } from '@angular/core';
import { TranslateHasKeyPipe } from '@common/translate/has-key/translate-has-key.pipe';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'categoryLabel'
})
export class CategoryLabelPipe implements PipeTransform {

  constructor(
    private translate: TranslateService,
    private translateHasKeyPipe: TranslateHasKeyPipe) {
  }

  transform(categoryCode: string): string {
    const labelKey = `common.category.${categoryCode}`;
    return this.translateHasKeyPipe.transform(labelKey) ? this.translate.instant(labelKey) : categoryCode;
  }

}
