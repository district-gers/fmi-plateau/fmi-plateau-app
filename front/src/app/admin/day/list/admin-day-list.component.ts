import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { emptyPage, Page, PageParams } from '@common/pagination/pagination';
import { ResponsiveService } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { Day } from '@resources/admin/day/day.model';
import { DayService } from '@resources/admin/day/day.service';
import { Division } from '@resources/admin/division/division.model';
import { DivisionService } from '@resources/admin/division/division.service';
import { Group } from '@resources/admin/group/group.model';
import { GroupService } from '@resources/admin/group/group.service';
import { tapUniqueItem } from '@utils/rxjs.util';
import { isEqual } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

interface FilterPageParams extends PageParams {
  competition?: string;
  division?: string;
  group?: string;
}

@Component({
  selector: 'app-admin-day-list',
  templateUrl: './admin-day-list.component.html',
  styleUrls: [
    './admin-day-list.component.scss',
    './admin-day-list.component.responsive.scss'
  ]
})
export class AdminDayListComponent extends AbstractAdminListComponent<Day> implements OnInit {

  columns = ['index', 'date', 'action'];
  categories$: Observable<Page<Category>>;
  competitions$: Observable<Page<Competition>>;
  divisions$: Observable<Page<Division>>;
  groups$: Observable<Page<Group>>;
  formGroup: FormGroup;

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private day: DayService,
    private category: CategoryService,
    private competition: CompetitionService,
    private division: DivisionService,
    private group: GroupService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  ngOnInit(): void {
    super.ngOnInit();

    const season$ = this.currentSeason.get$();

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.category = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().category && this.categoryChange(slug)),
        finalize(() => this.loading.category = false));

    this.competitions$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { category }]) => ({ season, categorySlug: category })),
        distinctUntilChanged(isEqual),
        filter(({ season, categorySlug }) => season && categorySlug),
        tap(() => this.loading.competition = true),
        switchMap(({ season, categorySlug }) => this.competition.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.competition = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().competition && this.competitionChange(slug)),
        finalize(() => this.loading.competition = false));

    this.divisions$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { competition }]) => ({ season, competitionSlug: competition })),
        distinctUntilChanged(isEqual),
        filter(({ season, competitionSlug }) => season && competitionSlug),
        tap(() => this.loading.division = true),
        switchMap(({ season, competitionSlug }) => this.division.find(season.slug, competitionSlug)
          .pipe(finalize(() => this.loading.division = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().division && this.divisionChange(slug)),
        finalize(() => this.loading.division = false));

    this.groups$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { competition, division }]) => ({ season, competitionSlug: competition, divisionSlug: division })),
        distinctUntilChanged(isEqual),
        filter(({ season, competitionSlug, divisionSlug }) => season && competitionSlug && divisionSlug),
        tap(() => this.loading.group = true),
        switchMap(({ season, competitionSlug, divisionSlug }) => this.group.find(season.slug, competitionSlug, divisionSlug)
          .pipe(finalize(() => this.loading.group = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().group && this.groupChange(slug)),
        finalize(() => this.loading.group = false));

    this.formGroup = this.fb.group({
      category: [],
      competition: [],
      division: [],
      group: []
    });
    this.formGroup.controls.competition.disable();
    this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();

    this.route.queryParams
      .pipe(takeUntil(this.destroy$), map(({ category, competition, division, group }) => ({ category, competition, division, group })))
      .subscribe(({ category, competition, division, group }) => this.formGroup.patchValue({ category, competition, division, group }));
  }

  categoryChange(category: string) {
    this.refresh$.next({ category, competition: undefined, division: undefined, group: undefined, page: undefined } as FilterPageParams);
    this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
    if (!category) {
      this.formGroup.controls.competition.disable();
    }
  }

  competitionChange(competition: string) {
    this.refresh$.next({ competition, division: undefined, group: undefined, page: undefined } as FilterPageParams);
    this.formGroup.controls.group.disable();
  }

  divisionChange(division: string) {
    this.refresh$.next({ division, group: undefined, page: undefined } as FilterPageParams);
  }

  groupChange(group: string) {
    this.refresh$.next({ group, page: undefined } as FilterPageParams);
  }

  protected delete$(item: Day): Observable<void> {
    return of(this.route.snapshot.queryParams)
      .pipe(
        filter(({ competition, division, group }) => competition && division && group),
        mergeMap(({ competition, division, group }) => this.day
          .delete(this.currentSeason.get().slug, competition, division, group, item.slug)));
  }

  protected find$(params: PageParams): Observable<Page<Day>> {
    return combineLatest([
      this.currentSeason.get$().pipe(take(1)),
      this.route.queryParams
    ])
      .pipe(
        map(([season, { competition, division, group }]) => ({
          season, competitionSlug: competition, divisionSlug: division, groupSlug: group
        })),
        distinctUntilChanged(isEqual),
        mergeMap(({ season, competitionSlug, divisionSlug, groupSlug }) => season && competitionSlug && divisionSlug && groupSlug
          ? this.day.find(season.slug, competitionSlug, divisionSlug, groupSlug, params)
          : of(emptyPage)));
  }

  protected getItemLabel(item: Day): string {
    return `${item.index}`;
  }

  protected getItemSlug(item: Day): string {
    return item.slug;
  }

  protected getGhostElementArray(): void[] {
    return Array(10);
  }
}
