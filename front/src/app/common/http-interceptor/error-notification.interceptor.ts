import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { Observable, throwError } from 'rxjs';
import { catchError, take } from 'rxjs/operators';

@Injectable()
export class ErrorNotificationInterceptor implements HttpInterceptor {

  constructor(
    private snackBar: MatSnackBar,
    private translate: TranslateService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse) {
          this.translate
            .get(`error.notification.${req.method.toLocaleLowerCase()}.message`)
            .pipe(take(1))
            .subscribe((message: string) => this.snackBar.open(message, undefined, { duration: 5000 }));
        }
        return throwError(error);
      }));
  }
}
