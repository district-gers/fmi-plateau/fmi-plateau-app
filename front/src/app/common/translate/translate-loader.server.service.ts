import { TranslateLoader } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

const fs = require('fs');

export class TranslateLoaderServerService implements TranslateLoader {

  public getTranslation(lang: string): Observable<{ [key: string]: any }> {
    return of(JSON.parse(fs.readFileSync(`static/browser/assets/i18n/${lang}.json`, 'utf8')));
  }

}
