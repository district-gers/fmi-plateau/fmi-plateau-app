import { HttpResponse } from '@angular/common/http';
import { Observable, OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';

export interface BlobFile {
  blob: Blob;
  name?: string;
}

export interface TextFile {
  text: string;
  name?: string;
}

export const extractBlobFileName = (defaultName?: string): OperatorFunction<HttpResponse<Blob>, BlobFile> => {
  return (blob$: Observable<HttpResponse<Blob>>): Observable<BlobFile> => blob$
    .pipe(
      map((response: HttpResponse<Blob>) => ({
        blob: response.body,
        name: response.headers.get('Content-Disposition')?.match(/filename="(.*)"/)?.[1] || defaultName
      })));
};

export const extractTextFileName = (defaultName?: string): OperatorFunction<HttpResponse<string>, BlobFile> => {
  return (source: Observable<HttpResponse<string>>): Observable<BlobFile> => source
    .pipe(
      map((response: HttpResponse<string>) => ({
        text: response.body,
        name: response.headers.get('Content-Disposition')?.match(/filename="(.*)"/)?.[1] || defaultName
      })),
      map(({ text, name }) => ({
        blob: new Blob([text], { type: 'text/plain' }),
        name
      })));
};
