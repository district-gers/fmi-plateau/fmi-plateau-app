import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminEducatorEditComponent } from './admin-educator-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminEducatorEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminEducatorEditRoutingModule { }
