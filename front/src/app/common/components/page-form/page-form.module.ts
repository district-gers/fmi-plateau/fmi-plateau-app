import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FooterFormModule } from '../footer-form/footer-form.module';

import { PageFormComponent } from './page-form.component';



@NgModule({
  declarations: [PageFormComponent],
  exports: [
    PageFormComponent
  ],
  imports: [
    CommonModule,
    FooterFormModule
  ]
})
export class PageFormModule { }
