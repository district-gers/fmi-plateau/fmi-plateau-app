import * as _ from 'lodash';
import { Config } from './config';

const base: Config = require('../../../assets/config/config.json');
const test: Config = require('../../../assets/config/config.test.json');
const prod: Config = require('../../../assets/config/config.prod.json');

const configs = { base, test, prod };
const arrayMerge = (destination, source) => {
  if (_.isArray(destination) && _.isArray(source)) {
    return source;
  }
};
const config = _.mergeWith(
  {}, configs.base, configs[process.env.ENVIRONMENT] || {},
  arrayMerge);

class ConfigService {

  get config(): Config {
    return config;
  }

}

export const configService = new ConfigService();
