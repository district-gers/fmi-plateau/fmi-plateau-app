import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { AsyncSelectModule } from '@components/async-select/async-select.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { ResetButtonIconModule } from '@components/reset-button-icon/reset-button-icon.module';
import { MatDatetimepickerModule } from '@mat-datetimepicker/core';
import { MatMomentDatetimeModule } from '@mat-datetimepicker/moment';
import { TranslateModule } from '@ngx-translate/core';
import { TeamNameModule } from '@pipes/team-name/team-name.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { AdminFmiEditRoutingModule } from './admin-fmi-edit-routing.module';
import { AdminFmiEditComponent } from './admin-fmi-edit.component';


@NgModule({
  declarations: [AdminFmiEditComponent],
  imports: [
    CommonModule,
    AdminFmiEditRoutingModule,
    PageFormModule,
    TranslateModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    AsyncSelectModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    AsyncButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatMomentDatetimeModule,
    MatDatetimepickerModule,
    MatInputModule,
    LazyLoadImageModule,
    TeamNameModule,
    ResetButtonIconModule
  ]
})
export class AdminFmiEditModule { }
