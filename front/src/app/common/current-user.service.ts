import { Injectable } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Auth } from '@resources/admin/auth/auth.model';
import { AuthService } from '@resources/admin/auth/auth.service';
import { User } from '@resources/admin/user/user.model';
import { isEqual } from 'lodash';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { catchError, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CurrentUserService {

  private readonly user$: BehaviorSubject<Auth> = new BehaviorSubject<Auth>(null);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private currentSeason: CurrentSeasonService,
    private auth: AuthService) {
    this.init();
  }

  private init() {
    combineLatest([
      this.route.queryParams.pipe(map(({ user }) => user || 'katy-delmotte')),
      this.currentSeason.get$().pipe(map(season => season.slug))
    ])
      .pipe(
        distinctUntilChanged(isEqual),
        switchMap(([token, seasonSlug]: string[]) => this.auth.login(token, seasonSlug)
          .pipe(catchError(() => of(null)))))
      .subscribe(auth => this.user$.next(auth));
  }

  switch(user: User) {
    this.router.navigate([], { queryParams: { user: user.uuid } });
  }

  get$(): Observable<Auth> {
    return this.user$.asObservable().pipe(filter(user => !!user));
  }

  get(): Auth {
    return this.user$.getValue();
  }

  appendQueryParams(queryParams: Params): Params {
    if (!queryParams?.user && !!this.get()?.uuid) {
      return { user: this.get()?.uuid };
    }
  }

}
