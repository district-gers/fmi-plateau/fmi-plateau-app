import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Page, PageParams } from '@common/pagination/pagination';
import { RefreshEvent, RefreshEventService } from '@common/refresh-event.service';
import { ResponsiveService } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Season } from '@resources/admin/season/season.model';
import { SeasonService } from '@resources/admin/season/season.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

@Component({
  selector: 'app-admin-season-list',
  templateUrl: './admin-season-list.component.html',
  styleUrls: ['./admin-season-list.component.scss']
})
export class AdminSeasonListComponent extends AbstractAdminListComponent<Season> {

  columns = ['name', 'startDate', 'endDate', 'isCurrent', 'action'];

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private season: SeasonService,
    private refreshEvent: RefreshEventService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  protected delete$(item: Season): Observable<void> {
    return this.season.delete(item.slug)
      .pipe(tap(() => this.refreshEvent.pushEvent(RefreshEvent.SEASON)));
  }

  protected find$(params: PageParams): Observable<Page<Season>> {
    return this.season.find(params);
  }

  protected getItemLabel(item: Season): string {
    return item.name;
  }

  protected getItemSlug(item: Season): string {
    return item.slug;
  }

}
