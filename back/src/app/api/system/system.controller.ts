import { NextFunction, Request, Response } from 'express';
import { configService } from '../../common/config/config.service';
import { logger } from '../../common/logger/logger';
import { memjsClient } from '../../common/memcache';
import { FmiPlateauError, FmiPlateauErrorType } from '../../common/error/error';

class SystemController {

  clearMemcache(req: Request, res: Response, next: NextFunction) {
    new Promise((resolve: Function, reject: Function) => {
      if (req.body.password !== configService.config.system.password) {
        reject(new FmiPlateauError(FmiPlateauErrorType.NOT_ALLOWED));
      }
      resolve();
    })
      .then(() => memjsClient.flush())
      .then(() => {
        logger.info('Memcache flush');
        res.status(200).send();
      }, error => {
        return Promise.reject(new FmiPlateauError(FmiPlateauErrorType.REMOTE_API_ERROR, 'Memcache flush error', error));
      })
      .catch(next);
  }

}

export const systemController = new SystemController();
