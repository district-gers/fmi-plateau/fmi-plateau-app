import { Category, CategoryDto } from '@resources/admin/category/category.model';

class CategoryMapper {

  toModel(dto: CategoryDto): Category {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      code: dto.code,
      name: dto.name,
      rules: dto.rules
    };
  }

  toDto(model: Category): CategoryDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('code') && {
        code: model.code
      },
      ...model.hasOwnProperty('name') && {
        name: model.name
      },
      ...model.hasOwnProperty('rules') && {
        rules: model.rules
      }
    };
  }

}

export const categoryMapper = new CategoryMapper();
