import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { NavMenuModule } from '@components/nav-menu/nav-menu.module';
import { TranslateModule } from '@ngx-translate/core';
import { Angulartics2OnModule } from 'angulartics2';

import { UserMenuModule } from '../user-menu/user-menu.module';

import { LayoutComponent } from './layout.component';



@NgModule({
  declarations: [LayoutComponent],
  exports: [
    LayoutComponent
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatButtonModule,
    RouterModule,
    MatIconModule,
    MatMenuModule,
    TranslateModule,
    UserMenuModule,
    Angulartics2OnModule,
    NavMenuModule
  ]
})
export class LayoutModule { }
