import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { DialogConfirmationModule } from '@components/dialog-confirmation/dialog-confirmation.module';
import { DialogUploadModule } from '@components/dialog-upload/dialog-upload.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';
import { LicenseNumberModule } from '@pipes/license-number/license-number.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { AdminClubListRoutingModule } from './admin-club-list-routing.module';
import { AdminClubListComponent } from './admin-club-list.component';


@NgModule({
  declarations: [AdminClubListComponent],
  imports: [
    CommonModule,
    AdminClubListRoutingModule,
    PageFormModule,
    TranslateModule,
    MatTableModule,
    MatSortModule,
    MatTooltipModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    LazyLoadImageModule,
    DialogConfirmationModule,
    LicenseNumberModule,
    DialogUploadModule,
    AsyncButtonModule
  ]
})
export class AdminClubListModule { }
