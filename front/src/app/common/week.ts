import { Moment } from 'moment';

export interface Week {
  week: number;
  start: Moment;
  end: Moment;
}
