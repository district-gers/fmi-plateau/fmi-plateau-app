import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminSeasonEditComponent } from './admin-season-edit.component';


const routes: Routes = [
  {
    path: '',
    component: AdminSeasonEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminSeasonEditRoutingModule { }
