import { Component, OnInit } from '@angular/core';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page } from '@common/pagination/pagination';
import { RefreshEvent, RefreshEventService } from '@common/refresh-event.service';
import { Auth } from '@resources/admin/auth/auth.model';
import { User } from '@resources/admin/user/user.model';
import { UserService } from '@resources/admin/user/user.service';
import { Role } from '@resources/user/role';
import { combineLatest, Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

import { CurrentUserService } from '../../current-user.service';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {

  users$: Observable<Page<User>>;
  user$: Observable<Auth>;
  roleEnum = Role;

  constructor(
    private currentSeason: CurrentSeasonService,
    private currentUser: CurrentUserService,
    private user: UserService,
    private refreshEvent: RefreshEventService) {
  }

  ngOnInit(): void {
    this.users$ = combineLatest([
      this.currentSeason.get$(),
      this.refreshEvent.getEvents$(RefreshEvent.USER).pipe(startWith(null as object))
    ])
      .pipe(
        map(([season]) => season.slug),
        switchMap(seasonSlug => this.user.findBySeason(seasonSlug)));
    this.user$ = this.currentUser.get$();
  }

  switchUser(user: User) {
    this.currentUser.switch(user);
  }

  trackBySlug(index, item): string {
    return item.slug;
  }

}
