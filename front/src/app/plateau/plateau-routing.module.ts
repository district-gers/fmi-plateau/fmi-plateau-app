import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@common/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('./list/plateau-list.module').then(module => module.PlateauListModule)
      },
      {
        path: ':plateauSlug',
        loadChildren: () => import('./detail/plateau-detail.module').then(module => module.PlateauDetailModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlateauRoutingModule { }
