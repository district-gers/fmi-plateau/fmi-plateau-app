import { MatPaginatorIntl } from '@angular/material/paginator';
import { TranslateService } from '@ngx-translate/core';

export class TranslatePaginator {

  constructor(private readonly translate: TranslateService) {}

  getPaginatorIntl(): MatPaginatorIntl {
    const paginatorIntl = new MatPaginatorIntl();
    paginatorIntl.itemsPerPageLabel = this.translate.instant('common.paginator.itemsPerPageLabel');
    paginatorIntl.nextPageLabel = this.translate.instant('common.paginator.nextPageLabel');
    paginatorIntl.previousPageLabel = this.translate.instant('common.paginator.previousPageLabel');
    paginatorIntl.firstPageLabel = this.translate.instant('common.paginator.firstPageLabel');
    paginatorIntl.lastPageLabel = this.translate.instant('common.paginator.lastPageLabel');
    paginatorIntl.getRangeLabel = this.getRangeLabel.bind(this);
    return paginatorIntl;
  }

  private getRangeLabel(page: number, pageSize: number, length: number): string {
    if (pageSize === 0 || length === 0) {
      return this.translate.instant('common.paginator.rangePerLabel.empty');
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    if (startIndex + 1 === endIndex) {
      return this.translate.instant('common.paginator.rangePerLabel.single', { startIndex: startIndex + 1, length });
    }
    return this.translate.instant('common.paginator.rangePerLabel.many', { startIndex: startIndex + 1, endIndex, length });
  }
}
