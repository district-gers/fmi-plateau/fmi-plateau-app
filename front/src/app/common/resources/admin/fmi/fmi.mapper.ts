import { getRequiredMatchCount } from '@resources/admin/fmi/fmi.util';
import * as moment from 'moment';

import { Fmi, FmiDto, TeamSquad, TeamSquadDto } from './fmi.model';

class FmiMapper {

  toModel(dto: FmiDto): Fmi {
    return {
      slug: dto.slug,
      plateau: dto.plateauSlug,
      date: moment(dto.date),
      location: dto.location,
      day: dto.daySlug,
      dayIndex: dto.dayIndex,
      group: dto.groupSlug,
      groupIndex: dto.groupIndex,
      groupName: dto.groupName,
      division: dto.divisionSlug,
      divisionIndex: dto.divisionIndex,
      divisionName: dto.divisionName,
      competition: dto.competitionSlug,
      competitionName: dto.competitionName,
      category: dto.categorySlug,
      categoryName: dto.categoryName,
      inChargeMember: dto.inChargeMember,
      comment: dto.comment,
      doctor: dto.doctor,
      matchs: dto.matchs
        ?.map(match => ({
          ...match,
          date: moment(match.date)
        })),
      teams: dto.teams
        ?.map((team): TeamSquad => ({
          slug: team.slug,
          index: team.index,
          participant: team.participantSlug,
          name: team.name,
          shortName: team.shortName,
          logoUrl: team.logoUrl,
          educator: team.educator,
          players: team.players,
          signature: team.signature
        })),
      participant: dto.participantSlug && {
        slug: dto.participantSlug,
        name: dto.participantName,
        shortName: dto.participantShortName,
        logoUrl: dto.participantLogoUrl
      },
      matchStatus: dto.matchStatus,
      matchCount: dto.matchCount,
      requiredMatchCount: getRequiredMatchCount(dto.teams?.length, dto.matchCount)
    };
  }

  toDto(model: Fmi): FmiDto {
    return {
      ...model.hasOwnProperty('slug') && {
        slug: model.slug
      },
      ...model.hasOwnProperty('plateau') && {
        plateauSlug: model.plateau
      },
      ...model.hasOwnProperty('inChargeMember') && {
        inChargeMember: model.inChargeMember
      },
      ...model.hasOwnProperty('comment') && {
        comment: model.comment
      },
      ...model.hasOwnProperty('doctor') && {
        doctor: model.doctor
      },
      ...model.hasOwnProperty('matchs') && {
        matchs: model.matchs?.map(match => ({
          ...match,
          date: match.date?.format(),
          ...match.toDelete && {
            toDelete: match.toDelete
          }
        }))
      },
      ...model.hasOwnProperty('teams') && {
        teams: model.teams
          ?.map((team): TeamSquadDto => ({
            slug: team.slug,
            educator: team.educator,
            players: team.players,
            signature: team.signature
          }))
      }
    };
  }

}

export const fmiMapper = new FmiMapper();
