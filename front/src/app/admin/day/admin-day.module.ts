import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminDayRoutingModule } from './admin-day-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminDayRoutingModule
  ]
})
export class AdminDayModule { }
