import { Educator, EducatorDto } from '@resources/admin/educator/educator.model';

class EducatorMapper {

  toModel(dto: EducatorDto): Educator {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      firstName: dto.firstName,
      lastName: dto.lastName,
      code: dto.code,
      club: dto.clubSlug,
      clubName: dto.clubName,
      clubShortName: dto.clubShortName,
      clubLogoUrl: dto.clubLogoUrl
    };
  }

  toDto(model: Educator): EducatorDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('firstName') && {
        firstName: model.firstName
      },
      ...model.hasOwnProperty('lastName') && {
        lastName: model.lastName
      },
      ...model.hasOwnProperty('code') && {
        code: model.code
      },
      ...model.hasOwnProperty('club') && {
        clubSlug: model.club
      }
    };
  }

}

export const educatorMapper = new EducatorMapper();
