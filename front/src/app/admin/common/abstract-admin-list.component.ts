import { OnDestroy, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { Loading } from '@common/loading';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapPageToRouteParams, mapRouteToPageParams, PAGE_SIZES } from '@common/pagination/pagination.util';
import { ResponsiveService, SCREEN_SIZES, ScreenSize } from '@common/responsive/responsive.service';
import { DialogConfirmationResult } from '@components/dialog-confirmation/dialog-confirmation';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, finalize, map, mergeMap, skip, switchMap, take, tap } from 'rxjs/operators';

export abstract class AbstractAdminListComponent<T> implements OnInit, OnDestroy {

  ghostColumns: string[];
  dataPage$: Observable<Page<T>>;
  ghostElements$: Observable<number[]>;
  pageSizes = PAGE_SIZES;
  loading: Loading = { list: false };

  abstract columns: string[];

  protected destroy$ = new Subject<void>();
  protected refresh$ = new Subject<PageParams>();

  protected constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected translate: TranslateService,
    protected dialogConfirmation: DialogConfirmationService,
    protected responsive: ResponsiveService) {
  }

  ngOnInit(): void {
    this.ghostColumns = this.columns.map((_, index) => `ghost-${index + 1}`);

    this.dataPage$ = this.responsive.isSmOrLower$
      .pipe(
        take(1),
        map(isSm => isSm ? this.pageSizes[1] : this.pageSizes[2]),
        mergeMap(defaultSize => this.route.queryParams.pipe(mapRouteToPageParams(defaultSize))),
        tap(() => this.loading.list = true),
        switchMap(params => this.find$(params)),
        tap(() => this.loading.list = false),
        finalize(() => this.loading.list = false));

    this.refresh$
      .pipe(debounceTime(50), mapPageToRouteParams())
      .subscribe(queryParams => {
        this.router.navigate([], { queryParams, queryParamsHandling: 'merge', replaceUrl: true, relativeTo: this.route });
      });

    this.ghostElements$ = this.responsive.size$
      .pipe(
        map(screenSize => this.getGhostElementArray(screenSize)),
        map(array => array.fill(null).map((v, index) => index)));

    this.route.queryParams
      .pipe(
        map(({ season }) => season),
        distinctUntilChanged(),
        skip(1),
        map(() => ['category', 'competition', 'division', 'group', 'day',
          'categories', 'participants', 'clubs', 'playerCategories',
          'participant', 'team',
          'startDate', 'endDate',
          'page', 'size', 'sort', 'deleted', 'import']
          .reduce((acc, key) => ({ ...acc, [key]: undefined }), {})))
      .subscribe(queryParams => this.router
        .navigate([], { queryParams, queryParamsHandling: 'merge', replaceUrl: true, relativeTo: this.route }));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.refresh$.complete();
  }

  protected abstract find$(params: PageParams): Observable<Page<T>>;
  protected abstract delete$(item: T): Observable<void>;
  protected abstract getItemLabel(item: T): string;
  protected abstract getItemSlug(item: T): string;

  delete(item: T) {
    this.dialogConfirmation.open({
      message: this.translate.instant('admin.common.confirmation.delete', { name: this.getItemLabel(item) })
    })
      .pipe(
        mergeMap(dialogRef => dialogRef.afterClosed()),
        filter(result => result === DialogConfirmationResult.CONFIRM),
        mergeMap(() => this.delete$(item)))
      .subscribe(() => this.refresh$.next({ deleted: this.getItemSlug(item) }));
  }

  goToPage(event: PageEvent) {
    this.refresh$.next({ page: event.pageIndex, size: event.pageSize, deleted: undefined });
  }

  sort(sort: Sort) {
    if (!sort.active || sort.direction === '') {
      this.refresh$.next({ sort: null, deleted: undefined });
    } else {
      this.refresh$.next(({ sort: { field: sort.active, direction: sort.direction }, deleted: undefined }));
    }
  }

  trackBySlug = (index: number, item: T): string => this.getItemSlug(item);

  protected getGhostElementArray(screenSize: ScreenSize): void[] {
    switch (true) {
      case screenSize.index <= SCREEN_SIZES.sm.index:
        return Array(10);
      default:
        return Array(25);
    }
  }

}
