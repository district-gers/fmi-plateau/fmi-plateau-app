import { KeyValue } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page, PageParams } from '@common/pagination/pagination';
import { ResponsiveService } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Observable } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

@Component({
  selector: 'app-admin-category-list',
  templateUrl: './admin-category-list.component.html',
  styleUrls: ['./admin-category-list.component.scss']
})
export class AdminCategoryListComponent extends AbstractAdminListComponent<Category> {

  columns = ['name', 'playerCategories', 'action'];

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private currentSeason: CurrentSeasonService,
    private category: CategoryService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  protected delete$(item: Category): Observable<void> {
    return this.category.delete(this.currentSeason.get().slug, item.slug);
  }

  protected find$(params: PageParams): Observable<Page<Category>> {
    return this.currentSeason.get$().pipe(take(1), mergeMap(season => this.category.find(season.slug, params)));
  }

  protected getItemLabel(item: Category): string {
    return item.name;
  }

  protected getItemSlug(item: Category): string {
    return item.slug;
  }

  trackByKey(index: number, item: KeyValue<string, boolean | number>): string {
    return item.key;
  }
}
