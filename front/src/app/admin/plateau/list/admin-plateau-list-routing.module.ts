import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminPlateauListComponent } from './admin-plateau-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminPlateauListComponent,
    data: {
      meta: {
        title: 'admin.plateau.list.seo.title',
        description: 'admin.plateau.list.seo.description',
        'twitter:title': 'admin.plateau.list.seo.title',
        'twitter:description': 'admin.plateau.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPlateauListRoutingModule { }
