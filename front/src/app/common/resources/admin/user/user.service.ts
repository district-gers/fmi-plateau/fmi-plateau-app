import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { userMapper } from '@resources/admin/user/user.mapper';
import { User, UserDto } from '@resources/admin/user/user.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = `${environment.cloudFunction.data.url}/users`;

@Injectable({ providedIn: 'root' })
export class UserService {

  constructor(
    private http: HttpClient) {
  }

  find(pageParams?: PageParams): Observable<Page<User>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>  this.http.get<UserDto[]>(resourceUrl, { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => userMapper.toModel(dto)));
  }

  findBySeason(seasonSlug: string, pageParams?: PageParams): Observable<Page<User>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>  this.http.get<UserDto[]>(resourceUrl, {
          params: { ...params, season: seasonSlug },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => userMapper.toModel(dto)));
  }

  get(slug: string): Observable<User> {
    return this.http.get<UserDto>(`${resourceUrl}/${slug}`)
      .pipe(map(dto => userMapper.toModel(dto)));
  }

  save(model: User): Observable<User> {
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => userMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<UserDto>(`${resourceUrl}/${dto.slug}`, dto)
          : this.http.post<UserDto>(resourceUrl, dto)),
        map(dto => userMapper.toModel(dto)));
  }

  delete(slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl}/${slug}`);
  }

}
