import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { AsyncButtonComponent } from './async-button.component';



@NgModule({
  declarations: [AsyncButtonComponent],
  exports: [
    AsyncButtonComponent
  ],
  imports: [
    CommonModule,
    MatProgressSpinnerModule
  ]
})
export class AsyncButtonModule { }
