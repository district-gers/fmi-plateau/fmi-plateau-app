import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminPlateauEditComponent } from './admin-plateau-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminPlateauEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPlateauEditRoutingModule { }
