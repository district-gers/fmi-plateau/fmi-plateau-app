import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page, PageParams } from '@common/pagination/pagination';
import { ResponsiveService } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { tapUniqueItem } from '@utils/rxjs.util';
import { Observable } from 'rxjs';
import { finalize, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

interface FilterPageParams extends PageParams {
  category?: string;
}

@Component({
  selector: 'app-admin-competition-list',
  templateUrl: './admin-competition-list.component.html',
  styleUrls: ['./admin-competition-list.component.scss']
})
export class AdminCompetitionListComponent extends AbstractAdminListComponent<Competition> implements OnInit {

  columns = ['name', 'categoryName', 'action'];
  categories$: Observable<Page<Category>>;
  formGroup: FormGroup;

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private competition: CompetitionService,
    private category: CategoryService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  ngOnInit(): void {
    super.ngOnInit();

    const season$ = this.currentSeason.get$();

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.category = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().category && this.categoryChange(slug)),
        finalize(() => this.loading.category = false));

    this.formGroup = this.fb.group({
      category: []
    });

    this.route.queryParams
      .pipe(takeUntil(this.destroy$), map(params => params.category))
      .subscribe(category => this.formGroup.patchValue({ category }));
  }

  categoryChange(category: string) {
    this.refresh$.next({ category, page: undefined } as FilterPageParams);
  }

  protected delete$(item: Competition): Observable<void> {
    return this.competition.delete(this.currentSeason.get().slug, item.slug);
  }

  protected find$(params: FilterPageParams): Observable<Page<Competition>> {
    return this.currentSeason.get$().pipe(take(1), mergeMap(season => params.category
      ? this.competition.findByCategory(season.slug, params.category, params)
      : this.competition.find(season.slug, params)));
  }

  protected getItemLabel(item: Competition): string {
    return item.name;
  }

  protected getItemSlug(item: Competition): string {
    return item.slug;
  }

  protected getGhostElementArray(): void[] {
    return Array(5);
  }
}
