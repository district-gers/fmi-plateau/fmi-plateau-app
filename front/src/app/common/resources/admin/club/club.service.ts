import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { BlobFile, extractTextFileName } from '@common/save-file.util';
import { clubMapper } from '@resources/admin/club/club.mapper';
import { Club, ClubDto } from '@resources/admin/club/club.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = (seasonSlug: string) => `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/clubs`;

@Injectable({ providedIn: 'root' })
export class ClubService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, pageParams?: PageParams): Observable<Page<Club>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>  this.http.get<ClubDto[]>(resourceUrl(seasonSlug), { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => clubMapper.toModel(dto)));
  }

  get(seasonSlug: string, slug: string): Observable<Club> {
    return this.http.get<ClubDto>(`${resourceUrl(seasonSlug)}/${slug}`)
      .pipe(map(dto => clubMapper.toModel(dto)));
  }

  save(seasonSlug: string, model: Club): Observable<Club> {
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => clubMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<ClubDto>(`${resourceUrl(seasonSlug)}/${dto.slug}`, dto)
          : this.http.post<ClubDto>(resourceUrl(seasonSlug), dto)),
        map(dto => clubMapper.toModel(dto)));
  }

  delete(seasonSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug)}/${slug}`);
  }

  csv(seasonSlug: string, defaultFileName?: string): Observable<BlobFile> {
    return this.http.get(`${resourceUrl(seasonSlug)}/csv`,
      { responseType: 'text', observe: 'response' })
      .pipe(extractTextFileName(defaultFileName));
  }

  upload(seasonSlug: string, file: File): Observable<void> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post<void>(`${resourceUrl(seasonSlug)}/upload`, formData);
  }

}
