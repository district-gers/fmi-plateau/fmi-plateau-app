import { AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export const controlValueChange$ = <T>(control: AbstractControl): Observable<T> => control.valueChanges
  .pipe(startWith(''), map(() => control.value));
