import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-team-list.module').then(module => module.AdminTeamListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-team-edit.module').then(module => module.AdminTeamEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-team-edit.module').then(module => module.AdminTeamEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-team-edit.module').then(module => module.AdminTeamEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminTeamRoutingModule { }
