import { Division, DivisionDto } from '@resources/admin/division/division.model';

class DivisionMapper {

  toModel(dto: DivisionDto): Division {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      index: dto.index,
      name: dto.name
    };
  }

  toDto(model: Division): DivisionDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('index') && {
        index: model.index
      },
      ...model.hasOwnProperty('name') && {
        name: model.name
      }
    };
  }

}

export const divisionMapper = new DivisionMapper();
