import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { RefreshEvent, RefreshEventService } from '@common/refresh-event.service';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Season } from '@resources/admin/season/season.model';
import { SeasonService } from '@resources/admin/season/season.service';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

@Component({
  selector: 'app-admin-season-edit',
  templateUrl: './admin-season-edit.component.html',
  styleUrls: ['./admin-season-edit.component.scss']
})
export class AdminSeasonEditComponent extends AbstractAdminEditComponent<Season> implements OnInit {

  formGroup: FormGroup;
  metaLabelsPrefix = 'admin.season.edit.seo';
  moment = moment;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private season: SeasonService,
    private refreshEvent: RefreshEventService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.formGroup = this.fb.group({
      slug: [],
      name: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      isCurrent: []
    });

    super.itemSlug$()
      .pipe(
        switchMap(slug => this.season.get(slug)))
      .subscribe(season => this.formGroup.patchValue({
        slug: this.mode === EditMode.UPDATE ? season.slug : undefined,
        name: season.name,
        startDate: season.startDate,
        endDate: season.endDate,
        isCurrent: season.isCurrent
      }));
  }

  protected save$(): Observable<[Season]> {
    const value = this.formGroup.getRawValue();
    return this.season.save({
      slug: value.slug,
      name: value.name,
      startDate: value.startDate,
      endDate: value.endDate,
      isCurrent: value.isCurrent
    })
      .pipe(
        tap(() => this.refreshEvent.pushEvent(RefreshEvent.SEASON)),
        map(season => [season]));
  }
}
