import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminSeasonListComponent } from './admin-season-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminSeasonListComponent,
    data: {
      meta: {
        title: 'admin.season.list.seo.title',
        description: 'admin.season.list.seo.description',
        'twitter:title': 'admin.season.list.seo.title',
        'twitter:description': 'admin.season.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminSeasonListRoutingModule { }
