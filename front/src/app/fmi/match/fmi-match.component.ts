import { AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Loading } from '@common/loading';
import { Fmi, FmiMatch, FmiMatchStatus } from '@common/resources/admin/fmi/fmi.model';
import { Plateau, PlateauTeam } from '@common/resources/admin/plateau/plateau.model';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { FmiService } from '@resources/admin/fmi/fmi.service';
import { getFmiMatchStatus, getRequiredMatchCount } from '@resources/admin/fmi/fmi.util';
import { PlateauService } from '@resources/admin/plateau/plateau.service';
import * as moment from 'moment';
import { combineLatest, ConnectableObservable, Observable, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  finalize,
  map,
  mergeMap,
  publishBehavior,
  skip,
  switchMap,
  take,
  takeUntil,
  tap
} from 'rxjs/operators';

const matchGroupConfig = {
  slug: [],
  date: [undefined, Validators.required],
  team1: ['', Validators.required],
  team2: ['', Validators.required]
};

@Component({
  selector: 'app-fmi-match',
  templateUrl: './fmi-match.component.html',
  styleUrls: [
    './fmi-match.component.scss',
    './fmi-match.component.mobile.scss'
  ]
})
export class FmiMatchComponent implements OnInit, AfterViewInit, OnDestroy {

  plateau$: ConnectableObservable<Plateau>;
  screenSize$: Observable<ScreenSize>;
  formGroup: FormGroup;
  previousRouteCommands: Link;
  matchFormGroups: FormGroup[];
  loading: Loading = { save: false };

  private destroy$ = new Subject<void>();

  requiredMatchCount$: Observable<number>;
  teamCount$: Observable<number>;
  matchStatus$: Observable<FmiMatchStatus>;

  FmiMatchStatus = FmiMatchStatus;

  @ViewChildren(FormGroupDirective) formGroupDirectives: QueryList<FormGroupDirective>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private routerExt: RouterExtService,
    private currentSeason: CurrentSeasonService,
    private responsive: ResponsiveService,
    private plateau: PlateauService,
    private fmi: FmiService) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      slug: [],
      matchs: this.fb.array([]),
      matchsToDelete: this.fb.array([])
    });
    this.addMatchFormGroup();
    this.matchFormGroups = (this.formGroup.controls.matchs as FormArray).controls as FormGroup[];
    this.previousRouteCommands = this.routerExt.getPreviousUrl() || { commands: ['/plateaux'] };

    const season$ = this.currentSeason.get$().pipe(take(1));

    this.plateau$ = combineLatest([season$, this.route.params])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, { plateauSlug }]) => !!plateauSlug),
        map(([season, { plateauSlug }]) => ({ season, plateauSlug })),
        distinctUntilChanged(),
        switchMap(({ season, plateauSlug }) => this.plateau.get(season.slug, plateauSlug)),
        publishBehavior(null)) as ConnectableObservable<Plateau>;
    this.plateau$.connect();

    this.screenSize$ = this.responsive.size$;

    combineLatest([season$, this.route.params])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, { fmiSlug }]) => !!fmiSlug),
        map(([season, { fmiSlug }]) => ({ season, fmiSlug })),
        distinctUntilChanged(),
        switchMap(({ season, fmiSlug }) => this.fmi.get(season.slug, fmiSlug)))
      .subscribe((fmi: Fmi) => {
        this.formGroup.patchValue({
          slug: fmi.slug
        });
        (this.formGroup.controls.matchs as FormArray).clear();
        fmi.matchs?.forEach((match) => {
          this.addMatchFormGroup(match);
        });
        if (!fmi.matchs?.length) {
          this.addMatchFormGroup({ date: fmi.date.clone() });
        }

        if (!this.previousRouteCommands.queryParams) {
          this.previousRouteCommands.queryParams = {
            category: fmi.category,
            competition: fmi.competition,
            division: fmi.division,
            group: fmi.group,
            day: fmi.day
          };
        }
      });

    const plateauAndMatchCount$ = combineLatest([
      this.plateau$,
      this.formGroup.controls.matchs.valueChanges.pipe(map(() => this.getValidMatchFormGroupCount()))
    ]);

    this.teamCount$ = this.plateau$.pipe(map(plateau => plateau?.teams?.length));

    this.matchStatus$ = plateauAndMatchCount$
      .pipe(
        map(([plateau, matchCount]: [Plateau, number]) => plateau?.teams?.length
          && getFmiMatchStatus(plateau.teams?.length, matchCount)));

    this.requiredMatchCount$ = plateauAndMatchCount$
      .pipe(
        map(([plateau, matchCount]: [Plateau, number]) => plateau?.teams?.length
          && getRequiredMatchCount(plateau.teams?.length, matchCount)));

    this.route.queryParams
      .pipe(
        map(({ season }) => season),
        distinctUntilChanged(),
        skip(1))
      .subscribe(() => this.router
        .navigate(['/plateaux'], { queryParamsHandling: 'merge', replaceUrl: true, relativeTo: this.route }));
  }

  ngAfterViewInit(): void {
    this.formGroupDirectives.first.ngSubmit
      .pipe(takeUntil(this.destroy$))
      .subscribe(event => {
        this.formGroupDirectives
          .filter(item => !item.submitted)
          .forEach(item => item.onSubmit(event));
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  save() {
    const value = this.formGroup.getRawValue();
    this.currentSeason.get$()
      .pipe(
        tap(() => this.loading.save = true),
        take(1),
        mergeMap(season => this.fmi.save(season.slug, {
          slug: value.slug,
          matchs: [
            ...value.matchs?.map((match): FmiMatch => ({
              slug: match.slug,
              date: match.date,
              team1: {
                slug: match.team1
              },
              team2: {
                slug: match.team2
              }
            })),
            ...value.matchsToDelete?.map((match): FmiMatch => ({
              slug: match.slug,
              toDelete: true
            }))
          ]
        })),
        finalize(() => this.loading.save = false))
      .subscribe(() => this.router.navigate(this.previousRouteCommands.commands, { queryParams: this.previousRouteCommands.queryParams }));
  }

  addMatchGroup(plateau: Plateau) {
    const lastMatch = this.formGroup.value.matchs[this.formGroup.value.matchs.length - 1];
    const control = this.fb.group(matchGroupConfig);
    control.patchValue({
      date: moment(lastMatch?.date || plateau.date)
    });
    (this.formGroup.controls.matchs as FormArray).push(control);
  }

  deleteMatchGroup(formGroupIndex: number) {
    const matchsFormArray = (this.formGroup.controls.matchs as FormArray);
    const formGroup = matchsFormArray.at(formGroupIndex) as FormGroup;
    if (formGroup) {
      if (formGroup.value.slug) {
        formGroup.patchValue({ toDelete: true });
        Object.values(formGroup.controls).forEach(control => control.setValidators(null));
        formGroup.setValidators(null);
        formGroup.updateValueAndValidity();
        (this.formGroup.controls.matchsToDelete as FormArray).push(formGroup);
      }
      matchsFormArray.removeAt(formGroupIndex);
    }
  }

  trackBySlug(index, item): string {
    return item.slug;
  }

  getTeamBySlug(teams: PlateauTeam[], slug: string): PlateauTeam {
    return teams?.find(team => team.slug === slug);
  }

  private addMatchFormGroup(match?: FmiMatch): FormGroup {
    const control = this.fb.group(matchGroupConfig);
    if (match) {
      control.patchValue({
        slug: match.slug,
        date: match.date,
        team1: match.team1?.slug,
        team2: match.team2?.slug
      });
    }
    (this.formGroup.controls.matchs as FormArray).push(control);
    return control;
  }

  getValidMatchFormGroupCount() {
    return (this.formGroup.controls.matchs as FormArray)?.controls?.filter(matchFormGroup => matchFormGroup.valid)?.length;
  }
}
