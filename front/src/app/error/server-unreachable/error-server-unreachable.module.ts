import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { Angulartics2OnModule } from 'angulartics2';

import { ErrorServerUnreachableRoutingModule } from './error-server-unreachable-routing.module';
import { ErrorServerUnreachableComponent } from './error-server-unreachable.component';


@NgModule({
  declarations: [ErrorServerUnreachableComponent],
  imports: [
    CommonModule,
    ErrorServerUnreachableRoutingModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    Angulartics2OnModule
  ]
})
export class ErrorServerUnreachableModule { }
