import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable } from 'rxjs';

import { CookiesStatus } from './cookies-status';
import { CookiesService } from './cookies.service';
import { ScriptAppendService } from './script-append.service';

const cookiesStatusKey = 'scp-cookie-status';

@Injectable({ providedIn: 'root' })
export class CookiesBrowserService extends CookiesService {

  private _cookiesStatus$ = new BehaviorSubject<CookiesStatus>(null);

  constructor(private cookie: CookieService,
              private scriptAppend: ScriptAppendService) {
    super();
    this._cookiesStatus$
      .subscribe((status: CookiesStatus) => {
        if (status === CookiesStatus.ALLOW) {
          this.scriptAppend.appendAnalytics();
        }
        if (status === CookiesStatus.DENY) {
          const cookies = this.cookie.getAll();
          Object.keys(cookies)
            .filter(key => key !== cookiesStatusKey)
            .forEach(key => this.cookie.delete(key, '/'));
        }
      });

    const currentStatus = this.cookie.get(cookiesStatusKey);
    if (currentStatus) {
      this._cookiesStatus$.next(currentStatus as CookiesStatus);
    }
  }

  get cookiesStatus$(): Observable<CookiesStatus> {
    return this._cookiesStatus$.asObservable();
  }

  allow() {
    this.setCookiesStatus(CookiesStatus.ALLOW);
  }

  deny() {
    this.setCookiesStatus(CookiesStatus.DENY);
  }

  private setCookiesStatus(status: CookiesStatus) {
    this.cookie.set(cookiesStatusKey, status, moment().add(1, 'year').toDate(), '/');
    this._cookiesStatus$.next(status);
  }

}
