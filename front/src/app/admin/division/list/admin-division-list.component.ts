import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { emptyPage, Page, PageParams } from '@common/pagination/pagination';
import { ResponsiveService } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { Division } from '@resources/admin/division/division.model';
import { DivisionService } from '@resources/admin/division/division.service';
import { tapUniqueItem } from '@utils/rxjs.util';
import { isEqual } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

interface FilterPageParams extends PageParams {
  competition?: string;
}

@Component({
  selector: 'app-admin-division-list',
  templateUrl: './admin-division-list.component.html',
  styleUrls: [
    './admin-division-list.component.scss',
    './admin-division-list.component.responsive.scss'
  ]
})
export class AdminDivisionListComponent extends AbstractAdminListComponent<Division> implements OnInit {

  columns = ['name', 'index', 'action'];
  categories$: Observable<Page<Category>>;
  competitions$: Observable<Page<Competition>>;
  formGroup: FormGroup;

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private division: DivisionService,
    private category: CategoryService,
    private competition: CompetitionService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  ngOnInit(): void {
    super.ngOnInit();

    const season$ = this.currentSeason.get$();

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.category = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().category && this.categoryChange(slug)),
        finalize(() => this.loading.category = false));

    this.competitions$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { category }]) => ({ season, categorySlug: category })),
        distinctUntilChanged(isEqual),
        filter(({ season, categorySlug }) => season && categorySlug),
        tap(() => this.loading.competition = true),
        switchMap(({ season, categorySlug }) => this.competition.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.competition = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().competition && this.competitionChange(slug)),
        finalize(() => this.loading.competition = false));

    this.formGroup = this.fb.group({
      category: [],
      competition: []
    });
    this.formGroup.controls.competition.disable();

    this.route.queryParams
      .pipe(takeUntil(this.destroy$), map(({ category, competition }) => ({ category, competition })))
      .subscribe(({ category, competition }) => this.formGroup.patchValue({ category, competition }));
  }

  categoryChange(category: string) {
    this.refresh$.next({ category, competition: undefined, page: undefined } as FilterPageParams);
  }

  competitionChange(competition: string) {
    this.refresh$.next({ competition, page: undefined } as FilterPageParams);
  }

  protected delete$(item: Division): Observable<void> {
    return of(this.route.snapshot.queryParams)
      .pipe(
        map(({ competition }) => ({ competitionSlug: competition })),
        filter(({ competitionSlug }) => competitionSlug),
        mergeMap(({ competitionSlug }) => this.division.delete(this.currentSeason.get().slug, competitionSlug, item.slug)));
  }

  protected find$(params: FilterPageParams): Observable<Page<Division>> {
    return this.currentSeason.get$().pipe(take(1))
      .pipe(
        mergeMap(season => season && params.competition
          ? this.division.find(season.slug, params.competition, params)
          : of(emptyPage)));
  }

  protected getItemLabel(item: Division): string {
    return item.name;
  }

  protected getItemSlug(item: Division): string {
    return item.slug;
  }

  protected getGhostElementArray(): void[] {
    return Array(5);
  }
}
