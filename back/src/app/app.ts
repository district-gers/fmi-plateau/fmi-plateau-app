import * as path from 'path';
import * as express from 'express';
import { Request, Response } from 'express';
import * as compression from 'compression';
import * as helmet from 'helmet';
import { configService } from './common/config/config.service';
import { apiRouter } from './api/api.router';
import { render } from './common/render-html.util';
import { expressLogger, logger } from './common/logger/logger';
import { errorMiddleware } from './common/error/error.handler';
import { queryParamsMiddleware } from './common/query-params-filter.middleware';

const { AppServerModule, LAZY_MODULE_MAP, ngExpressEngine, provideModuleMap } = require('../../static/server/main');
const DIST_FOLDER = path.join(process.cwd(), 'static');

export const app = express();

app
  .engine('html', ngExpressEngine({
    bootstrap: AppServerModule,
    providers: [provideModuleMap(LAZY_MODULE_MAP)]
  }))
  .set('view engine', 'html')
  .set('views', path.join(DIST_FOLDER, 'browser'));

app
  .use(helmet())
  .use(expressLogger)
  .use(express.json())
  .use(compression())
  .use('/api', apiRouter)
  .use(errorMiddleware);

app
  .get('*.*', express.static(
    path.join(DIST_FOLDER, 'browser'),
    { maxAge: configService.config.cache.assets.duration }
  ))
  .get('*', queryParamsMiddleware(),
    (req: Request, res: Response) => {
      render(req, res)
        .then(html => !(res.headersSent || res.writableEnded) && res.send(html))
        .catch(error => {
          res.send(500);
          logger.error(error);
        });
    });
