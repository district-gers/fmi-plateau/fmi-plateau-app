import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FmiReadComponent } from './fmi-read.component';

const routes: Routes = [
  {
    path: '',
    component: FmiReadComponent,
    data: {
      meta: {
        title: 'fmi.read.seo.title',
        description: 'fmi.read.seo.description',
        'twitter:title': 'fmi.read.seo.title',
        'twitter:description': 'fmi.read.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FmiReadRoutingModule { }
