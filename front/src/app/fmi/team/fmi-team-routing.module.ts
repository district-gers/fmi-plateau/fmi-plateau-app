import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FmiTeamComponent } from './fmi-team.component';

const routes: Routes = [
  {
    path: '',
    component: FmiTeamComponent,
    data: {
      meta: {
        title: 'fmi.team.seo.title',
        description: 'fmi.team.seo.description',
        'twitter:title': 'fmi.team.seo.title',
        'twitter:description': 'fmi.team.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FmiTeamRoutingModule { }
