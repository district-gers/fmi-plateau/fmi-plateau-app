import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { emptyPage, Page, PageParams } from '@common/pagination/pagination';
import { ResponsiveService } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { Division } from '@resources/admin/division/division.model';
import { DivisionService } from '@resources/admin/division/division.service';
import { Group } from '@resources/admin/group/group.model';
import { GroupService } from '@resources/admin/group/group.service';
import { tapUniqueItem } from '@utils/rxjs.util';
import { isEqual } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

interface FilterPageParams extends PageParams {
  competition?: string;
  division?: string;
}

@Component({
  selector: 'app-admin-group-list',
  templateUrl: './admin-group-list.component.html',
  styleUrls: [
    './admin-group-list.component.scss',
    './admin-group-list.component.responsive.scss'
  ]
})
export class AdminGroupListComponent extends AbstractAdminListComponent<Group> implements OnInit {

  columns = ['name', 'index', 'teams', 'action'];
  categories$: Observable<Page<Category>>;
  competitions$: Observable<Page<Competition>>;
  divisions$: Observable<Page<Division>>;
  formGroup: FormGroup;

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private group: GroupService,
    private category: CategoryService,
    private competition: CompetitionService,
    private division: DivisionService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  ngOnInit(): void {
    super.ngOnInit();

    const season$ = this.currentSeason.get$();

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.category = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().category && this.categoryChange(slug)),
        finalize(() => this.loading.category = false));

    this.competitions$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { category }]) => ({ season, categorySlug: category })),
        distinctUntilChanged(isEqual),
        filter(({ season, categorySlug }) => season && categorySlug),
        tap(() => this.loading.competition = true),
        switchMap(({ season, categorySlug }) => this.competition.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.competition = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().competition && this.competitionChange(slug)),
        finalize(() => this.loading.competition = false));

    this.divisions$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { competition }]) => ({ season, competitionSlug: competition })),
        distinctUntilChanged(isEqual),
        filter(({ season, competitionSlug }) => season && competitionSlug),
        tap(() => this.loading.division = true),
        switchMap(({ season, competitionSlug }) => this.division.find(season.slug, competitionSlug)
          .pipe(finalize(() => this.loading.division = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().division && this.divisionChange(slug)),
        finalize(() => this.loading.division = false));

    this.formGroup = this.fb.group({
      category: [],
      competition: [],
      division: [],
    });
    this.formGroup.controls.competition.disable();
    this.formGroup.controls.division.disable();

    this.route.queryParams
      .pipe(takeUntil(this.destroy$), map(({ category, competition, division }) => ({ category, competition, division })))
      .subscribe(({ category, competition, division }) => this.formGroup.patchValue({ category, competition, division }));
  }

  categoryChange(category: string) {
    this.refresh$.next({ category, competition: undefined, division: undefined, page: undefined } as FilterPageParams);
    this.formGroup.controls.division.disable();
    if (!category) {
      this.formGroup.controls.competition.disable();
    }
  }

  competitionChange(competition: string) {
    this.refresh$.next({ competition, division: undefined, page: undefined } as FilterPageParams);
  }

  divisionChange(division: string) {
    this.refresh$.next({ division, page: undefined } as FilterPageParams);
  }

  protected delete$(item: Group): Observable<void> {
    return of(this.route.snapshot.queryParams)
      .pipe(
        map(({ competition, division }) => ({ competitionSlug: competition, divisionSlug: division })),
        filter(({ competitionSlug, divisionSlug }) => competitionSlug && divisionSlug),
        mergeMap(({ competitionSlug, divisionSlug }) => this.group
          .delete(this.currentSeason.get().slug, competitionSlug, divisionSlug, item.slug)));
  }

  protected find$(params: PageParams): Observable<Page<Group>> {
    return combineLatest([
      this.currentSeason.get$().pipe(take(1)),
      this.route.queryParams
    ])
      .pipe(
        map(([season, { competition, division }]) => ({ season, competitionSlug: competition, divisionSlug: division })),
        distinctUntilChanged(isEqual),
        mergeMap(({ season, competitionSlug, divisionSlug }) => season && competitionSlug && divisionSlug
          ? this.group.find(season.slug, competitionSlug, divisionSlug, params)
          : of(emptyPage)));
  }

  protected getItemLabel(item: Group): string {
    return item.name;
  }

  protected getItemSlug(item: Group): string {
    return item.slug;
  }

  protected getGhostElementArray(): void[] {
    return Array(5);
  }
}
