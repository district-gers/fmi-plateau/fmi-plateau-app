import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ResponsiveService } from '@common/responsive/responsive.service';
import { DialogConfirmationData, DialogConfirmationResult } from '@components/dialog-confirmation/dialog-confirmation';
import { DialogConfirmationComponent } from '@components/dialog-confirmation/dialog-confirmation.component';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable()
export class DialogConfirmationService {

  constructor(
    private dialog: MatDialog,
    private responsive: ResponsiveService) {
  }

  open(data: DialogConfirmationData): Observable<MatDialogRef<DialogConfirmationComponent, DialogConfirmationResult>> {
    return this.responsive.isXsOrLower$
      .pipe(
        take(1),
        map(isXsOrLower => this.dialog
          .open<DialogConfirmationComponent, DialogConfirmationData, DialogConfirmationResult>(DialogConfirmationComponent, {
            data,
            height: 'auto',
            width: isXsOrLower ? '100%' : 'auto',
            minWidth: isXsOrLower ? 'auto' : '30rem',
            maxWidth: 'auto',
            panelClass: 'dialog-confirmation'
          })));
  }

}
