import { Component, OnInit } from '@angular/core';
import { ResponsiveService, SCREEN_SIZES, ScreenSize } from '@common/responsive/responsive.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [
    './home.component.scss',
    './home.component.mobile.scss'
  ]
})
export class HomeComponent implements OnInit {

  pictureSize$: Observable<ScreenSize>;

  constructor(
    private responsive: ResponsiveService) {
  }

  ngOnInit(): void {
    this.pictureSize$ = this.responsive.size$
      .pipe(
        map((screenSize: ScreenSize) => screenSize || SCREEN_SIZES.xxs),
        map((screenSize: ScreenSize) => screenSize.index >= SCREEN_SIZES.sm.index ? SCREEN_SIZES.sm : screenSize));
  }

}
