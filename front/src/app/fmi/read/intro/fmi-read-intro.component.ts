import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Fmi } from '@common/resources/admin/fmi/fmi.model';
import { Plateau } from '@common/resources/admin/plateau/plateau.model';
import { UserIsInChargeOfPlateauPipe } from '@pipes/user-is-in-charge-of-plateau/user-is-in-charge-of-plateau.pipe';
import { Auth } from '@resources/admin/auth/auth.model';

@Component({
  selector: 'app-fmi-read-intro',
  templateUrl: './fmi-read-intro.component.html',
  styleUrls: ['./fmi-read-intro.component.scss']
})
export class FmiReadIntroComponent implements OnChanges {

  @Input() plateau: Plateau;
  @Input() fmi: Fmi;
  @Input() user: Auth;
  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userIsInChargeOfPlateauPipe: UserIsInChargeOfPlateauPipe) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    const plateauChange = changes.plateau;
    const fmiChange = changes.fmi;
    const userChange = changes.user;
    if (plateauChange && plateauChange.previousValue !== plateauChange.currentValue
      || fmiChange && fmiChange.previousValue !== fmiChange.currentValue
      || userChange && userChange.previousValue !== userChange.currentValue) {
      if (this.formGroup) {
        Object.keys(this.formGroup.controls)
          .forEach((key: string) => this.formGroup.removeControl(key));
      }
      this.formGroup = null;
      if (this.userIsInChargeOfPlateauPipe.transform(this.user, this.plateau)) {
        this.formGroup = this.fb.group({
          comment: []
        });
        this.formGroup.patchValue({ comment: this.fmi?.comment });
      }
    }
  }

}
