import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';
import { CategoryLabelModule } from '@pipes/category-label/category-label.module';
import { Angulartics2OnModule } from 'angulartics2';

import { AdminCategoryEditRoutingModule } from './admin-category-edit-routing.module';
import { AdminCategoryEditComponent } from './admin-category-edit.component';


@NgModule({
  declarations: [AdminCategoryEditComponent],
  imports: [
    CommonModule,
    AdminCategoryEditRoutingModule,
    PageFormModule,
    TranslateModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    CategoryLabelModule,
    Angulartics2OnModule,
    AsyncButtonModule
  ]
})
export class AdminCategoryEditModule { }
