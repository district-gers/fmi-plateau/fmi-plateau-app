import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthStorageService } from '@common/auth-storage.service';
import { authMapper } from '@resources/admin/auth/auth.mapper';
import { Auth, AuthDto } from '@resources/admin/auth/auth.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = `${environment.cloudFunction.data.url}/auth`;

@Injectable({ providedIn: 'root' })
export class AuthService {

  constructor(
    private http: HttpClient,
    private authStorageService: AuthStorageService) {
  }

  login(token: string, seasonSlug?: string): Observable<Auth> {
    return of({ token })
      .pipe(
        mergeMap(dto => this.http.post<AuthDto>(`${resourceUrl}/login`, {
          password: '',
          ...dto
        }, { params: { season: seasonSlug } })),
        // tslint:disable-next-line:no-shadowed-variable
        tap(({ token }) => this.authStorageService.storeToken(token)),
        map(dto => authMapper.toModel(dto)));
  }
}
