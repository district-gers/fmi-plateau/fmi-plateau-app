import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ResponsiveService } from '@common/responsive/responsive.service';
import { DialogUploadData, DialogUploadResult } from '@components/dialog-upload/dialog-upload';
import { DialogUploadComponent } from '@components/dialog-upload/dialog-upload.component';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable()
export class DialogUploadService {

  constructor(
    private dialog: MatDialog,
    private responsive: ResponsiveService) {
  }

  open(data?: DialogUploadData): Observable<MatDialogRef<DialogUploadComponent, DialogUploadResult>> {
    return this.responsive.isXsOrLower$
      .pipe(
        take(1),
        map(isXsOrLower => this.dialog
          .open<DialogUploadComponent, DialogUploadData, DialogUploadResult>(DialogUploadComponent, {
            data,
            height: 'auto',
            width: isXsOrLower ? '100%' : 'auto',
            minWidth: isXsOrLower ? 'auto' : '35rem',
            maxWidth: 'auto',
            panelClass: 'dialog-upload'
          })));
  }

}
