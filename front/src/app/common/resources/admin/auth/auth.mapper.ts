import { Auth, AuthDto } from '@resources/admin/auth/auth.model';

class AuthMapper {

  toModel(dto: AuthDto): Auth {
    if (!dto) {
      return;
    }
    return {
      token: dto.token,
      uuid: dto.uuid,
      educatorSlug: dto.educatorSlug,
      roles: dto.roles,
      rights: dto.rights,
      firstName: dto.firstName,
      lastName: dto.lastName,
      participantSlugs: [...(dto.participantSlugs || [])],
      clubSlug: dto.clubSlug,
      clubLogoUrl: dto.clubLogoUrl
    };
  }

  toDto(model: Auth): AuthDto {
    if (!model) {
      return;
    }
    return {
      ...model.hasOwnProperty('token') && {
        token: model.token
      },
      ...model.hasOwnProperty('uuid') && {
        uuid: model.uuid
      },
      ...model.hasOwnProperty('educatorSlug') && {
        educatorSlug: model.educatorSlug
      },
      ...model.hasOwnProperty('roles') && {
        roles: model.roles
      },
      ...model.hasOwnProperty('rights') && {
        rights: model.rights
      },
      ...model.hasOwnProperty('firstName') && {
        firstName: model.firstName
      },
      ...model.hasOwnProperty('lastName') && {
        lastName: model.lastName
      },
      ...model.hasOwnProperty('participantSlugs') && {
        participantSlugs: [...(model.participantSlugs || [])]
      },
      ...model.hasOwnProperty('clubSlug') && {
        clubSlug: model.clubSlug
      },
      ...model.hasOwnProperty('clubLogoUrl') && {
        clubLogoUrl: model.clubLogoUrl
      }
    };
  }

}

export const authMapper = new AuthMapper();
