import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminDayListComponent } from './admin-day-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminDayListComponent,
    data: {
      meta: {
        title: 'admin.day.list.seo.title',
        description: 'admin.day.list.seo.description',
        'twitter:title': 'admin.day.list.seo.title',
        'twitter:description': 'admin.day.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminDayListRoutingModule { }
