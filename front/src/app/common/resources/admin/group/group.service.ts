import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { groupMapper } from '@resources/admin/group/group.mapper';
import { Group, GroupDto } from '@resources/admin/group/group.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = (seasonSlug: string, competitionSlug: string, divisionSlug: string) =>
  `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/competitions/${competitionSlug}/divisions/${divisionSlug}/groups`;

interface SaveParams {
  dayCount?: number;
}

const toHttpParams = (saveParams: SaveParams): { [key: string]: string; } => ({
  ...saveParams && {
    ...saveParams.dayCount && {
      dayCount: '' + saveParams.dayCount
    }
  }
});

@Injectable({ providedIn: 'root' })
export class GroupService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, competitionSlug: string, divisionSlug: string, pageParams?: PageParams): Observable<Page<Group>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>
          this.http.get<GroupDto[]>(resourceUrl(seasonSlug, competitionSlug, divisionSlug), { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => groupMapper.toModel(dto)));
  }

  get(seasonSlug: string, competitionSlug: string, divisionSlug: string, slug: string): Observable<Group> {
    return this.http.get<GroupDto>(`${resourceUrl(seasonSlug, competitionSlug, divisionSlug)}/${slug}`)
      .pipe(map(dto => groupMapper.toModel(dto)));
  }

  save(seasonSlug: string, competitionSlug: string, divisionSlug: string, model: Group, saveParams?: SaveParams): Observable<Group> {
    const params = toHttpParams(saveParams);
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => groupMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<GroupDto>(`${resourceUrl(seasonSlug, competitionSlug, divisionSlug)}/${dto.slug}`, dto, { params })
          : this.http.post<GroupDto>(resourceUrl(seasonSlug, competitionSlug, divisionSlug), dto, { params })),
        map(dto => groupMapper.toModel(dto)));
  }

  delete(seasonSlug: string, competitionSlug: string, divisionSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug, competitionSlug, divisionSlug)}/${slug}`);
  }

}
