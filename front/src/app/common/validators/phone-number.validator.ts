import { Validators } from '@angular/forms';

export const phoneNumberValidator = Validators.pattern(/^(((\+|00)33\s?)|0)[1-9]\s?(\d{2}\s?){4}$/);
