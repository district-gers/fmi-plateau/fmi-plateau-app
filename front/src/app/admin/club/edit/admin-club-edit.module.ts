import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { AdminClubEditRoutingModule } from './admin-club-edit-routing.module';
import { AdminClubEditComponent } from './admin-club-edit.component';


@NgModule({
  declarations: [AdminClubEditComponent],
  imports: [
    CommonModule,
    AdminClubEditRoutingModule,
    PageFormModule,
    TranslateModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    LazyLoadImageModule,
    AsyncButtonModule
  ]
})
export class AdminClubEditModule { }
