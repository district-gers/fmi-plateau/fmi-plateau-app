import { Component, Input, OnInit } from '@angular/core';
import { Fmi } from '@common/resources/admin/fmi/fmi.model';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-fmi-read-scores',
  templateUrl: './fmi-read-scores.component.html',
  styleUrls: [
    './fmi-read-scores.component.scss',
    './fmi-read-scores.component.mobile.scss'
  ]
})
export class FmiReadScoresComponent implements OnInit {

  @Input() fmi: Fmi;
  screenSize$: Observable<ScreenSize>;

  constructor(
    private responsive: ResponsiveService) { }

  ngOnInit(): void {
    this.screenSize$ = this.responsive.size$;
  }

  trackBySlug(index, item): string {
    return item.slug;
  }

}
