import { HttpResponse } from '@angular/common/http';
import { Params } from '@angular/router';
import { Page, PageParams } from '@common/pagination/pagination';
import { omitBy } from 'lodash';
import { OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';

const HEADER_TOTAL_COUNT = 'X-Total-Count';

export const mapRouteToPageParams = (defaultSize: number): OperatorFunction<Params, PageParams> => source => source
  .pipe(
    map(queryParams => queryParams && ({
      ...queryParams,
      page: parseInt(queryParams.page, 10) || 0,
      size: parseInt(queryParams.size, 10) || defaultSize,
      sort: queryParams.sort && (() => {
        const sortParams = queryParams.sort.split(',');
        return {
          field: sortParams[0],
          direction: sortParams[1]
        };
      })() || null
    })));

export const mapPageToRouteParams = (): OperatorFunction<PageParams, Params> => source => source
  .pipe(map(pageParams => pageParams && ({
    ...pageParams,
    ...pageParams.hasOwnProperty('sort') && {
      sort: pageParams.sort && [pageParams.sort.field, pageParams.sort.direction]
        .filter(sortParam => !!sortParam).join(',')
    }
  })));

export const filterPageParams = (params: PageParams): PageParams => omitBy(params, (value, key) => !['page', 'size', 'sort'].includes(key));

export const mapPageToHttpParams = (appendParams = false): OperatorFunction<PageParams, { [key: string]: string; }> => source =>  source
  .pipe(
    map(params => params || {}),
    map(({ page, size, sort, ...pageParams }) => pageParams && ({
      ...appendParams && pageParams,
      ...page?.toString() && {
        page: page.toString()
      },
      ...size?.toString() && {
        size: size.toString(),
      },
      ...sort && {
        sort: [sort.field, sort.direction]
          .filter(sortParam => !!sortParam).join(',')
      }
    })));

export const mapHttpResponseToPage = <T>(params: PageParams): OperatorFunction<HttpResponse<T[]>, Page<T>> => source => source
  .pipe(
    map(response => ({
      ...params,
      items: response.body,
      count: parseInt(response.headers.get(HEADER_TOTAL_COUNT), 10) || response.body?.length
    })));

export const mapPageItems = <T, U>(itemMapper: (x: T) => U): OperatorFunction<Page<T>, Page<U>> => source => source
  .pipe(
    map(page => ({ ...page, items: page.items.map(itemMapper)})));

export const PAGE_SIZES = [5, 10, 25, 50];
