import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UserIsInChargeOfPlateauPipe } from './user-is-in-charge-of-plateau.pipe';

@NgModule({
  declarations: [UserIsInChargeOfPlateauPipe],
  imports: [
    CommonModule
  ],
  exports: [UserIsInChargeOfPlateauPipe],
  providers: [UserIsInChargeOfPlateauPipe]
})
export class UserIsInChargeOfPlateauModule { }
