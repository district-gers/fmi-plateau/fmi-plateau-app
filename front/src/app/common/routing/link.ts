import { Params } from '@angular/router';

export interface Link {
  label?: string;
  commands: string[];
  queryParams?: Params;
}
