import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-competition-list.module').then(module => module.AdminCompetitionListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-competition-edit.module').then(module => module.AdminCompetitionEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-competition-edit.module').then(module => module.AdminCompetitionEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-competition-edit.module').then(module => module.AdminCompetitionEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminCompetitionRoutingModule {
}
