import { FmiMatchStatus } from '@resources/admin/fmi/fmi.model';

export const requiredMatchCountsByTeamCount: { [teamCount: number]: number[]; } = {
  2: [1],
  3: [3],
  4: [4, 6],
  5: [5]
};

export const getRequiredMatchCount = (teamCount: number, matchCount: number): number => {
  return requiredMatchCountsByTeamCount[teamCount]
    ?.find((requiredMatchCount, index, array) => requiredMatchCount >= matchCount || index === array.length - 1);
};

export const getFmiMatchStatus = (teamCount: number, matchCount: number): FmiMatchStatus => {
  const requiredMatchCounts = requiredMatchCountsByTeamCount[teamCount];
  switch (true) {
    case !matchCount:
      return FmiMatchStatus.EMPTY;
    case requiredMatchCounts?.includes(matchCount) || matchCount >= requiredMatchCounts?.[requiredMatchCounts?.length - 1]:
      return FmiMatchStatus.COMPLETE;
    default:
      return FmiMatchStatus.PARTIAL;
  }
};
