import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { plateauMapper } from '@resources/admin/plateau/plateau.mapper';
import { Plateau, PlateauDto } from '@resources/admin/plateau/plateau.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

export interface QueryPageParams extends PageParams {
  category?: string;
  competition?: string;
  division?: string;
  group?: string;
  day?: string;
}
const resourceUrl = (seasonSlug: string) => `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/plateaux`;

@Injectable({ providedIn: 'root' })
export class PlateauService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, pageParams?: QueryPageParams): Observable<Page<Plateau>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(true),
        mergeMap(params =>  this.http.get<PlateauDto[]>(resourceUrl(seasonSlug), { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => plateauMapper.toModel(dto)));
  }

  findByFmi(seasonSlug: string, fmiSlug: string, pageParams?: PageParams): Observable<Page<Plateau>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>  this.http.get<PlateauDto[]>(resourceUrl(seasonSlug), {
          params: { ...params, fmi: fmiSlug },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => plateauMapper.toModel(dto)));
  }

  get(seasonSlug: string, slug: string): Observable<Plateau> {
    return this.http.get<PlateauDto>(`${resourceUrl(seasonSlug)}/${slug}`)
      .pipe(map(dto => plateauMapper.toModel(dto)));
  }

  save(seasonSlug: string, model: Plateau): Observable<Plateau> {
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => plateauMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<PlateauDto>(`${resourceUrl(seasonSlug)}/${dto.slug}`, dto)
          : this.http.post<PlateauDto>(resourceUrl(seasonSlug), dto)),
        map(dto => plateauMapper.toModel(dto)));
  }

  delete(seasonSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug)}/${slug}`);
  }

}
