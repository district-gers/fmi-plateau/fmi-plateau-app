import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { teamMapper } from '@resources/admin/team/team.mapper';
import { Team, TeamDto } from '@resources/admin/team/team.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

export interface QueryPageParams extends PageParams {
  category?: string;
  participant?: string;
}
const resourceUrl = (seasonSlug: string) => `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/teams`;

@Injectable({ providedIn: 'root' })
export class TeamService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, pageParams?: QueryPageParams): Observable<Page<Team>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(true),
        mergeMap(params => this.http.get<TeamDto[]>(resourceUrl(seasonSlug), { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => teamMapper.toModel(dto)));
  }

  findByCategory(seasonSlug: string, categorySlug: string, pageParams?: PageParams): Observable<Page<Team>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params => this.http.get<TeamDto[]>(resourceUrl(seasonSlug), {
          params: { ...params, category: categorySlug },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => teamMapper.toModel(dto)));
  }

  findByParticipant(seasonSlug: string, participantSlug: string, pageParams?: PageParams): Observable<Page<Team>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params => this.http.get<TeamDto[]>(resourceUrl(seasonSlug), {
          params: { ...params, participant: participantSlug },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => teamMapper.toModel(dto)));
  }

  get(seasonSlug: string, slug: string): Observable<Team> {
    return this.http.get<TeamDto>(`${resourceUrl(seasonSlug)}/${slug}`)
      .pipe(map(dto => teamMapper.toModel(dto)));
  }

  save(seasonSlug: string, model: Team): Observable<Team> {
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => teamMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<TeamDto>(`${resourceUrl(seasonSlug)}/${dto.slug}`, dto)
          : this.http.post<TeamDto>(resourceUrl(seasonSlug), dto)),
        map(dto => teamMapper.toModel(dto)));
  }

  delete(seasonSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug)}/${slug}`);
  }

}
