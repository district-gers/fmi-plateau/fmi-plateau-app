import { Member } from '@resources/admin/fmi/fmi.model';
import { Observable } from 'rxjs';

export interface FmiTeamMemberConfig {
  member: Member;
  members$: Observable<Member[]>;
  membersRefresh$?: Observable<void>;
  extraFilterFn?: () => (member: Member) => boolean;
  categories$?: Observable<string[]>;
  analyticsLabel: string;
  labelKeys: {
    member: string;
    firstName: string;
    lastName: string;
    code: string;
    category?: string;
  };
}
