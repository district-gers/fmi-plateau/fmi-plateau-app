import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PlateauRoutingModule } from './plateau-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PlateauRoutingModule
  ]
})
export class PlateauModule { }
