import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';

import { AdminSeasonEditRoutingModule } from './admin-season-edit-routing.module';
import { AdminSeasonEditComponent } from './admin-season-edit.component';

@NgModule({
  declarations: [AdminSeasonEditComponent],
  imports: [
    CommonModule,
    AdminSeasonEditRoutingModule,
    PageFormModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    AsyncButtonModule,
    MatCheckboxModule
  ]
})
export class AdminSeasonEditModule { }
