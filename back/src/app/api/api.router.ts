import { Router } from 'express';
import { systemRouter } from './system/system.router';
import { defaultQueryParamsMiddleware } from '../common/query-params-filter.middleware';

export const apiRouter = Router();

apiRouter.use('/system', defaultQueryParamsMiddleware, systemRouter);
