import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-fmi-list.module').then(module => module.AdminFmiListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-fmi-edit.module').then(module => module.AdminFmiEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-fmi-edit.module').then(module => module.AdminFmiEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-fmi-edit.module').then(module => module.AdminFmiEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminFmiRoutingModule { }
