interface GroupTeamDto {
  slug?: string;
  index?: number;
  participantSlug?: string;
  participantName?: string;
  participantShortName?: string;
  participantLogoUrl?: string;
}

export interface GroupTeam {
  slug?: string;
  index?: number;
  participant?: string;
  name?: string;
  shortName?: string;
  logoUrl?: string;
}

export interface GroupDto {
  slug?: string;
  index: number;
  name: string;
  teams?: GroupTeamDto[];
  teamSlugs?: string[];
}

export interface Group {
  slug?: string;
  index: number;
  name: string;
  teams?: GroupTeam[];
}
