import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthStorageService } from '@common/auth-storage.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authStorageService: AuthStorageService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authStorageService.getToken();
    if (token) {
      const headers = req.headers.append('Authorization', `Bearer ${token}`);
      return next.handle(req.clone({ headers }));
    }
    return next.handle(req);
  }
}
