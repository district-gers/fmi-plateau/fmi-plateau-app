import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { CookiesStatus } from './cookies-status';
import { CookiesService } from './cookies.service';

@Injectable({ providedIn: 'root' })
export class CookiesServerService extends CookiesService {

  allow(): void {
  }

  deny(): void {
  }

  get cookiesStatus$(): Observable<CookiesStatus> {
    return of(CookiesStatus.ALLOW);
  }

}
