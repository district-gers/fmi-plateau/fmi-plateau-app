import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { divisionMapper } from '@resources/admin/division/division.mapper';
import { Division, DivisionDto } from '@resources/admin/division/division.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = (seasonSlug: string, competitionSlug: string) =>
  `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/competitions/${competitionSlug}/divisions`;

interface SaveParams {
  groupCount?: number;
}

const toHttpParams = (saveParams: SaveParams): { [key: string]: string; } => ({
  ...saveParams && {
    ...saveParams.groupCount && {
      groupCount: '' + saveParams.groupCount
    }
  }
});

@Injectable({ providedIn: 'root' })
export class DivisionService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, competitionSlug: string, pageParams?: PageParams): Observable<Page<Division>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params =>
          this.http.get<DivisionDto[]>(resourceUrl(seasonSlug, competitionSlug), { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => divisionMapper.toModel(dto)));
  }

  get(seasonSlug: string, competitionSlug: string, slug: string): Observable<Division> {
    return this.http.get<DivisionDto>(`${resourceUrl(seasonSlug, competitionSlug)}/${slug}`)
      .pipe(map(dto => divisionMapper.toModel(dto)));
  }

  save(seasonSlug: string, competitionSlug: string, model: Division, saveParams?: SaveParams): Observable<Division> {
    const params = toHttpParams(saveParams);
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => divisionMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<DivisionDto>(`${resourceUrl(seasonSlug, competitionSlug)}/${dto.slug}`, dto, { params })
          : this.http.post<DivisionDto>(resourceUrl(seasonSlug, competitionSlug), dto, { params })),
        map(dto => divisionMapper.toModel(dto)));
  }

  delete(seasonSlug: string, competitionSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug, competitionSlug)}/${slug}`);
  }

}
