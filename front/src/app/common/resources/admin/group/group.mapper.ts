import { Group, GroupDto } from '@resources/admin/group/group.model';

class GroupMapper {

  toModel(dto: GroupDto): Group {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      index: dto.index,
      name: dto.name,
      teams: [...(dto.teams || [])]
        .map(team => ({
          slug: team.slug,
          index: team.index,
          participant: team.participantSlug,
          name: team.participantName,
          shortName: team.participantShortName,
          logoUrl: team.participantLogoUrl
        }))
        .sort((t1, t2) => t1.name?.toLocaleLowerCase()?.localeCompare(t2.name?.toLocaleLowerCase()) || t1.index - t2.index)
    };
  }

  toDto(model: Group): GroupDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('index') && {
        index: model.index
      },
      ...model.hasOwnProperty('name') && {
        name: model.name
      },
      ...model.hasOwnProperty('teams') && {
        teamSlugs: model?.teams.map(({ slug }) => slug)
      }
    };
  }

}

export const groupMapper = new GroupMapper();
