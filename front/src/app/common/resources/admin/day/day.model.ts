import { Moment } from 'moment';

export interface DayDto {
  slug?: string;
  index: number;
  date: string;
}

export interface Day {
  slug?: string;
  index: number;
  date?: Moment;
}
