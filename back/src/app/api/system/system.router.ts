import { NextFunction, Request, Response, Router } from 'express';
import { systemController } from './system.controller';

export const systemRouter = Router();

systemRouter
  .delete('/reset/memcache', (req: Request, res: Response, next: NextFunction) => {
    systemController.clearMemcache(req, res, next);
  });
