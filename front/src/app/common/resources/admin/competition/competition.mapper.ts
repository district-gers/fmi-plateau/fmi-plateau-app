import { Competition, CompetitionDto } from '@resources/admin/competition/competition.model';

class CompetitionMapper {

  toModel(dto: CompetitionDto): Competition {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      name: dto.name,
      category: dto.categorySlug,
      categoryName: dto.categoryName
    };
  }

  toDto(model: Competition): CompetitionDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('name') && {
        name: model.name
      },
      ...model.hasOwnProperty('category') && {
        categorySlug: model.category
      }
    };
  }

}

export const competitionMapper = new CompetitionMapper();
