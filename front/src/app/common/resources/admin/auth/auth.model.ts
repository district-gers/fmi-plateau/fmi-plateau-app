import { Right } from '@resources/user/right';
import { Role } from '@resources/user/role';

export interface AuthDto {
  token: string;
  uuid?: string;
  educatorSlug?: string;
  roles?: Role[];
  rights: Right[];
  firstName?: string;
  lastName?: string;
  participantSlugs?: string[];
  clubSlug?: string;
  clubLogoUrl?: string;
}

export interface Auth {
  token: string;
  uuid?: string;
  educatorSlug?: string;
  roles?: Role[];
  rights: Right[];
  firstName?: string;
  lastName?: string;
  participantSlugs?: string[];
  clubSlug?: string;
  clubLogoUrl?: string;
}
