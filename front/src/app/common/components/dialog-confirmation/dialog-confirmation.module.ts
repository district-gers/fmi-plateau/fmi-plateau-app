import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateModule } from '@ngx-translate/core';

import { DialogConfirmationComponent } from './dialog-confirmation.component';

@NgModule({
  declarations: [DialogConfirmationComponent],
  entryComponents: [DialogConfirmationComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    TranslateModule,
    MatIconModule
  ],
  providers: [
    DialogConfirmationService
  ]
})
export class DialogConfirmationModule { }
