import { NextFunction, Request, Response } from 'express';
import { logger } from '../logger/logger';
import { FmiPlateauErrorType } from './error';

// Do not remove next parameter event if it is not used, since Express requires it to consider it as a middleware
export const errorMiddleware = (error, req: Request, res: Response, next: NextFunction) => {
  let errorType: FmiPlateauErrorType;
  let logMessage: string;
  let details = {};
  if (error && error.type && error.type instanceof FmiPlateauErrorType) {
    logMessage = error.stack;
    errorType = error.type;
    details = error.details instanceof Object ? error.details : { details: error.details };
  } else if (error instanceof Error) {
    logMessage = error.stack;
    errorType = FmiPlateauErrorType.INTERNAL_ERROR;
  } else {
    logMessage = error instanceof Object ? JSON.stringify(error) : error;
    errorType = FmiPlateauErrorType.INTERNAL_ERROR;
  }
  logger.error(logMessage, {url: req.originalUrl, ...details});
  res.status(errorType.httpCode).json({
    code: errorType.code,
    message: errorType.description
  });
};
