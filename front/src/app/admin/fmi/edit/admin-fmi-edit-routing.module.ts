import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminFmiEditComponent } from './admin-fmi-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminFmiEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminFmiEditRoutingModule { }
