import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminCompetitionListComponent } from './admin-competition-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminCompetitionListComponent,
    data: {
      meta: {
        title: 'admin.competition.list.seo.title',
        description: 'admin.competition.list.seo.description',
        'twitter:title': 'admin.competition.list.seo.title',
        'twitter:description': 'admin.competition.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminCompetitionListRoutingModule { }
