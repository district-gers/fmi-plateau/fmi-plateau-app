import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { competitionMapper } from '@resources/admin/competition/competition.mapper';
import { Competition, CompetitionDto } from '@resources/admin/competition/competition.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

const resourceUrl = (seasonSlug: string) => `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/competitions`;

interface SaveParams {
  divisionCount?: number;
}

const toHttpParams = (saveParams: SaveParams): { [key: string]: string; } => ({
  ...saveParams && {
    ...saveParams.divisionCount && {
      divisionCount: '' + saveParams.divisionCount
    }
  }
});

@Injectable({ providedIn: 'root' })
export class CompetitionService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, pageParams?: PageParams): Observable<Page<Competition>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params => this.http.get<CompetitionDto[]>(resourceUrl(seasonSlug), { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => competitionMapper.toModel(dto)));
  }

  findByCategory(seasonSlug: string, categorySlug: string, pageParams?: PageParams): Observable<Page<Competition>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params => this.http.get<CompetitionDto[]>(resourceUrl(seasonSlug), {
          params: { ...params, category: categorySlug },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => competitionMapper.toModel(dto)));
  }

  get(seasonSlug: string, slug: string): Observable<Competition> {
    return this.http.get<CompetitionDto>(`${resourceUrl(seasonSlug)}/${slug}`)
      .pipe(map(dto => competitionMapper.toModel(dto)));
  }

  save(seasonSlug: string, model: Competition, saveParams?: SaveParams): Observable<Competition> {
    const params = toHttpParams(saveParams);
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => competitionMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<CompetitionDto>(`${resourceUrl(seasonSlug)}/${dto.slug}`, dto, { params })
          : this.http.post<CompetitionDto>(resourceUrl(seasonSlug), dto, { params })),
        map(dto => competitionMapper.toModel(dto)));
  }

  delete(seasonSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug)}/${slug}`);
  }

}
