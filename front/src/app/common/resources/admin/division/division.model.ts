export interface DivisionDto {
  slug?: string;
  index: number;
  name: string;
}

export interface Division {
  slug?: string;
  index: number;
  name: string;
}
