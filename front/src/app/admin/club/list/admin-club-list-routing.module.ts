import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminClubListComponent } from './admin-club-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminClubListComponent,
    data: {
      meta: {
        title: 'admin.club.list.seo.title',
        description: 'admin.club.list.seo.description',
        'twitter:title': 'admin.club.list.seo.title',
        'twitter:description': 'admin.club.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminClubListRoutingModule { }
