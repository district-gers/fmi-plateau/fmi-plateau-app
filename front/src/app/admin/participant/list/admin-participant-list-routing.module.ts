import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminParticipantListComponent } from './admin-participant-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminParticipantListComponent,
    data: {
      meta: {
        title: 'admin.participant.list.seo.title',
        description: 'admin.participant.list.seo.description',
        'twitter:title': 'admin.participant.list.seo.title',
        'twitter:description': 'admin.participant.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminParticipantListRoutingModule { }
