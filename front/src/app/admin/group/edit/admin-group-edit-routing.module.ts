import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminGroupEditComponent } from './admin-group-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminGroupEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminGroupEditRoutingModule { }
