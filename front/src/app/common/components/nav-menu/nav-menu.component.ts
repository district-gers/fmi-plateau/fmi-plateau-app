import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatExpansionPanel } from '@angular/material/expansion';
import { ActivatedRoute, ActivatedRouteSnapshot, Event, NavigationEnd, Params, Router } from '@angular/router';
import { CurrentUserService } from '@common/current-user.service';
import { adminLinks, NavMenuLink, plateauLinks, rootLinks } from '@components/nav-menu/nav-menu.util';
import { Auth } from '@resources/admin/auth/auth.model';
import { Right } from '@resources/user/right';
import { isEqual } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, takeUntil } from 'rxjs/operators';

const getRouteParam = (paramKey: string, route: ActivatedRouteSnapshot): string => {
  const routeParamValue = route.params[paramKey];
  if (routeParamValue) {
    return routeParamValue;
  }
  const child = route.firstChild;
  if (child) {
    return getRouteParam(paramKey, child);
  }
};

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements OnInit, OnDestroy {

  user$: Observable<Auth>;
  plateauSlug$: Observable<string>;
  fmiSlug$: Observable<string>;
  queryParams$: Observable<Params>;

  rightEnum = Right;

  rootLinks: NavMenuLink[] = rootLinks;
  adminLinks: NavMenuLink[] = adminLinks;

  @ViewChild('adminExpansionPanel', { static: false, read: MatExpansionPanel }) adminExpansionPanel: MatExpansionPanel;

  private destroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private currentUser: CurrentUserService) {
  }

  ngOnInit(): void {
    this.user$ = this.currentUser.get$();

    this.queryParams$ = this.route.queryParams
      .pipe(distinctUntilChanged(isEqual));

    this.plateauSlug$ = this.router.events
      .pipe(
        filter((event: Event) => event instanceof NavigationEnd),
        map(() => getRouteParam('plateauSlug', this.route.snapshot)));

    this.fmiSlug$ = this.router.events
      .pipe(
        filter((event: Event) => event instanceof NavigationEnd),
        map(() => getRouteParam('fmiSlug', this.route.snapshot)));

    this.router.events
      .pipe(
        takeUntil(this.destroy$),
        filter((event: Event) => event instanceof NavigationEnd),
        filter(() => !this.isParentLinkActive('/admin'))
      )
      .subscribe(() => this.adminExpansionPanel?.close());
  }

  ngOnDestroy(): void {
    this.destroy$.complete();
  }

  isLinkActive(url: string): boolean {
    const queryParamsIndex = this.router.url.indexOf('?');
    const baseUrl = queryParamsIndex > -1 ? this.router.url.slice(0, queryParamsIndex) : this.router.url;
    return baseUrl === url;
  }

  isParentLinkActive(url: string): boolean {
    const queryParamsIndex = this.router.url.indexOf('?');
    const baseUrl = queryParamsIndex > -1 ? this.router.url.slice(0, queryParamsIndex) : this.router.url;
    return baseUrl.startsWith(url);
  }

  plateauLinks(plateauSlug: string, fmiSlug: string): NavMenuLink[] {
    return plateauLinks(plateauSlug, fmiSlug);
  }

  trackByCommands(index: number, link: NavMenuLink) {
    return link.routerCommands.map(command => command.replace(/(^\/)|(\/$)/g, '')).join('/');
  }

}
