import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { AsyncSelectModule } from '@components/async-select/async-select.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';
import { CategoryLabelModule } from '@pipes/category-label/category-label.module';
import { LicenseNumberModule } from '@pipes/license-number/license-number.module';
import { TeamNameModule } from '@pipes/team-name/team-name.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { AdminPlayerEditRoutingModule } from './admin-player-edit-routing.module';
import { AdminPlayerEditComponent } from './admin-player-edit.component';


@NgModule({
  declarations: [AdminPlayerEditComponent],
  imports: [
    CommonModule,
    AdminPlayerEditRoutingModule,
    PageFormModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    CategoryLabelModule,
    TeamNameModule,
    LazyLoadImageModule,
    LicenseNumberModule,
    AsyncButtonModule,
    AsyncSelectModule
  ]
})
export class AdminPlayerEditModule { }
