import { Injectable } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { ConnectableObservable, Observable } from 'rxjs';
import { distinctUntilChanged, map, publishBehavior } from 'rxjs/internal/operators';

import { BREAKPOINTS } from './responsive.config';

export type ScreenSizeAlias = 'xxs' | 'xs' | 'sm' | 'md' | 'lg' | 'xl';

export interface ScreenSize {
  index: number;
  alias: ScreenSizeAlias;
}

export const SCREEN_SIZES: { [alias: string]: ScreenSize; } = BREAKPOINTS
  .reduce((acc, { alias }, index) => ({ ...acc, [alias]: { index, alias }}), {});

@Injectable({
  providedIn: 'root'
})
export class ResponsiveService {

  private _size$: ConnectableObservable<ScreenSize>;

  constructor(private media: MediaObserver) {
    this._size$ = this.media.asObservable().pipe(
      map(([mediaChange]: MediaChange[]) => SCREEN_SIZES[mediaChange.mqAlias] || SCREEN_SIZES.xxs),
      distinctUntilChanged(),
      publishBehavior(Object.entries(SCREEN_SIZES)
          .map(([, size]) => size)
          .find((size: ScreenSize): boolean => this.media.isActive(size.alias)) || SCREEN_SIZES.xxs)) as ConnectableObservable<ScreenSize>;
    this._size$.connect();
  }

  get size$(): Observable<ScreenSize> {
    return this._size$;
  }

  private is({ alias }: ScreenSize): Observable<boolean> {
    return this.size$.pipe(
      map((size: ScreenSize): boolean => (size || SCREEN_SIZES.xss).alias === alias),
      distinctUntilChanged());
  }

  private isOrLower({ index }: ScreenSize): Observable<boolean> {
    return this.size$.pipe(
      map((size: ScreenSize): boolean => (size || SCREEN_SIZES.xss).index <= index),
      distinctUntilChanged());
  }

  // tslint:disable-next-line:no-unused-variable
  private isOrHigher({ index }: ScreenSize): Observable<boolean> {
    return this.size$.pipe(
      map((size: ScreenSize): boolean => (size || SCREEN_SIZES.xss).index >= index),
      distinctUntilChanged());
  }

  get isXs$(): Observable<boolean> {
    return this.is(SCREEN_SIZES.xs);
  }

  get isXsOrLower$(): Observable<boolean> {
    return this.isOrLower(SCREEN_SIZES.xs);
  }

  get isXxsOrLower$(): Observable<boolean> {
    return this.isOrLower(SCREEN_SIZES.xxs);
  }

  get isSm$(): Observable<boolean> {
    return this.is(SCREEN_SIZES.sm);
  }

  get isSmOrLower$(): Observable<boolean> {
    return this.isOrLower(SCREEN_SIZES.sm);
  }

}
