import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorRedirectInterceptor implements HttpInterceptor {

  constructor(private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.method === 'GET') {
      return next.handle(req).pipe(
        catchError(error => {
          if (error instanceof HttpErrorResponse) {
            const route = (() => {
              switch ((error as HttpErrorResponse).status) {
                case 404:
                  return ['/error', 'not-found'];
                case 0:
                case 504:
                  return ['/error', 'server-unreachable'];
                default:
                  return ['/error', 'internal'];
              }
            })();
            this.router.navigate(route);
          }
          return throwError(error);
        }));
    }
    return next.handle(req);
  }
}
