import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminFmiRoutingModule } from './admin-fmi-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminFmiRoutingModule
  ]
})
export class AdminFmiModule { }
