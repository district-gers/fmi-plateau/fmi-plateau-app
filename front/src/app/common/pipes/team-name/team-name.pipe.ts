import { Pipe, PipeTransform } from '@angular/core';
import { SCREEN_SIZES, ScreenSize, ScreenSizeAlias } from '@common/responsive/responsive.service';

interface Name {
  name?: string;
  shortName?: string;
}

@Pipe({
  name: 'teamName'
})
export class TeamNamePipe<T extends Name> implements PipeTransform {

  transform(team: T, screenSize: ScreenSize, switchSize: ScreenSizeAlias = 'sm'): string {
    if (!team) {
      return '';
    }
    return (screenSize?.index || 0) > SCREEN_SIZES[switchSize].index ? team.name : team.shortName || team.name;
  }

}
