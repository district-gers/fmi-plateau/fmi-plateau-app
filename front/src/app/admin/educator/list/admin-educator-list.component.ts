import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { emptyPage, Page, PageParams } from '@common/pagination/pagination';
import { RefreshEvent, RefreshEventService } from '@common/refresh-event.service';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Club } from '@resources/admin/club/club.model';
import { ClubService } from '@resources/admin/club/club.service';
import { Educator } from '@resources/admin/educator/educator.model';
import { EducatorService } from '@resources/admin/educator/educator.service';
import { Participant } from '@resources/admin/participant/participant.model';
import { ParticipantService } from '@resources/admin/participant/participant.service';
import { controlValueChange$ } from '@utils/form.util';
import { isEqual } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, mergeMap, startWith, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

interface FilterPageParams extends PageParams {
  categories?: string;
  clubs?: string;
  participants?: string;
}

const parseParamsArrayStr = (...arrayFields: string[]) => (params: { [key: string]: any; }): { [key: string]: string[]; } => ({
  ...params && Object.entries(params)
    .filter(([key]) => arrayFields.includes(key))
    .map(([key, value]): [string, string[]] => [key, value?.split(',')])
    .map(([key, value]): [string, string[]] => [key, value?.length ? value : undefined])
    .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {})
});

@Component({
  selector: 'app-admin-educator-list',
  templateUrl: './admin-educator-list.component.html',
  styleUrls: [
    './admin-educator-list.component.scss',
    './admin-educator-list.component.responsive.scss'
  ]
})
export class AdminEducatorListComponent extends AbstractAdminListComponent<Educator> implements OnInit {

  columns = ['firstName', 'lastName', 'code', 'clubName', 'action'];
  screenSize$: Observable<ScreenSize>;
  clubs$: Observable<Page<Club>>;
  categories$: Observable<Page<Category>>;
  participants$: Observable<Page<Participant>>;
  formGroup: FormGroup;

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private educator: EducatorService,
    private club: ClubService,
    private category: CategoryService,
    private participant: ParticipantService,
    private refreshEvent: RefreshEventService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;

    const season$ = this.currentSeason.get$();

    this.clubs$ = season$
      .pipe(
        tap(() => this.loading.clubs = true),
        mergeMap(season => this.club.find(season.slug)
          .pipe(finalize(() => this.loading.clubs = false))),
        finalize(() => this.loading.clubs = false));

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.categories = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.categories = false))),
        finalize(() => this.loading.categories = false));

    this.participants$ = combineLatest([ season$, this.route.queryParams.pipe(map(parseParamsArrayStr('categories'))) ])
      .pipe(
        map(([season, { categories }]) => ({ season, categorySlugs: categories })),
        filter(({ season, categorySlugs }) => season && !!categorySlugs?.length),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.participants = true),
        switchMap(({ season, categorySlugs }) => this.participant.findByCategories(season.slug, categorySlugs)
          .pipe(finalize(() => this.loading.participants = false))),
        finalize(() => this.loading.participants = false));

    this.formGroup = this.fb.group({
        raw: this.fb.group({
          clubs: [],
        }, {
          validators: (control: AbstractControl) => {
            const filledFieldsCount = Object.values(control.value).filter(value => Array.isArray(value) && value?.length).length < 2;
            return filledFieldsCount ? { required: filledFieldsCount } : null;
          }
        }),
        competition: this.fb.group({
          categories: [],
          participants: []
        }, {
          validators: (control: AbstractControl) => {
            const filledFieldsCount = Object.values(control.value).filter(value => Array.isArray(value) && value?.length).length < 2;
            return filledFieldsCount ? { required: filledFieldsCount } : null;
          }
        })
      },
      {
        validators: (control: AbstractControl) => {
          const hasAnySubControlValid = Object.values((control as FormGroup).controls).some(subControl => subControl.valid);
          return hasAnySubControlValid ? null : { required: !hasAnySubControlValid };
        }
      });
    const rawFormGroup = this.formGroup.controls.raw as FormGroup;
    const competitionFormGroup = this.formGroup.controls.competition as FormGroup;

    this.route.queryParams
      .pipe(
        takeUntil(this.destroy$),
        map(parseParamsArrayStr('clubs', 'categories', 'participants')))
      .subscribe(({ clubs, categories, participants }) => this.formGroup
        .patchValue({ raw: { clubs }, competition: { categories, participants } }));

    this.formGroup.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        startWith(this.formGroup.value as object),
        map(() => this.formGroup.getRawValue()),
        distinctUntilChanged(isEqual))
      .subscribe(({ raw: { clubs }, competition: { categories } }: { [_: string]: { [_: string]: string[] }; }) => {
        switch (true) {
          case !!(clubs?.length):
            rawFormGroup.enable();
            competitionFormGroup.patchValue({ categories: undefined, participants: undefined });
            competitionFormGroup.disable();
            break;
          case !!categories?.length:
            rawFormGroup.patchValue({ clubs: undefined });
            rawFormGroup.disable();
            competitionFormGroup.enable();
            break;
          default:
            rawFormGroup.enable();
            competitionFormGroup.enable();
        }
      });

    controlValueChange$<string[]>(competitionFormGroup.controls.categories)
      .pipe(takeUntil(this.destroy$))
      .subscribe(categories => categories?.length
        ? competitionFormGroup.controls.participants.enable()
        : competitionFormGroup.controls.participants.disable());
  }

  clubsChange(clubs: string[]) {
    this.refresh$.next({ clubs: clubs?.join(',') || undefined, page: undefined } as FilterPageParams);
  }

  categoriesChange(categories: string[]) {
    this.refresh$.next({ categories: categories?.join(',') || undefined, participants: undefined, page: undefined } as FilterPageParams);
  }

  participantsChange(participants: string[]) {
    this.refresh$.next({ participants: participants?.join(',') || undefined, page: undefined } as FilterPageParams);
  }

  protected delete$(item: Educator): Observable<void> {
    return this.educator.delete(this.currentSeason.get().slug, item.slug)
      .pipe(tap(() => this.refreshEvent.pushEvent(RefreshEvent.USER)));
  }

  protected find$(params: PageParams): Observable<Page<Educator>> {
    const criteria = parseParamsArrayStr('clubs', 'participants')(params);
    if (criteria.categories?.length && !criteria.participants?.length) {
      return of(emptyPage);
    }
    return this.currentSeason.get$().pipe(
      take(1),
      mergeMap(season => this.educator.find(season.slug, criteria, params)));
  }

  protected getItemLabel(item: Educator): string {
    return `${item.firstName} ${item.lastName}`;
  }

  protected getItemSlug(item: Educator): string {
    return item.slug;
  }

  asFormGroup(control: AbstractControl): FormGroup {
    return control as FormGroup;
  }

}
