import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { difference } from '@common/object.util';
import { Page, PageParams } from '@common/pagination/pagination';
import { filterPageParams } from '@common/pagination/pagination.util';
import { ResponsiveService, SCREEN_SIZES, ScreenSize } from '@common/responsive/responsive.service';
import { Week } from '@common/week';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { Day } from '@resources/admin/day/day.model';
import { DayService } from '@resources/admin/day/day.service';
import { Division } from '@resources/admin/division/division.model';
import { DivisionService } from '@resources/admin/division/division.service';
import { Group } from '@resources/admin/group/group.model';
import { GroupService } from '@resources/admin/group/group.service';
import { Plateau } from '@resources/admin/plateau/plateau.model';
import { PlateauService } from '@resources/admin/plateau/plateau.service';
import { tapUniqueItem } from '@utils/rxjs.util';
import { isEqual } from 'lodash';
import { Moment } from 'moment';
import * as moment from 'moment';
import { combineLatest, Observable, of } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  finalize,
  map,
  mergeMap,
  pairwise,
  startWith,
  switchMap,
  take,
  takeUntil,
  tap
} from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

interface FilterPageParams extends PageParams {
  category?: string;
  competition?: string;
  division?: string;
  group?: string;
  day?: string;
  startDate?: string;
  endDate?: string;
}

@Component({
  selector: 'app-admin-plateau-list',
  templateUrl: './admin-plateau-list.component.html',
  styleUrls: [
    './admin-plateau-list.component.scss',
    './admin-plateau-list.component.responsive.scss'
  ]
})
export class AdminPlateauListComponent extends AbstractAdminListComponent<Plateau> implements OnInit {

  columns = [
    'date', 'location',
    'categoryName', 'competitionName', 'divisionName', 'groupName', 'dayIndex',
    'teams', 'participant', 'action'
  ];
  categories$: Observable<Page<Category>>;
  competitions$: Observable<Page<Competition>>;
  divisions$: Observable<Page<Division>>;
  groups$: Observable<Page<Group>>;
  days$: Observable<Page<Day>>;
  formGroup: FormGroup;
  screenSize$: Observable<ScreenSize>;
  weeks$: Observable<Week[]>;
  SCREEN_SIZES = SCREEN_SIZES;

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private plateau: PlateauService,
    private category: CategoryService,
    private competition: CompetitionService,
    private division: DivisionService,
    private group: GroupService,
    private day: DayService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;

    const season$ = this.currentSeason.get$();

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.category = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().category && this.categoryChange(slug)),
        finalize(() => this.loading.category = false));

    this.competitions$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { category }]) => ({ season, categorySlug: category })),
        filter(({ season, categorySlug }) => season && categorySlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.competition = true),
        switchMap(({ season, categorySlug }) => this.competition.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.competition = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().competition && this.competitionChange(slug)),
        finalize(() => this.loading.competition = false));

    this.divisions$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { competition }]) => ({ season, competitionSlug: competition })),
        filter(({ season, competitionSlug }) => season && competitionSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.division = true),
        switchMap(({ season, competitionSlug }) => this.division.find(season.slug, competitionSlug)
          .pipe(finalize(() => this.loading.division = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().division && this.divisionChange(slug)),
        finalize(() => this.loading.division = false));

    this.groups$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { competition, division }]) => ({ season, competitionSlug: competition, divisionSlug: division })),
        filter(({ season, competitionSlug, divisionSlug }) => season && competitionSlug && divisionSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.group = true),
        switchMap(({ season, competitionSlug, divisionSlug }) => this.group.find(season.slug, competitionSlug, divisionSlug)
          .pipe(finalize(() => this.loading.group = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().group && this.groupChange(slug)),
        finalize(() => this.loading.group = false));

    this.days$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        map(([season, { competition, division, group }]) => ({
          season, competitionSlug: competition, divisionSlug: division, groupSlug: group })),
        filter(({ season, competitionSlug, divisionSlug, groupSlug }) => season && competitionSlug && divisionSlug && groupSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.day = true),
        switchMap(({ season, competitionSlug, divisionSlug, groupSlug }) => this.day
          .find(season.slug, competitionSlug, divisionSlug, groupSlug)
          .pipe(finalize(() => this.loading.day = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().day && this.dayChange(slug)),
        finalize(() => this.loading.day = false));

    const weeksWindowSize = 64;
    this.weeks$ = of(
      Array(weeksWindowSize).fill(null)
        .reduce(
          (weeks: Moment[]): Moment[] => [...weeks, weeks[weeks.length - 1].clone().add(1, 'week')],
          [moment().startOf('isoWeek').add(- weeksWindowSize + 12, 'weeks')])
        .map((week: Moment) => ({
          week: week.isoWeek(),
          start: week,
          end: week.clone().endOf('isoWeek')
        }))
        .sort((week1, week2) => week2.start - week1.start));

    this.formGroup = this.fb.group({
      category: [],
      competition: [],
      division: [],
      group: [],
      day: [],
      week: [],
      startDate: [],
      endDate: []
    });
    this.formGroup.controls.competition.disable();
    this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();

    this.route.queryParams
      .pipe(
        takeUntil(this.destroy$),
        startWith({} as FilterPageParams),
        map(({ category, competition, division, group, day, startDate, endDate }) => ({
          category, competition, division, group, day,
          startDate: startDate && moment(startDate).isValid() ? moment(startDate) : undefined,
          endDate: endDate && moment(endDate).isValid() ? moment(endDate) : undefined
        })),
        pairwise(),
        map(([previous, current]) => difference(current, previous)),
        distinctUntilChanged(isEqual))
      .subscribe(changes => this.formGroup.patchValue({
        ...changes,
        ...(changes.hasOwnProperty('startDate') || changes.hasOwnProperty('endDate')) && {
          week: changes.startDate?.isSame(changes.startDate.clone().startOf('isoWeek'), 'second')
            && changes.endDate?.isSame(changes.endDate.clone().endOf('isoWeek'), 'second')
            ? { start: changes.startDate, end: changes.endDate }
            : undefined
        }
      }));

    this.formGroup.controls.startDate.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        map((date: Moment) => date?.isValid() ? date.clone().utc().format() : undefined),
        distinctUntilChanged((d1, d2) => d1 === d2 || d1 && d2 && moment(d1).isSame(d2)))
      .subscribe((startDate: string) => this.refresh$.next({ startDate, page: undefined } as FilterPageParams));

    this.formGroup.controls.endDate.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        map((date: Moment) => date?.isValid() ? date.clone().endOf('day').utc().format() : undefined),
        distinctUntilChanged((d1, d2) => d1 === d2 || d1 && d2 && moment(d1).isSame(d2)))
      .subscribe((endDate: string) => this.refresh$.next({ endDate, page: undefined } as FilterPageParams));
  }

  categoryChange(category: string) {
    this.refresh$.next({ category, competition: undefined, division: undefined, group: undefined, day: undefined,
      page: undefined } as FilterPageParams);
    category ? this.formGroup.controls.competition.enable() : this.formGroup.controls.competition.disable();
    this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();
  }

  competitionChange(competition: string) {
    this.refresh$.next({ competition, division: undefined, group: undefined, day: undefined, page: undefined } as FilterPageParams);
    competition ? this.formGroup.controls.division.enable() : this.formGroup.controls.division.disable();
    this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();
  }

  divisionChange(division: string) {
    this.refresh$.next({ division, group: undefined, day: undefined, page: undefined } as FilterPageParams);
    division ? this.formGroup.controls.group.enable() : this.formGroup.controls.group.disable();
    this.formGroup.controls.day.disable();
  }

  groupChange(group: string) {
    this.refresh$.next({ group, day: undefined, page: undefined } as FilterPageParams);
    group ? this.formGroup.controls.day.enable() : this.formGroup.controls.day.disable();
  }

  dayChange(day: string) {
    this.refresh$.next({ day, page: undefined } as FilterPageParams);
  }

  weekChange(week: Week) {
    this.refresh$.next({
      startDate: week && week.start.clone().utc().format(),
      endDate: week && week.end.clone().utc().format(),
      page: undefined } as FilterPageParams);
  }

  protected delete$(item: Plateau): Observable<void> {
    return this.plateau.delete(this.currentSeason.get().slug, item.slug);
  }

  protected find$(params: PageParams): Observable<Page<Plateau>> {
    return combineLatest([
      this.currentSeason.get$().pipe(take(1)),
      this.route.queryParams.pipe(take(1))
    ])
      .pipe(
        map(([season, { category, competition, division, group, day, startDate, endDate }]) => ({
          season, category, competition, division, group, day,
          startDate: startDate && moment(startDate).isValid() ? moment(startDate).utc().format() : undefined,
          endDate: endDate && moment(endDate).isValid() ? moment(endDate).utc().format() : undefined
        })),
        map(({ season, ...filterParams }) => Object.entries(filterParams)
          .filter(([, value]) => !!value)
          .reduce((acc, [key, value]) => ({ ...acc, [key]: value}), { season })),
        distinctUntilChanged(isEqual),
        mergeMap(({ season, ...filterParams }) => this.plateau.find(season.slug, { ...filterPageParams(params), ...filterParams })));
  }

  protected getItemLabel(item: Plateau): string {
    return [
      item.date.format('DD/MM/YYYY HH:mm'),
      item.location,
      item.categoryName,
      item.competitionName
    ].join(' - ');
  }

  protected getItemSlug(item: Plateau): string {
    return item.slug;
  }

  compareByWeek(w1: Week, w2: Week): boolean {
    return !w1 && !w2 || w1?.start?.isSame(w2?.start, 'second') && w1?.end?.isSame(w2?.end, 'second');
  }

}
