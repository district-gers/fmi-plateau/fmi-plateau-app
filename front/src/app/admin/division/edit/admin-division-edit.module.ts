import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { AsyncSelectModule } from '@components/async-select/async-select.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';

import { AdminDivisionEditRoutingModule } from './admin-division-edit-routing.module';
import { AdminDivisionEditComponent } from './admin-division-edit.component';


@NgModule({
  declarations: [AdminDivisionEditComponent],
  imports: [
    CommonModule,
    AdminDivisionEditRoutingModule,
    PageFormModule,
    TranslateModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    AsyncButtonModule,
    AsyncSelectModule
  ]
})
export class AdminDivisionEditModule { }
