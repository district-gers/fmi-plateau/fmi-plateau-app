import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
  {
    path: '',
    loadChildren: () => import('./list/admin-group-list.module').then(module => module.AdminGroupListModule)
  },
  {
    path: 'create',
    loadChildren: () => import('./edit/admin-group-edit.module').then(module => module.AdminGroupEditModule)
  },
  {
    path: ':slug/update',
    loadChildren: () => import('./edit/admin-group-edit.module').then(module => module.AdminGroupEditModule)
  },
  {
    path: ':slug/duplicate',
    loadChildren: () => import('./edit/admin-group-edit.module').then(module => module.AdminGroupEditModule)
  }
]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminGroupRoutingModule { }
