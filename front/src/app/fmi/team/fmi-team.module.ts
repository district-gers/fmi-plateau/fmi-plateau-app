import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';
import { UserHasRightModule } from '@pipes/user-has-right/user-has-right.module';
import { UserIsInChargeOfPlateauModule } from '@pipes/user-is-in-charge-of-plateau/user-is-in-charge-of-plateau.module';
import { Angulartics2OnModule } from 'angulartics2';

import { FmiTeamRoutingModule } from './fmi-team-routing.module';
import { FmiTeamComponent } from './fmi-team.component';
import { FmiTeamItemModule } from './item/fmi-team-item.module';
import { FmiTeamMemberModule } from './member/fmi-team-member.module';

@NgModule({
  declarations: [FmiTeamComponent],
  imports: [
    CommonModule,
    FmiTeamRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    UserHasRightModule,
    PageFormModule,
    UserIsInChargeOfPlateauModule,
    Angulartics2OnModule,
    FmiTeamMemberModule,
    AsyncButtonModule,
    FmiTeamItemModule
  ]
})
export class FmiTeamModule { }
