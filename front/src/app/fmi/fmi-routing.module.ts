import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: ':fmiSlug',
    children: [
      {
        path: 'matchs',
        loadChildren: () => import('./match/fmi-match.module').then(module => module.FmiMatchModule)
      },
      {
        path: 'team',
        loadChildren: () => import('./team/fmi-team.module').then(module => module.FmiTeamModule)
      },
      {
        path: 'score',
        loadChildren: () => import('./score/fmi-score.module').then(module => module.FmiScoreModule)
      },
      {
        path: 'read',
        loadChildren: () => import('./read/fmi-read.module').then(module => module.FmiReadModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FmiRoutingModule { }
