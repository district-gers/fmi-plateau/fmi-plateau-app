import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

export enum RefreshEvent {
  SEASON,
  USER,
}

@Injectable({ providedIn: 'root' })
export class RefreshEventService {

  private events$ = new Subject<{ type: RefreshEvent; data?: any; }>();

  pushEvent(type: RefreshEvent, data?: any) {
    this.events$.next({ type, data });
  }

  getEvents$(...types: RefreshEvent[]) {
    return this.events$.pipe(filter((event) => types?.includes(event.type)));
  }

}
