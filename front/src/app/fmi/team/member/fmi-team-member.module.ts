import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateHasKeyModule } from '@common/translate/has-key/translate-has-key.module';
import { TranslateModule } from '@ngx-translate/core';
import { LicenseNumberModule } from '@pipes/license-number/license-number.module';
import { Angulartics2OnModule } from 'angulartics2';

import { FmiTeamMemberComponent } from './fmi-team-member.component';



@NgModule({
  declarations: [FmiTeamMemberComponent],
  exports: [
    FmiTeamMemberComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatSidenavModule,
    TranslateModule,
    MatAutocompleteModule,
    TranslateHasKeyModule,
    MatFormFieldModule,
    Angulartics2OnModule,
    LicenseNumberModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatTooltipModule
  ]
})
export class FmiTeamMemberModule { }
