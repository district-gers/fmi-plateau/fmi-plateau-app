export interface ParticipantClub {
  slug?: string;
  name?: string;
  shortName?: string;
  logoUrl?: string;
}

export interface ParticipantDto {
  slug?: string;
  name: string;
  shortName: string;
  logoUrl?: string;
  clubs?: ParticipantClub[];
  clubSlugs: string[];
  categorySlug: string;
  categoryName?: string;
}

export interface Participant {
  slug?: string;
  name: string;
  shortName: string;
  logoUrl: string;
  clubs: ParticipantClub[];
  category: string;
  categoryName?: string;
}
