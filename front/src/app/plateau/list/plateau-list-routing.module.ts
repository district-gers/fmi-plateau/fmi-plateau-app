import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PlateauListComponent } from './plateau-list.component';


const routes: Routes = [
  {
    path: '',
    component: PlateauListComponent,
    data: {
      meta: {
        title: 'plateau.list.seo.title',
        description: 'plateau.list.seo.description',
        'twitter:title': 'plateau.list.seo.title',
        'twitter:description': 'plateau.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlateauListRoutingModule { }
