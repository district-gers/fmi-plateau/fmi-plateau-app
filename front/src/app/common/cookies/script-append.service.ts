import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ScriptAppendService {

  private isAppended = {
    analytics: false
  };

  constructor(@Inject(DOCUMENT) private doc: any) {
  }

  appendAnalytics() {
    if (this.isAppended.analytics) {
      return;
    }
    const analyticsId = environment.analytics.id;
    const scriptElement = this.doc.createElement('script');
    scriptElement.type = 'text/javascript';
    scriptElement.innerHTML = `(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', '${analyticsId}', 'auto');`;
    const headElement = this.doc.getElementsByTagName('head')[0];
    headElement.appendChild(scriptElement);
    this.isAppended.analytics = true;
  }

}
