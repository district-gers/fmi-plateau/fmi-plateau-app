import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminParticipantEditComponent } from './admin-participant-edit.component';

const routes: Routes = [
  {
    path: '',
    component: AdminParticipantEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminParticipantEditRoutingModule { }
