import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { CurrentUserService } from '@common/current-user.service';
import { Loading } from '@common/loading';
import { emptyPage } from '@common/pagination/pagination';
import { Fmi, PlayerSquad, TeamSquad } from '@common/resources/admin/fmi/fmi.model';
import { Plateau } from '@common/resources/admin/plateau/plateau.model';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { Auth } from '@resources/admin/auth/auth.model';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Educator } from '@resources/admin/educator/educator.model';
import { EducatorService } from '@resources/admin/educator/educator.service';
import { FmiService } from '@resources/admin/fmi/fmi.service';
import { PlateauService } from '@resources/admin/plateau/plateau.service';
import { Right } from '@resources/user/right';
import { userHasRights, userIsInChargeOfPlateau, userIsInChargeOfTeam } from '@utils/user-right.util';
import { BehaviorSubject, combineLatest, ConnectableObservable, Observable, of, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  finalize,
  map,
  mergeMap,
  publishBehavior,
  skip,
  switchMap,
  take,
  takeUntil,
  tap
} from 'rxjs/operators';

import { FmiTeamMemberConfig } from './member/fmi-team-member.config';

@Component({
  selector: 'app-fmi-team',
  templateUrl: './fmi-team.component.html',
  styleUrls: [
    './fmi-team.component.scss',
    './fmi-team.component.mobile.scss'
  ]
})
export class FmiTeamComponent implements OnInit, OnDestroy {

  user$: Observable<Auth>;
  plateau$: ConnectableObservable<Plateau>;
  fmi$: ConnectableObservable<Fmi>;
  educators$ = new BehaviorSubject<Educator[]>([]);
  loading: Loading = { save: false };
  formGroup: FormGroup;
  previousRouteCommands: Link;
  rightEnum = Right;

  private destroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private routerExt: RouterExtService,
    private currentSeason: CurrentSeasonService,
    private currentUser: CurrentUserService,
    private fmi: FmiService,
    private educator: EducatorService,
    private plateau: PlateauService,
    private category: CategoryService) {
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      fmi: [],
      inChargeMember: this.fb.group({ config: [] }),
      doctor: [],
      teams: this.fb.array([])
    });

    this.previousRouteCommands = this.routerExt.getPreviousUrl() || { commands: ['/plateaux'] };
    this.user$ = this.currentUser.get$();
    const season$ = this.currentSeason.get$().pipe(take(1));

    this.plateau$ = combineLatest([season$, this.route.params, this.user$])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, { plateauSlug }]) => !!plateauSlug),
        map(([season, { plateauSlug }]) => ({ season, plateauSlug })),
        distinctUntilChanged(),
        switchMap(({ season, plateauSlug }) => this.plateau.get(season.slug, plateauSlug)),
        publishBehavior<Plateau>(null)) as ConnectableObservable<Plateau>;
    this.plateau$.connect();

    combineLatest([
      season$,
      this.plateau$.pipe(filter(plateau => !!plateau))
    ])
      .pipe(
        takeUntil(this.destroy$),
        switchMap(([season, plateau]) => combineLatest([
          of(season),
          of(plateau),
          this.user$.pipe(take(1))
        ])),
        map(([season, plateau, user]) => ({ season, plateau, user })),
        switchMap(({ season, plateau, user }) => userHasRights(user, Right.FMI_EDIT)
        || userHasRights(user, Right.FMI_PREPARE) && userIsInChargeOfPlateau(user, plateau)
          ? this.educator.findAllByParticipant(season.slug, plateau.participant)
          : of(emptyPage)),
        map(({ items }) => items))
      .subscribe(x => this.educators$.next(x));

    this.fmi$ = combineLatest([season$, this.route.params, this.user$])
      .pipe(
        takeUntil(this.destroy$),
        filter(([, { fmiSlug }]) => !!fmiSlug),
        map(([season, { fmiSlug }]) => ({ season, fmiSlug })),
        distinctUntilChanged(),
        switchMap(({ season, fmiSlug }) => this.fmi.get(season.slug, fmiSlug)),
        publishBehavior(null)) as ConnectableObservable<Fmi>;
    this.fmi$.connect();

    this.fmi$
      .pipe(
        filter(fmi => !!fmi),
        switchMap(fmi => combineLatest([
          of(fmi),
          this.user$.pipe(take(1)),
          this.category.get(this.currentSeason.get().slug, fmi.category)
        ])))
      .subscribe(([fmi, user, category]: [Fmi, Auth, Category]) => {
        this.formGroup.reset();
        (this.formGroup.controls.teams as FormArray).clear();

        const config: FmiTeamMemberConfig = {
          member: fmi.inChargeMember,
          members$: this.educators$,
          analyticsLabel: 'inChargeMember',
          labelKeys: {
            member: 'fmi.team.form.member',
            firstName: 'fmi.team.form.firstName',
            lastName: 'fmi.team.form.lastName',
            code: 'fmi.team.form.code'
          }
        };
        this.formGroup.patchValue({
          fmi,
          inChargeMember: { config },
          doctor: fmi.doctor
        });

        fmi.teams
          ?.filter(team => userHasRights(user, Right.FMI_EDIT) || userHasRights(user, Right.FMI_TEAM) && userIsInChargeOfTeam(user, team))
          ?.forEach(team => {
            const teamFormGroup = this.fb.group({
              team: [team],
              fmi: [fmi],
              participantTeamFormGroups: [],
              category: [category],
              educator: this.fb.group({ config: [] }),
              players: this.fb.array([]),
              unavailablePlayers: []
            });
            (this.formGroup.controls.teams as FormArray).push(teamFormGroup);
          });
        (this.formGroup.controls.teams as FormArray).controls
          .forEach(teamFormGroup => teamFormGroup
            .patchValue({
              participantTeamFormGroups: (this.formGroup.controls.teams as FormArray).controls
                .filter(participantTeamFormGroup => participantTeamFormGroup !== teamFormGroup.value)
                .filter(participantTeamFormGroup =>
                  (participantTeamFormGroup.value.team as TeamSquad).participant === (teamFormGroup.value.team as TeamSquad).participant)
            }));

        if (!this.previousRouteCommands.queryParams) {
          this.previousRouteCommands.queryParams = {
            category: fmi.category,
            competition: fmi.competition,
            division: fmi.division,
            group: fmi.group,
            day: fmi.day
          };
        }
      });

    this.route.queryParams
      .pipe(
        map(({ season }) => season),
        distinctUntilChanged(),
        skip(1))
      .subscribe(() => this.router
        .navigate(['/plateaux'], { queryParamsHandling: 'merge', replaceUrl: true, relativeTo: this.route }));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  save() {
    const value = this.formGroup.getRawValue();
    const inChargeMember = value.inChargeMember;
    this.currentSeason.get$()
      .pipe(
        tap(() => this.loading.save = true),
        take(1),
        mergeMap(season => this.fmi.save(season.slug, {
          slug: value.fmi.slug,
          ...(inChargeMember?.auto?.member || inChargeMember?.manual?.lastName && inChargeMember?.manual?.firstName) && {
            inChargeMember: {
              ...inChargeMember.auto.member && {
                slug: inChargeMember.auto.member.slug
              },
              ...!inChargeMember.auto.member && {
                firstName: inChargeMember.manual.firstName,
                lastName: inChargeMember.manual.lastName,
                code: inChargeMember.manual.code
              }
            }
          },
          ...value.doctor && {
            doctor: value.doctor
          },
          teams: value.teams?.map((team): TeamSquad => ({
            slug: team.team.slug,
            ...(team.educator?.auto?.member || team.educator?.manual?.lastName && team.educator?.manual?.firstName) && {
              educator: {
                ...team.educator.auto.member && {
                  slug: team.educator.auto.member.slug
                },
                ...!team.educator.auto.member && {
                  firstName: team.educator.manual.firstName,
                  lastName: team.educator.manual.lastName,
                  code: team.educator.manual.code
                }
              },
            },
            players: team.players
              ?.filter(player => player?.auto?.member || player?.manual?.lastName && player?.manual?.firstName)
              ?.map((player): PlayerSquad => ({
                ...player.auto.member && {
                  slug: player.auto.member.slug
                },
                ...!player.auto.member && {
                  firstName: player.manual.firstName,
                  lastName: player.manual.lastName,
                  code: player.manual.code,
                  category: player.manual.category
                }
              }))
          }))
        })),
        finalize(() => this.loading.save = false))
      .subscribe(() => this.router.navigate(this.previousRouteCommands.commands, { queryParams: this.previousRouteCommands.queryParams }));
  }

  trackByTeamValue(index, formGroup: FormGroup) {
    return formGroup.value.team?.slug;
  }

  controlAsFormGroups(control: AbstractControl): FormGroup[] {
    return (control as FormArray).controls as FormGroup[];
  }

  controlAsFormGroup(control: AbstractControl): FormGroup {
    return control as FormGroup;
  }

  enableSave(fmi: Fmi): boolean {
    const user = this.currentUser.get();
    if (user && userHasRights(user, Right.FMI_EDIT)) {
      return true;
    }
    return fmi?.teams?.every(team => !team.signature?.pictureUrl);
  }

}
