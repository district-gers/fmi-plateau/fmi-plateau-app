import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { LayoutConfig } from './layout.config';

@Injectable({ providedIn: 'root' })
export class LayoutService {

  private config$ = new BehaviorSubject<LayoutConfig>({});

  getConfig(): Observable<LayoutConfig> {
    return this.config$.asObservable();
  }

  setConfig(config: LayoutConfig) {
    this.config$.next({ ...this.config$.getValue(), ...config });
  }
}
