import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { TranslateModule } from '@ngx-translate/core';

import { SeasonSelectorComponent } from './season-selector.component';



@NgModule({
  declarations: [SeasonSelectorComponent],
  exports: [
    SeasonSelectorComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
    TranslateModule,
    ReactiveFormsModule
  ]
})
export class SeasonSelectorModule { }
