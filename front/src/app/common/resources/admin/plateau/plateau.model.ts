import { Moment } from 'moment';

interface PlateauTeamDto {
  slug?: string;
  index?: number;
  participantSlug?: string;
  participantName?: string;
  participantShortName?: string;
  participantLogoUrl?: string;
}

export interface PlateauTeam {
  slug?: string;
  index?: number;
  participant?: string;
  name?: string;
  shortName?: string;
  logoUrl?: string;
}

export interface PlateauDto {
  slug?: string;
  date: string;
  location: string;
  daySlug: string;
  dayIndex?: number;
  groupSlug: string;
  groupIndex?: number;
  groupName?: string;
  divisionSlug: string;
  divisionIndex?: number;
  divisionName?: string;
  competitionSlug: string;
  competitionName?: string;
  categorySlug: string;
  categoryName?: string;
  teams?: PlateauTeamDto[];
  teamSlugs?: string[];
  participantSlug: string;
  participantName?: string;
  participantShortName?: string;
  participantLogoUrl?: string;
  fmiSlug: string;
}

export interface Plateau {
  slug?: string;
  date: Moment;
  location: string;
  day: string;
  group: string;
  dayIndex?: number;
  groupIndex?: number;
  groupName?: string;
  division: string;
  divisionIndex?: number;
  divisionName?: string;
  competition: string;
  competitionName?: string;
  category: string;
  categoryName?: string;
  teams?: PlateauTeam[];
  participant: string;
  participantName?: string;
  participantShortName?: string;
  participantLogoUrl?: string;
  fmiSlug?: string;
}
