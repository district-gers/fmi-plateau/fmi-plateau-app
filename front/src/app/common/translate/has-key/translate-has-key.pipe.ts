import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'translateHasKey',
  pure: false
})
export class TranslateHasKeyPipe implements PipeTransform {

  constructor(private translate: TranslateService) {
  }

  transform(value: string): any {
    return value && this.translate.instant(value) !== value;
  }

}
