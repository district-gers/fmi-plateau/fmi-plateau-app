import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { AsyncSelectModule } from '@components/async-select/async-select.module';
import { DialogConfirmationModule } from '@components/dialog-confirmation/dialog-confirmation.module';
import { DialogUploadModule } from '@components/dialog-upload/dialog-upload.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { ResetButtonIconModule } from '@components/reset-button-icon/reset-button-icon.module';
import { TranslateModule } from '@ngx-translate/core';
import { CategoryLabelModule } from '@pipes/category-label/category-label.module';
import { LicenseNumberModule } from '@pipes/license-number/license-number.module';
import { TeamNameModule } from '@pipes/team-name/team-name.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { AdminPlayerListRoutingModule } from './admin-player-list-routing.module';
import { AdminPlayerListComponent } from './admin-player-list.component';


@NgModule({
  declarations: [AdminPlayerListComponent],
  imports: [
    CommonModule,
    AdminPlayerListRoutingModule,
    PageFormModule,
    TranslateModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    MatPaginatorModule,
    CategoryLabelModule,
    DialogConfirmationModule,
    LicenseNumberModule,
    LazyLoadImageModule,
    DialogUploadModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    AsyncSelectModule,
    MatSelectModule,
    TeamNameModule,
    ResetButtonIconModule,
    AsyncButtonModule
  ]
})
export class AdminPlayerListModule { }
