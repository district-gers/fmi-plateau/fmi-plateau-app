import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../../../../environments/environment';

const resourceUrl = `${environment.cloudFunction.data.url}/pictures`;

@Injectable({ providedIn: 'root' })
export class PictureService {

  constructor(
    private http: HttpClient) {
  }

  find(): Observable<string[]> {
    return this.http.get<string[]>(resourceUrl);
  }
}
