import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UserIsInChargeOfTeamPipe } from './user-is-in-charge-of-team.pipe';



@NgModule({
  declarations: [UserIsInChargeOfTeamPipe],
  imports: [
    CommonModule
  ],
  exports: [UserIsInChargeOfTeamPipe],
  providers: [UserIsInChargeOfTeamPipe]
})
export class UserIsInChargeOfTeamModule { }
