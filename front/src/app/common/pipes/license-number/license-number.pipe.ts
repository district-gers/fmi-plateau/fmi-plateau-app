import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'licenseNumber'
})
export class LicenseNumberPipe implements PipeTransform {

  constructor(
    private decimalPipe: DecimalPipe) {
  }

  transform(value: string | number): string {
    return value && this.decimalPipe.transform(typeof value === 'string' ? value?.replace(/\s/g, '') : value, '6.0-0', 'fr-FR');
  }

}
