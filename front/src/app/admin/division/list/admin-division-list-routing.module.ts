import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminDivisionListComponent } from './admin-division-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminDivisionListComponent,
    data: {
      meta: {
        title: 'admin.division.list.seo.title',
        description: 'admin.division.list.seo.description',
        'twitter:title': 'admin.division.list.seo.title',
        'twitter:description': 'admin.division.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminDivisionListRoutingModule { }
