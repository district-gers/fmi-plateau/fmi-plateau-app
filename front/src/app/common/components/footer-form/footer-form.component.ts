import { Component } from '@angular/core';

@Component({
  selector: 'app-footer-form',
  templateUrl: './footer-form.component.html',
  styleUrls: [
    './footer-form.component.scss',
    './footer-form.component.mobile.scss'
  ]
})
export class FooterFormComponent {
}
