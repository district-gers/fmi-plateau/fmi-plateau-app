import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-player-list.module').then(module => module.AdminPlayerListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-player-edit.module').then(module => module.AdminPlayerEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-player-edit.module').then(module => module.AdminPlayerEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-player-edit.module').then(module => module.AdminPlayerEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPlayerRoutingModule {
}
