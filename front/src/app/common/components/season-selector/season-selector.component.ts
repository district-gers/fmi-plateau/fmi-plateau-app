import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page } from '@common/pagination/pagination';
import { RefreshEvent, RefreshEventService } from '@common/refresh-event.service';
import { Season } from '@resources/admin/season/season.model';
import { SeasonService } from '@resources/admin/season/season.service';
import { combineLatest, ConnectableObservable, Subject } from 'rxjs';
import { distinctUntilChanged, map, publishBehavior, startWith, switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-season-selector',
  templateUrl: './season-selector.component.html',
  styleUrls: ['./season-selector.component.scss']
})
export class SeasonSelectorComponent implements OnInit, OnDestroy {

  formControl = new FormControl();
  seasons$: ConnectableObservable<Page<Season>>;
  private destroy$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private season: SeasonService,
    private currentSeason: CurrentSeasonService,
    private refreshEvent: RefreshEventService) { }

  ngOnInit(): void {
    this.seasons$ = this.refreshEvent.getEvents$(RefreshEvent.SEASON)
      .pipe(
        startWith(null as object),
        switchMap(() => this.season.find({ sort: { field: 'startDate', direction: 'desc' } })),
        publishBehavior(null)) as ConnectableObservable<Page<Season>>;
    this.seasons$.connect();

    this.formControl.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        map(season => season as Season),
        distinctUntilChanged((s1, s2) => s1?.slug === s2?.slug))
      .subscribe(season => this.currentSeason.switch(season));

    combineLatest([
      this.route.queryParams
        .pipe(
          map(({ season }) => season),
          distinctUntilChanged()),
      this.season.getCurrent(),
      this.seasons$,
    ])
      .pipe(map(([seasonSlug, currentSeason, seasons]) => seasons?.items
        .find(season => season.slug === (seasonSlug || currentSeason.slug))))
      .subscribe(season => this.formControl.patchValue(season));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  compareBySlug(i1: { slug: string; }, i2: { slug: string; }) {
    return i1 && i2 && i1?.slug === i2?.slug;
  }

}
