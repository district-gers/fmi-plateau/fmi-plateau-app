import { Pipe, PipeTransform } from '@angular/core';
import { Auth } from '@resources/admin/auth/auth.model';
import { Plateau } from '@resources/admin/plateau/plateau.model';
import { userIsInChargeOfPlateau } from '@utils/user-right.util';

@Pipe({
  name: 'userIsInChargeOfPlateau'
})
export class UserIsInChargeOfPlateauPipe implements PipeTransform {

  transform(user: Auth, plateau: Plateau): boolean {
    return user && plateau && userIsInChargeOfPlateau(user, plateau);
  }

}
