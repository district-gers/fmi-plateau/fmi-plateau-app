import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { LicenseNumberModule } from '@pipes/license-number/license-number.module';
import { TeamNameModule } from '@pipes/team-name/team-name.module';
import { UserIsInChargeOfTeamModule } from '@pipes/user-is-in-charge-of-team/user-is-in-charge-of-team.module';
import { Angulartics2OnModule } from 'angulartics2';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { FmiReadTeamsComponent } from './fmi-read-teams.component';

@NgModule({
  declarations: [FmiReadTeamsComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatExpansionModule,
    LicenseNumberModule,
    TeamNameModule,
    LazyLoadImageModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonModule,
    MatMenuModule,
    Angulartics2OnModule,
    MatCheckboxModule,
    FormsModule,
    UserIsInChargeOfTeamModule
  ],
  exports: [FmiReadTeamsComponent]
})
export class FmiReadTeamsModule { }
