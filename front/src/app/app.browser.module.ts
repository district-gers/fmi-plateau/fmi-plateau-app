import { NgModule } from '@angular/core';
import { BrowserTransferStateModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { CookiesBrowserService } from '@common/cookies/cookies.browser.service';
import { CookiesModule } from '@common/cookies/cookies.module';
import { LocalStorage } from '@common/local-storage/local-storage';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { AppModule } from './app.module';

@NgModule({
  imports: [
    AppModule,
    BrowserTransferStateModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    CookiesModule.forRoot(CookiesBrowserService)
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: LocalStorage,
      useValue: localStorage
    }
  ]
})
export class AppBrowserModule {}
