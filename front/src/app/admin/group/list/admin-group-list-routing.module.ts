import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminGroupListComponent } from './admin-group-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminGroupListComponent,
    data: {
      meta: {
        title: 'admin.group.list.seo.title',
        description: 'admin.group.list.seo.description',
        'twitter:title': 'admin.group.list.seo.title',
        'twitter:description': 'admin.group.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminGroupListRoutingModule { }
