import { HttpErrorResponse, HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InMemoryCacheService } from '@common/in-memory-cache.service';
import { Observable, of, throwError } from 'rxjs';
import { catchError, filter, finalize, mergeMap, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

const getMemoryCacheKey = (request: HttpRequest<any>) => {
  return request.urlWithParams;
};

@Injectable()
export class MicroCacheInterceptor implements HttpInterceptor {

  constructor(private inMemoryCacheService: InMemoryCacheService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.method !== 'GET' || !request.url.startsWith(environment.cloudFunction.data.url)) {
      return next.handle(request);
    }
    const memoryCacheKey = getMemoryCacheKey(request);
    const cachedObservable = this.inMemoryCacheService.get(memoryCacheKey);

    let lastEventType;
    const handleObservable = next.handle(request)
      .pipe(
        filter((response: HttpEvent<any>) => response.type === HttpEventType.Response),
        tap((response: HttpEvent<any>) => {
          lastEventType = response.type;
          this.inMemoryCacheService.put(memoryCacheKey, response);
        }),
        catchError((error: HttpErrorResponse) => {
          lastEventType = HttpEventType.Response;
          this.inMemoryCacheService.putError(memoryCacheKey, error);
          return throwError(error);
        }),
        finalize(() => {
          if (lastEventType !== HttpEventType.Response) {
            this.inMemoryCacheService.delete(memoryCacheKey);
          }
        }));
    return cachedObservable && cachedObservable
      .pipe(
        mergeMap((response: HttpEvent<any>) => {
          if (response.type === -1) {
            return this.intercept(request, next);
          } else {
            return of(response);
          }
        })
      ) || handleObservable;
  }
}
