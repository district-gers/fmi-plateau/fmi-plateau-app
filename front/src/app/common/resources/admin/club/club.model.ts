export interface ClubDto {
  slug?: string;
  name: string;
  shortName: string;
  code: number;
  city: string;
  logoUrl: string;
}

export interface Club {
  slug?: string;
  name: string;
  shortName: string;
  code: number;
  city: string;
  logoUrl: string;
}
