import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-reset-button-icon',
  templateUrl: './reset-button-icon.component.html',
  styleUrls: ['./reset-button-icon.component.scss']
})
export class ResetButtonIconComponent {

  @Input() display: boolean;
  @Output() resetClick = new EventEmitter<MouseEvent>();

}
