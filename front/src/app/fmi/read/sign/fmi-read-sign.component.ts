import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Fmi, TeamSquad } from '@common/resources/admin/fmi/fmi.model';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { Auth } from '@resources/admin/auth/auth.model';
import { userIsInChargeOfTeam } from '@utils/user-right.util';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-fmi-read-sign',
  templateUrl: './fmi-read-sign.component.html',
  styleUrls: ['./fmi-read-sign.component.scss']
})
export class FmiReadSignComponent implements OnInit, OnChanges, OnDestroy {

  @Input() fmi: Fmi;
  @Input() user: Auth;
  @Input() formGroup: FormGroup;
  screenSize$: Observable<ScreenSize>;
  userTeams: TeamSquad[];

  private destroy$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private responsive: ResponsiveService) { }

  ngOnInit(): void {
    this.screenSize$ = this.responsive.size$;
  }

  ngOnChanges(changes: SimpleChanges): void {
    const fmiChange = changes.fmi;
    const userChange = changes.user;
    if (fmiChange && fmiChange.previousValue !== fmiChange.currentValue
      || userChange && userChange.previousValue !== userChange.currentValue) {
      Object.keys(this.formGroup.controls)
        .forEach((key: string) => this.formGroup.removeControl(key));
      this.userTeams = this.fmi?.teams?.filter(team => userIsInChargeOfTeam(this.user, team));
      this.userTeams?.forEach(team => {
        const teamFormGroup = this.fb.group({
          team: [team],
          confirmed: [false, Validators.required],
          comment: [],
          pictureUrl: []
        });
        this.formGroup.addControl(team.slug, teamFormGroup);
        teamFormGroup.controls.pictureUrl.disable();
        if (team?.signature) {
          teamFormGroup.patchValue({
            confirmed: !!team?.signature.pictureUrl,
            comment: team?.signature.comment,
            pictureUrl: team?.signature.pictureUrl
          });
          if (teamFormGroup.value.confirmed) {
            teamFormGroup.disable();
          }
        }

        teamFormGroup.controls.confirmed.valueChanges
          .pipe(takeUntil(this.destroy$))
          .subscribe(confirmed => {
            const pictureUrlControl = teamFormGroup.controls.pictureUrl;
            if (confirmed) {
              pictureUrlControl.enable();
            } else {
              pictureUrlControl.reset();
              pictureUrlControl.disable();
            }
          });
      });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  trackBySlug(index, item): string {
    return item.slug;
  }

  controlAsFormGroup(control: AbstractControl): FormGroup {
    return control as FormGroup;
  }
}
