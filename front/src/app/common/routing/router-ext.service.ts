import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Link } from '@common/routing/link';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, filter, map, pairwise } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class RouterExtService {

  private previousUrl$ = new BehaviorSubject<Link>(null);

  constructor(private router: Router) {
    router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.router.url),
        filter(url => !url.startsWith('/error')),
        distinctUntilChanged(),
        pairwise(),
        map(([previousUrl]: string[]) => previousUrl),
        map((previousUrl: string): string[] => previousUrl.split('?')),
        map(([path, queryParams]): Link => ({
          commands: [path],
          queryParams: queryParams?.split('&')
            .map(param => param.split('='))
            .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {})
        })))
      .subscribe((previousLink: Link) => this.previousUrl$.next(previousLink));
  }

  getPreviousUrl(): Link {
    return this.previousUrl$.getValue();
  }
}
