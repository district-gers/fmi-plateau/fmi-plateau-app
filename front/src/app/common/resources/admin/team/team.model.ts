export interface TeamDto {
  slug?: string;
  index: number;
  participantSlug?: string;
  participantName?: string;
  participantShortName?: string;
  participantLogoUrl?: string;
  categorySlug?: string;
  categoryName?: string;
}

export interface Team {
  slug?: string;
  index: number;
  participant: string;
  name?: string;
  shortName?: string;
  logoUrl?: string;
  category?: string;
  categoryName?: string;
}
