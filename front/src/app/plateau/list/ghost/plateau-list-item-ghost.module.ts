import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';

import { PlateauListItemGhostComponent } from './plateau-list-item-ghost.component';



@NgModule({
  declarations: [PlateauListItemGhostComponent],
  exports: [
    PlateauListItemGhostComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatChipsModule,
    MatButtonModule
  ]
})
export class PlateauListItemGhostModule { }
