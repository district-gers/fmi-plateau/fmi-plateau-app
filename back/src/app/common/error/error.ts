export class FmiPlateauErrorType {
  static readonly NOT_FOUND = new FmiPlateauErrorType('NOT_FOUND', 404, 'The requested resource has not been found');
  static readonly NOT_AUTHENTICATED = new FmiPlateauErrorType('NOT_AUTHENTICATED', 401, 'The requested resource requires authentication');
  static readonly NOT_ALLOWED = new FmiPlateauErrorType('NOT_ALLOWED', 403, 'The requested resource requires specific granted rights');
  static readonly INTERNAL_ERROR = new FmiPlateauErrorType('INTERNAL_ERROR', 500, 'An error has occurred while processing the request');
  static readonly REMOTE_API_ERROR = new FmiPlateauErrorType('REMOTE_API_ERROR', 502, 'Remote API has sent a blocking error');

  constructor(public code: string,
              public httpCode: number,
              public description: string) {
  }
}

export class FmiPlateauError extends Error {

  constructor(public type: FmiPlateauErrorType,
              message?: string,
              public details?: any) {
    super(message || type && type.description);
  }

}
