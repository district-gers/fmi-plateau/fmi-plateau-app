import { Season, SeasonDto } from '@resources/admin/season/season.model';
import * as moment from 'moment';

class SeasonMapper {

  toModel(dto: SeasonDto): Season {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      name: dto.name,
      startDate: dto.startDate && moment(dto.startDate),
      endDate: dto.endDate && moment(dto.endDate),
      isCurrent: dto.isCurrent
    };
  }

  toDto(model: Season): SeasonDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('name') && {
        name: model.name
      },
      ...model.hasOwnProperty('startDate') && {
        startDate: model.startDate?.format()
      },
      ...model.hasOwnProperty('endDate') && {
        endDate: model.endDate?.format()
      },
      ...model.hasOwnProperty('isCurrent') && {
        isCurrent: model.isCurrent
      }
    };
  }

}

export const seasonMapper = new SeasonMapper();
