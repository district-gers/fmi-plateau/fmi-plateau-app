import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page } from '@common/pagination/pagination';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { DivisionService } from '@resources/admin/division/division.service';
import { tapUniqueItem } from '@utils/rxjs.util';
import { combineLatest, Observable } from 'rxjs';
import { finalize, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

const COUNT_MAX_OFFSET = 10;

@Component({
  selector: 'app-admin-competition-edit',
  templateUrl: './admin-competition-edit.component.html',
  styleUrls: ['./admin-competition-edit.component.scss']
})
export class AdminCompetitionEditComponent extends AbstractAdminEditComponent<Competition> implements OnInit {

  formGroup: FormGroup;
  logoFormGroup: FormGroup;
  metaLabelsPrefix = 'admin.competition.edit.seo';
  minDivisionCount = 0;
  maxDivisionCount = COUNT_MAX_OFFSET;

  categories$: Observable<Page<Category>>;
  screenSize$: Observable<ScreenSize>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private responsive: ResponsiveService,
    private competition: CompetitionService,
    private division: DivisionService,
    private category: CategoryService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;

    this.formGroup = this.fb.group({
      slug: [],
      name: ['', Validators.required],
      divisionCount: [1, Validators.max(COUNT_MAX_OFFSET)],
      category: [undefined, Validators.required]
    });
    this.logoFormGroup = this.formGroup.controls.logo as FormGroup;
    if (this.mode === EditMode.UPDATE) {
      this.formGroup.controls.category.disable();
    }

    const season$ = this.currentSeason.get$().pipe(take(1));

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)),
        tapUniqueItem(({ slug: category }) => category !== this.formGroup.getRawValue().category
          && this.formGroup.patchValue({ category })),
        finalize(() => this.loading.category = false));

    this.route.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe(({ category }) => this.formGroup.patchValue({ category }));

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => season$.pipe(
          mergeMap(season => combineLatest([
            this.competition.get(season.slug, slug),
            this.division.find(season.slug, slug)
          ])))))
      .subscribe(([competition, divisions]) => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? competition.slug : undefined,
          name: competition.name,
          category: competition.category,
          divisionCount: divisions.count
        });
        this.formGroup.controls.divisionCount.setValidators([
          Validators.min(divisions.count),
          Validators.max(divisions.count + COUNT_MAX_OFFSET)
        ]);
        this.minDivisionCount = divisions.count;
        this.maxDivisionCount = divisions.count + COUNT_MAX_OFFSET;

        const { category } = competition;
        this.previousRouteCommands = this.getPreviousRouteCommands({ category });
      });
  }

  protected save$(): Observable<[Competition, Link]> {
    const value = this.formGroup.getRawValue();
    const { category, divisionCount } = this.formGroup.getRawValue();
    return this.competition.save(this.currentSeason.get().slug, {
      slug: value.slug,
      name: value.name,
      category: value.category
    }, { divisionCount })
      .pipe(
        tap(() => this.previousRouteCommands = this.getPreviousRouteCommands({ category })),
        map(competition => [competition, {
          commands: ['divisions'],
          queryParams: { category, competition: competition.slug },
          label: this.translate.instant('admin.competition.edit.action.seeDivisions')
        }]));
  }

}
