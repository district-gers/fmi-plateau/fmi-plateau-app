import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminCategoryRoutingModule } from './admin-category-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminCategoryRoutingModule
  ]
})
export class AdminCategoryModule { }
