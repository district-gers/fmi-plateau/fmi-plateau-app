import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DialogConfirmationModule } from '@components/dialog-confirmation/dialog-confirmation.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';
import { CategoryLabelModule } from '@pipes/category-label/category-label.module';

import { AdminCategoryListRoutingModule } from './admin-category-list-routing.module';
import { AdminCategoryListComponent } from './admin-category-list.component';


@NgModule({
  declarations: [AdminCategoryListComponent],
  imports: [
    CommonModule,
    AdminCategoryListRoutingModule,
    TranslateModule,
    MatTableModule,
    MatSortModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonModule,
    MatPaginatorModule,
    PageFormModule,
    CategoryLabelModule,
    MatChipsModule,
    DialogConfirmationModule
  ]
})
export class AdminCategoryListModule { }
