import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  Input,
  OnChanges,
  Renderer2, SimpleChanges,
  ViewChild,
} from '@angular/core';
import { MatButton } from '@angular/material/button';

@Component({
  selector: 'app-async-button',
  templateUrl: './async-button.component.html',
  styleUrls: ['./async-button.component.scss']
})
export class AsyncButtonComponent implements AfterViewInit, OnChanges {

  @ViewChild('spinnerRef', { static: true, read: ElementRef }) spinnerElement: ElementRef;
  @ContentChild(MatButton, { static: true, read: ElementRef }) matButtonElement: ElementRef;
  @ContentChild(MatButton, { static: true }) matButton: MatButton;

  @Input() loading: boolean;

  constructor(private r2: Renderer2) {
  }

  ngAfterViewInit() {
    this.r2.appendChild(this.matButtonElement.nativeElement, this.spinnerElement.nativeElement);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.loading) {
      this.matButton.disabled = !!this.loading;
    }
  }

}
