import { omitBy } from 'lodash';
import { isMoment, Moment } from 'moment';

export const difference = <T extends object>(a: T, b: T) => omitBy<T>(a, (v, k) => {
  switch (true) {
    case isMoment(v):
      return (v as unknown as Moment).isSame(b?.[k]);
    case typeof v === 'object':
      return !Object.keys(difference(v, b?.[k]) || {}).length;
    default:
      return v === b?.[k];
  }
});
