import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AsyncSelectModule } from '@components/async-select/async-select.module';
import { ResetButtonIconModule } from '@components/reset-button-icon/reset-button-icon.module';
import { TranslateModule } from '@ngx-translate/core';

import { PlateauListItemGhostModule } from './ghost/plateau-list-item-ghost.module';
import { PlateauListItemModule } from './item/plateau-list-item.module';
import { PlateauListRoutingModule } from './plateau-list-routing.module';
import { PlateauListComponent } from './plateau-list.component';

@NgModule({
  declarations: [PlateauListComponent],
  imports: [
    CommonModule,
    PlateauListRoutingModule,
    MatButtonModule,
    TranslateModule,
    MatExpansionModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    AsyncSelectModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,
    PlateauListItemModule,
    PlateauListItemGhostModule,
    ResetButtonIconModule
  ]
})
export class PlateauListModule { }
