import { Participant, ParticipantDto } from '@resources/admin/participant/participant.model';

class ParticipantMapper {

  toModel(dto: ParticipantDto): Participant {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      name: dto.name,
      shortName: dto.shortName,
      logoUrl: dto.logoUrl,
      clubs: (dto.clubs || [])
        .sort((c1, c2) => c1.name?.toLocaleLowerCase()?.localeCompare(c2.name?.toLocaleLowerCase())),
      category: dto.categorySlug,
      categoryName: dto.categoryName
    };
  }

  toDto(model: Participant): ParticipantDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('name') && {
        name: model.name
      },
      ...model.hasOwnProperty('shortName') && {
        shortName: model.shortName
      },
      ...model.hasOwnProperty('logoUrl') && {
        logoUrl: model.logoUrl
      },
      ...model.hasOwnProperty('clubs') && {
        clubSlugs: model.clubs.map(({ slug }) => slug)
      },
      ...model.hasOwnProperty('category') && {
        categorySlug: model.category
      }
    };
  }

}

export const participantMapper = new ParticipantMapper();
