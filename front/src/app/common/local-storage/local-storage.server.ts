export const inMemoryLocalStorage: Storage = {

  _storage: new Map<string, string>(),
  length: 0,

  key(index: number): string | null {
    return [...this._storage.keys()][index];
  },

  removeItem(key: string): void {
    this._storage.delete(key);
    this.length = this._storage.size;
  },

  setItem(key: string, value: string): void {
    this._storage.set(key, value);
    this.length = this._storage.size;
  },

  clear(): void {
    this._storage.clear();
    this.length = this._storage.size;
  },

  getItem(key: string): string | null {
    return this._storage.get(key);
  }
};
