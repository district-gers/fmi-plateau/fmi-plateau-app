import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Optional,
  Output,
  Self,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, NgControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import { SignaturePad } from 'angular2-signaturepad';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

export class SignatureControlChange {
  constructor(
    public source: SignatureControlComponent,
    public value: string) {
  }
}

@Component({
  selector: 'app-signature-control',
  templateUrl: './signature-control.component.html',
  styleUrls: ['./signature-control.component.scss'],
  providers: [{ provide: MatFormFieldControl, useExisting: SignatureControlComponent }],
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    '[class.example-floating]': 'shouldLabelFloat',
    '[id]': 'id',
    '[attr.aria-describedby]': 'describedBy',
  }
})
export class SignatureControlComponent implements AfterViewInit, OnDestroy, ControlValueAccessor, MatFormFieldControl<string> {
  static nextId = 0;

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  signaturePadOptions = {
    canvasWidth: 300,
    canvasHeight: 300,
    penColor: '#1c2977'
  };

  readonly autofilled: boolean;
  readonly empty: boolean;
  readonly shouldLabelFloat: boolean;
  readonly stateChanges = new Subject<void>();
  focused = false;
  readonly errorState = false;
  readonly controlType = 'app-signature-control';
  readonly id = `${this.controlType}-${SignatureControlComponent.nextId}`;
  describedBy = '';

  private destroy$ = new Subject<void>();

  private _placeholder: string;
  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  private _required = false;
  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }
  private _disabled = false;

  private _value: string;
  @Input()
  get value(): string | null {
    return this._value;
  }

  set value(url: string | null) {
    if (url) {
      this.signaturePad?.fromDataURL(url);
      this._value = url;
    } else {
      this.signaturePad?.clear();
      this._value = null;
    }
    this.stateChanges.next();
  }

  @Output() readonly signatureChange = new EventEmitter<SignatureControlChange>();

  constructor(
    fb: FormBuilder,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,
    @Optional() @Self() public ngControl: NgControl) {

    _focusMonitor.monitor(_elementRef, true).subscribe(origin => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
  }

  propagateChange = (_: string) => {};
  onTouched = () => {};

  ngAfterViewInit(): void {
    if (this._value) {
      this.signaturePad.fromDataURL(this._value);
    }

    this.signaturePad.onEndEvent
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$))
      .subscribe(() => {
        this._value = this.signaturePad.toDataURL();
        this.stateChanges.next();
        this.propagateChange(this._value);
        this.signatureChange.emit(new SignatureControlChange(this, this._value));
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
  }

  reset() {
    this.value = null;
    this.propagateChange(this._value);
    this.signatureChange.emit(new SignatureControlChange(this, this._value));
  }

  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent) {
  }

  writeValue(url: string): void {
    this.value = url;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
