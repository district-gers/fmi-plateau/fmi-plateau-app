import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UserHasRightPipe } from './user-has-right.pipe';

@NgModule({
  declarations: [
    UserHasRightPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    UserHasRightPipe
  ],
  providers: [
    UserHasRightPipe
  ]
})
export class UserHasRightModule { }
