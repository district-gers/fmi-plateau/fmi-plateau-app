import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'internal',
        loadChildren: () => import('./internal/error-internal.module').then(module => module.ErrorInternalModule)
      },
      {
        path: 'not-found',
        loadChildren: () => import('./not-found/error-not-found.module').then(module => module.ErrorNotFoundModule)
      },
      {
        path: 'server-unreachable',
        loadChildren: () => import('./server-unreachable/error-server-unreachable.module')
          .then(module => module.ErrorServerUnreachableModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorRoutingModule { }
