import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page, PageParams } from '@common/pagination/pagination';
import { mapHttpResponseToPage, mapPageItems, mapPageToHttpParams } from '@common/pagination/pagination.util';
import { participantMapper } from '@resources/admin/participant/participant.mapper';
import { Participant, ParticipantDto } from '@resources/admin/participant/participant.model';
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { environment } from '../../../../../environments/environment';

export interface QueryPageParams extends PageParams {
  category?: string;
}

interface SaveParams {
  teamCount?: number;
}

const toHttpParams = (saveParams: SaveParams): { [key: string]: string; } => ({
  ...saveParams && {
    ...saveParams.teamCount && {
      teamCount: '' + saveParams.teamCount
    }
  }
});

const resourceUrl = (seasonSlug: string) => `${environment.cloudFunction.data.url}/seasons/${seasonSlug}/participants`;

@Injectable({ providedIn: 'root' })
export class ParticipantService {

  constructor(
    private http: HttpClient) {
  }

  find(seasonSlug: string, pageParams?: QueryPageParams): Observable<Page<Participant>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params => this.http.get<ParticipantDto[]>(resourceUrl(seasonSlug), { params, observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => participantMapper.toModel(dto)));
  }

  findByCategory(seasonSlug: string, categorySlug: string, pageParams?: QueryPageParams): Observable<Page<Participant>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params => this.http.get<ParticipantDto[]>(resourceUrl(seasonSlug), {
          params: { ...params, category: categorySlug },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => participantMapper.toModel(dto)));
  }

  findByCategories(seasonSlug: string, categorySlugs: string[], pageParams?: QueryPageParams): Observable<Page<Participant>> {
    return of(pageParams)
      .pipe(
        mapPageToHttpParams(),
        mergeMap(params => this.http.get<ParticipantDto[]>(resourceUrl(seasonSlug), {
          params: { ...params, categories: categorySlugs?.length ? categorySlugs.join(',') : undefined },
          observe: 'response' })),
        mapHttpResponseToPage(pageParams),
        mapPageItems(dto => participantMapper.toModel(dto)));
  }

  get(seasonSlug: string, slug: string): Observable<Participant> {
    return this.http.get<ParticipantDto>(`${resourceUrl(seasonSlug)}/${slug}`)
      .pipe(map(dto => participantMapper.toModel(dto)));
  }

  save(seasonSlug: string, model: Participant, saveParams?: SaveParams): Observable<Participant> {
    const params = toHttpParams(saveParams);
    return of(model)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(model => participantMapper.toDto(model)),
        mergeMap(dto => dto.slug
          ? this.http.put<ParticipantDto>(`${resourceUrl(seasonSlug)}/${dto.slug}`, dto, { params })
          : this.http.post<ParticipantDto>(resourceUrl(seasonSlug), dto, { params })),
        map(dto => participantMapper.toModel(dto)));
  }

  delete(seasonSlug: string, slug: string): Observable<void> {
    return this.http.delete<void>(`${resourceUrl(seasonSlug)}/${slug}`);
  }

}
