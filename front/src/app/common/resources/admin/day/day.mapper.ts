import { Day, DayDto } from '@resources/admin/day/day.model';
import * as moment from 'moment';

class DayMapper {

  toModel(dto: DayDto): Day {
    if (!dto) {
      return;
    }
    return {
      slug: dto.slug,
      index: dto.index,
      date: dto.date && moment(dto.date)
    };
  }

  toDto(model: Day): DayDto {
    if (!model) {
      return;
    }
    return {
      ...model.slug && {
        slug: model.slug
      },
      ...model.hasOwnProperty('index') && {
        index: model.index
      },
      ...model.hasOwnProperty('date') && {
        date: model.date?.format()
      }
    };
  }

}

export const dayMapper = new DayMapper();
