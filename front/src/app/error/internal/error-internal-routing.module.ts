import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ErrorInternalComponent } from './error-internal.component';

const routes: Routes = [
  {
    path: '',
    component: ErrorInternalComponent,
    data: {
      meta: {
        title: 'error.internal.seo.title',
        description: 'error.internal.seo.description',
        'twitter:title': 'error.internal.seo.title',
        'twitter:description': 'error.internal.seo.description'
      }
    }
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorInternalRoutingModule { }
