import { ScrollingModule } from '@angular/cdk/scrolling';
import { registerLocaleData } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import localeFrExtra from '@angular/common/locales/extra/fr';
import localeFr from '@angular/common/locales/fr';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_PAGINATOR_DEFAULT_OPTIONS, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { CookiesModule } from '@common/cookies/cookies.module';
import { HttpInterceptorModule } from '@common/http-interceptor/http-interceptor.module';
import { BREAKPOINTS } from '@common/responsive/responsive.config';
import { TranslatePaginator } from '@common/translate/translate-paginator';
import { LayoutModule } from '@components/layout/layout.module';
import { MetaLoader, MetaModule, MetaStaticLoader } from '@ngx-meta/core';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Angulartics2Module } from 'angulartics2';

import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

registerLocaleData(localeFr, 'fr-FR', localeFrExtra);

export function appInitializerFactory(translate: TranslateService) {
  translate.setDefaultLang(environment.i18n.defaultLanguage.code);
  return () => translate.use(environment.i18n.defaultLanguage.code).toPromise();
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/');
}

export function MetaFactory(translate: TranslateService): MetaLoader {
  return new MetaStaticLoader({
    // ES6 template to handle keys that are not string (e.g. number)
    callback: (key: string) => translate.stream(`${key}`),
    pageTitlePositioning: environment.seo.pageTitlePositioning,
    pageTitleSeparator: environment.seo.pageTitleSeparator,
    applicationName: environment.application.name,
    applicationUrl: environment.application.url,
    defaults: {
      title: environment.seo.default.title,
      description: environment.seo.default.description,
      'og:site_name': environment.application.name,
      'og:type': 'website',
      'og:locale': environment.i18n.defaultLanguage.culture,
      'og:locale:alternate': environment.i18n.availableLanguages
        .map(({ culture }) => culture).toString(),
      'og:image': environment.seo.default.image.url,
      'og:image:url': environment.seo.default.image.url,
      'og:image:alt': environment.seo.default.image.description,
      'og:image:width': '512',
      'og:image:height': '512',
      'twitter:card': 'summary',
      'twitter:title': environment.seo.default.title,
      'twitter:description': environment.seo.default.description,
      'twitter:image': environment.seo.default.image.url
    }
  });
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MetaModule.forRoot({
      provide: MetaLoader,
      useFactory: MetaFactory,
      deps: [TranslateService]
    }),
    ScrollingModule,
    FlexLayoutModule.withConfig({}, BREAKPOINTS),
    Angulartics2Module.forRoot(),
    MatSnackBarModule,
    MatMomentDateModule,
    HttpInterceptorModule,
    AppRoutingModule,
    CookiesModule,
    LayoutModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFactory,
      deps: [TranslateService],
      multi: true
    },
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'fr-FR'
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'DD/MM/YYYY'
        },
        display: {
          dateInput: 'DD/MM/YYYY',
          monthYearLabel: 'MMMM YYYY',
          dateA11yLabel: 'DD/MM/YYYY',
          monthYearA11yLabel: 'MMMM YYYY'
        }
      }
    },
    {
      provide: MAT_PAGINATOR_DEFAULT_OPTIONS,
      useValue: {
        showFirstLastButtons: true
      }
    },
    {
      provide: MatPaginatorIntl,
      useFactory: (translateService: TranslateService) => new TranslatePaginator(translateService).getPaginatorIntl(),
      deps: [TranslateService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
