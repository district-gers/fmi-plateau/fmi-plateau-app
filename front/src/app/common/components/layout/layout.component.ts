import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Event, NavigationStart, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';


import { LayoutConfig } from './layout.config';
import { LayoutService } from './layout.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: [
    './layout.component.scss',
    './layout.component.mobile.scss'
  ]
})
export class LayoutComponent implements OnInit, OnDestroy {

  @ViewChild(MatSidenav) sidenav: MatSidenav;
  config$: Observable<LayoutConfig>;

  private destroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private layout: LayoutService) {
  }

  ngOnInit(): void {
    this.config$ = this.layout.getConfig();

    this.router.events
      .pipe(
        takeUntil(this.destroy$),
        filter((event: Event) => event instanceof NavigationStart),
      )
      .subscribe(() => this.sidenav.close());
  }

  ngOnDestroy(): void {
    this.destroy$.complete();
  }
}
