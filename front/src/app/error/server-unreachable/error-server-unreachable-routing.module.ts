import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ErrorServerUnreachableComponent } from './error-server-unreachable.component';

const routes: Routes = [
  {
    path: '',
    component: ErrorServerUnreachableComponent,
    data: {
      meta: {
        title: 'error.serverUnreachable.seo.title',
        description: 'error.serverUnreachable.seo.description',
        'twitter:title': 'error.serverUnreachable.seo.title',
        'twitter:description': 'error.serverUnreachable.seo.description'
      }
    }
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorServerUnreachableRoutingModule { }
