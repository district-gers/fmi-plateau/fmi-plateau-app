export interface EducatorDto {
  slug?: string;
  firstName: string;
  lastName: string;
  code: number;
  clubSlug: string;
  clubName?: string;
  clubShortName?: string;
  clubLogoUrl?: string;
}

export interface Educator {
  slug?: string;
  firstName: string;
  lastName: string;
  code: number;
  club: string;
  clubName?: string;
  clubShortName?: string;
  clubLogoUrl?: string;
}
