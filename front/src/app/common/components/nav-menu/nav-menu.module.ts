import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { RouterModule } from '@angular/router';
import { SeasonSelectorModule } from '@components/season-selector/season-selector.module';
import { TranslateModule } from '@ngx-translate/core';
import { UserHasRightModule } from '@pipes/user-has-right/user-has-right.module';
import { UserIsInChargeOfPlateauModule } from '@pipes/user-is-in-charge-of-plateau/user-is-in-charge-of-plateau.module';
import { Angulartics2Module } from 'angulartics2';

import { NavMenuComponent } from './nav-menu.component';



@NgModule({
  declarations: [NavMenuComponent],
  exports: [
    NavMenuComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    Angulartics2Module,
    MatIconModule,
    TranslateModule,
    UserHasRightModule,
    MatDividerModule,
    MatMenuModule,
    UserIsInChargeOfPlateauModule,
    MatExpansionModule,
    SeasonSelectorModule
  ]
})
export class NavMenuModule { }
