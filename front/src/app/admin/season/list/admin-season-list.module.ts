import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DialogConfirmationModule } from '@components/dialog-confirmation/dialog-confirmation.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';

import { AdminSeasonListRoutingModule } from './admin-season-list-routing.module';
import { AdminSeasonListComponent } from './admin-season-list.component';


@NgModule({
  declarations: [AdminSeasonListComponent],
  imports: [
    CommonModule,
    AdminSeasonListRoutingModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    TranslateModule,
    PageFormModule,
    MatTooltipModule,
    DialogConfirmationModule,
    MatPaginatorModule,
    MatSortModule
  ]
})
export class AdminSeasonListModule { }
