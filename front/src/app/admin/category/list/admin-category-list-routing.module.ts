import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminCategoryListComponent } from './admin-category-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminCategoryListComponent,
    data: {
      meta: {
        title: 'admin.category.list.seo.title',
        description: 'admin.category.list.seo.description',
        'twitter:title': 'admin.category.list.seo.title',
        'twitter:description': 'admin.category.list.seo.description'
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminCategoryListRoutingModule { }
