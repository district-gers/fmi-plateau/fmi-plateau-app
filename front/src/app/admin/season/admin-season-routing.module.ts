import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./list/admin-season-list.module').then(module => module.AdminSeasonListModule)
      },
      {
        path: 'create',
        loadChildren: () => import('./edit/admin-season-edit.module').then(module => module.AdminSeasonEditModule)
      },
      {
        path: ':slug/update',
        loadChildren: () => import('./edit/admin-season-edit.module').then(module => module.AdminSeasonEditModule)
      },
      {
        path: ':slug/duplicate',
        loadChildren: () => import('./edit/admin-season-edit.module').then(module => module.AdminSeasonEditModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminSeasonRoutingModule { }
