import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { TeamNameModule } from '@pipes/team-name/team-name.module';
import { Angulartics2Module } from 'angulartics2';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { FmiTeamMemberModule } from '../member/fmi-team-member.module';

import { FmiTeamItemComponent } from './fmi-team-item.component';



@NgModule({
  declarations: [FmiTeamItemComponent],
  exports: [
    FmiTeamItemComponent
  ],
  imports: [
    CommonModule,
    MatExpansionModule,
    LazyLoadImageModule,
    ReactiveFormsModule,
    FmiTeamMemberModule,
    TranslateModule,
    DragDropModule,
    Angulartics2Module,
    MatIconModule,
    MatButtonModule,
    TeamNameModule
  ]
})
export class FmiTeamItemModule { }
