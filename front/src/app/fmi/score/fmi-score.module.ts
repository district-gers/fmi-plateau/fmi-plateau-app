import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { AsyncButtonModule } from '@components/async-button/async-button.module';
import { PageFormModule } from '@components/page-form/page-form.module';
import { TranslateModule } from '@ngx-translate/core';
import { TeamNameModule } from '@pipes/team-name/team-name.module';
import { Angulartics2OnModule } from 'angulartics2';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { FmiScoreRoutingModule } from './fmi-score-routing.module';
import { FmiScoreComponent } from './fmi-score.component';


@NgModule({
  declarations: [FmiScoreComponent],
  imports: [
    CommonModule,
    FmiScoreRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
    MatIconModule,
    PageFormModule,
    TeamNameModule,
    Angulartics2OnModule,
    LazyLoadImageModule,
    AsyncButtonModule
  ]
})
export class FmiScoreModule { }
