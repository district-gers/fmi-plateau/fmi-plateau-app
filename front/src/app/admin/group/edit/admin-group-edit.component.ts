import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { emptyPage, Page } from '@common/pagination/pagination';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { Link } from '@common/routing/link';
import { RouterExtService } from '@common/routing/router-ext.service';
import { MetaService } from '@ngx-meta/core';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Competition } from '@resources/admin/competition/competition.model';
import { CompetitionService } from '@resources/admin/competition/competition.service';
import { DayService } from '@resources/admin/day/day.service';
import { Division } from '@resources/admin/division/division.model';
import { DivisionService } from '@resources/admin/division/division.service';
import { Group } from '@resources/admin/group/group.model';
import { GroupService } from '@resources/admin/group/group.service';
import { Team } from '@resources/admin/team/team.model';
import { TeamService } from '@resources/admin/team/team.service';
import { controlValueChange$ } from '@utils/form.util';
import { tapUniqueItem } from '@utils/rxjs.util';
import { isEqual } from 'lodash';
import { combineLatest, ConnectableObservable, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, finalize, map, publishBehavior, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminEditComponent } from '../../common/abstract-admin-edit.component';
import { EditMode } from '../../common/edit-mode.enum';

const COUNT_MAX_OFFSET = 10;

@Component({
  selector: 'app-admin-group-edit',
  templateUrl: './admin-group-edit.component.html',
  styleUrls: ['./admin-group-edit.component.scss']
})
export class AdminGroupEditComponent extends AbstractAdminEditComponent<Group> implements OnInit {

  formGroup: FormGroup;
  metaLabelsPrefix = 'admin.group.edit.seo';
  minDayCount = 0;
  maxDayCount = COUNT_MAX_OFFSET;

  categories$: Observable<Page<Category>>;
  competitions$: Observable<Page<Competition>>;
  divisions$: Observable<Page<Division>>;
  teams$: Observable<Page<Team>>;
  screenSize$: Observable<ScreenSize>;

  constructor(
    router: Router,
    route: ActivatedRoute,
    snackBar: MatSnackBar,
    translate: TranslateService,
    meta: MetaService,
    routerExt: RouterExtService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private responsive: ResponsiveService,
    private group: GroupService,
    private category: CategoryService,
    private competition: CompetitionService,
    private division: DivisionService,
    private day: DayService,
    private team: TeamService) {
    super(router, route, snackBar, translate, meta, routerExt);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.screenSize$ = this.responsive.size$;

    this.formGroup = this.fb.group({
      slug: [],
      name: ['', Validators.required],
      index: [1, [Validators.required, Validators.min(1)]],
      dayCount: [1, Validators.max(COUNT_MAX_OFFSET)],
      teams: [[], [Validators.required, Validators.minLength(1)]],
      category: [undefined, Validators.required],
      competition: [undefined, Validators.required],
      division: [undefined, Validators.required]
    });
    this.formGroup.controls.teams.disable();
    this.formGroup.controls.competition.disable();
    this.formGroup.controls.division.disable();
    if (this.mode === EditMode.UPDATE) {
      this.formGroup.controls.category.disable();
    }

    const season$ = this.currentSeason.get$().pipe(take(1));

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)),
        tapUniqueItem(({ slug: category }) => category !== this.formGroup.getRawValue().category
          && this.formGroup.patchValue({ category })),
        finalize(() => this.loading.category = false));

    this.competitions$ = combineLatest([ season$, controlValueChange$<string>(this.formGroup.controls.category) ])
      .pipe(
        map(([season, categorySlug]) => ({ season, categorySlug })),
        filter(({ season, categorySlug }) => season && !!categorySlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.competition = true),
        switchMap(({ season, categorySlug }) => this.competition.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.competition = false))),
        tapUniqueItem(({ slug: competition }) => competition !== this.formGroup.getRawValue().competition
          && this.formGroup.patchValue({ competition })),
        finalize(() => this.loading.competition = false));

    this.divisions$ = combineLatest([ season$, controlValueChange$<string>(this.formGroup.controls.competition) ])
      .pipe(
        map(([season, competitionSlug]) => ({ season, competitionSlug })),
        filter(({ season, competitionSlug }) => season && !!competitionSlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.division = true),
        switchMap(({ season, competitionSlug }) => this.division.find(season.slug, competitionSlug)
          .pipe(finalize(() => this.loading.division = false))),
        tapUniqueItem(({ slug: division }) => division !== this.formGroup.getRawValue().division
          && this.formGroup.patchValue({ division })),
        finalize(() => this.loading.division = false));

    this.teams$ = combineLatest([ season$, controlValueChange$<string>(this.formGroup.controls.category) ])
      .pipe(
        map(([season, categorySlug]) => ({ season, categorySlug })),
        filter(({ season, categorySlug }) => season && !!categorySlug),
        distinctUntilChanged(isEqual),
        tap(() => this.loading.teams = true),
        switchMap(({ season, categorySlug }) => this.team.findByCategory(season.slug, categorySlug)
          .pipe(finalize(() => this.loading.teams = false))),
        finalize(() => this.loading.teams = false));

    this.route.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe(({ competition, division }) => this.formGroup.patchValue({ competition, division }));

    combineLatest([ season$, this.route.queryParams ])
      .pipe(
        takeUntil(this.destroy$),
        map(([season, { competition }]) => ({ season, competitionSlug: competition })),
        filter(({ season, competitionSlug }) => season && !!competitionSlug),
        distinctUntilChanged(isEqual),
        switchMap(({ season, competitionSlug }) => this.competition.get(season.slug, competitionSlug)))
      .subscribe(({ category }) => this.formGroup.patchValue({ category }));

    const groups$: ConnectableObservable<Page<Group>> = combineLatest([
      season$.pipe(map(season => season.slug)),
      combineLatest([
        controlValueChange$<string>(this.formGroup.controls.competition),
        controlValueChange$<string>(this.formGroup.controls.division)
      ])
        .pipe(
          map(([competition, division ]) => ({ competition, division })),
          filter(({ competition, division }) => !!(competition && division)),
          distinctUntilChanged(isEqual))
    ])
      .pipe(
        takeUntil(this.destroy$),
        map(([season, value]) => ({ ...value, season })),
        switchMap(({ season, competition, division }) => this.group
          .find(season, competition, division, { sort: { field: 'index', direction: 'asc' } })),
        filter(({ count }) => !!count))
      .pipe(publishBehavior(emptyPage)) as ConnectableObservable<Page<Group>>;
    groups$.connect();

    this.formGroup.controls.index.setAsyncValidators(control => groups$
      .pipe(
        take(1),
        map(({ items }) => items
          .filter(group => group.slug !== this.formGroup.value.slug)
          .map(group => group.index)
          .includes(control.value)
          ? { unicity: { items, current: control.value } }
          : undefined)));

    groups$
      .pipe(
        takeUntil(this.destroy$),
        filter(() => this.mode !== EditMode.UPDATE),
        map(({ items }): Group => items[items.length - 1]),
        map(group => (group?.index || 0) + 1))
      .subscribe(index => this.formGroup.patchValue({
        index,
        name: `Poule ${String.fromCharCode(65 + index - 1)}`
      }));

    super.itemSlug$()
      .pipe(
        switchMap((slug: string) => combineLatest([
          this.currentSeason.get$().pipe(take(1)),
          controlValueChange$<string>(this.formGroup.controls.competition),
          controlValueChange$<string>(this.formGroup.controls.division),
          of(slug)
        ])),
        map(([season, competitionSlug, divisionSlug, slug]) => ({ season, competitionSlug, divisionSlug, slug })),
        filter(({ season, competitionSlug, divisionSlug }) => season && !!competitionSlug && !!divisionSlug),
        distinctUntilChanged(isEqual),
        switchMap(({ season, competitionSlug, divisionSlug, slug }) => combineLatest([
          this.group.get(season.slug, competitionSlug, divisionSlug, slug),
          this.day.find(season.slug, competitionSlug, divisionSlug, slug)
        ])))
      .subscribe(([group, days]) => {
        this.formGroup.patchValue({
          slug: this.mode === EditMode.UPDATE ? group.slug : undefined,
          name: group.name,
          index: group.index,
          teams: group.teams
            ?.map(team => ({
              slug: team.slug,
              name: team.name,
              shortName: team.shortName,
              logoUrl: team.logoUrl,
            })),
          dayCount: days.count
        });
        this.formGroup.controls.dayCount.setValidators([
          Validators.min(days.count),
          Validators.max(days.count + COUNT_MAX_OFFSET)
        ]);
        this.minDayCount = days.count;
        this.maxDayCount = days.count + COUNT_MAX_OFFSET;
        this.formGroup.controls.index.updateValueAndValidity();
      });
  }

  protected save$(): Observable<[Group, Link]> {
    const value = this.formGroup.getRawValue();
    const { category, competition, division, dayCount } = this.formGroup.getRawValue();
    return this.group.save(this.currentSeason.get().slug, competition, division, {
      slug: value.slug,
      name: value.name,
      index: value.index,
      teams: value.teams?.map(({ slug }) => ({ slug }))
    }, { dayCount })
      .pipe(
        tap(() => this.previousRouteCommands = this.getPreviousRouteCommands({ category, competition, division })),
        map(group => [group, {
          commands: ['days'],
          queryParams: { category, competition, division, group: group.slug },
          label: this.translate.instant('admin.group.edit.action.seeDays')
        }]));
  }

  categoryChange(): void {
    this.formGroup.patchValue({
      competition: undefined,
      division: undefined
    });
    this.formGroup.controls.division.disable();
  }

  competitionChange(): void {
    this.formGroup.patchValue({
      division: undefined
    });
  }

}
