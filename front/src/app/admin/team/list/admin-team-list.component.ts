import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentSeasonService } from '@common/current-season.service';
import { Page, PageParams } from '@common/pagination/pagination';
import { filterPageParams } from '@common/pagination/pagination.util';
import { ResponsiveService, ScreenSize } from '@common/responsive/responsive.service';
import { DialogConfirmationService } from '@components/dialog-confirmation/dialog-confirmation.service';
import { TranslateService } from '@ngx-translate/core';
import { Category } from '@resources/admin/category/category.model';
import { CategoryService } from '@resources/admin/category/category.service';
import { Participant } from '@resources/admin/participant/participant.model';
import { ParticipantService } from '@resources/admin/participant/participant.service';
import { Team } from '@resources/admin/team/team.model';
import { TeamService } from '@resources/admin/team/team.service';
import { tapUniqueItem } from '@utils/rxjs.util';
import { isEqual } from 'lodash';
import { combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, finalize, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';

import { AbstractAdminListComponent } from '../../common/abstract-admin-list.component';

interface FilterPageParams extends PageParams {
  category?: string;
  participant?: string;
}

@Component({
  selector: 'app-admin-team-list',
  templateUrl: './admin-team-list.component.html',
  styleUrls: [
    './admin-team-list.component.scss',
    './admin-team-list.component.responsive.scss'
  ]
})
export class AdminTeamListComponent extends AbstractAdminListComponent<Team> implements OnInit {

  columns = ['logo', 'participantName', 'participantShortName', 'index', 'categoryName', 'action'];
  screenSize$: Observable<ScreenSize>;

  categories$: Observable<Page<Category>>;
  participants$: Observable<Page<Participant>>;

  formGroup: FormGroup;

  constructor(
    route: ActivatedRoute,
    router: Router,
    translate: TranslateService,
    dialogConfirmation: DialogConfirmationService,
    responsive: ResponsiveService,
    private fb: FormBuilder,
    private currentSeason: CurrentSeasonService,
    private team: TeamService,
    private category: CategoryService,
    private participant: ParticipantService) {
    super(route, router, translate, dialogConfirmation, responsive);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.screenSize$ = this.responsive.size$;

    const season$ = this.currentSeason.get$();

    this.categories$ = season$
      .pipe(
        tap(() => this.loading.category = true),
        switchMap(season => this.category.find(season.slug)
          .pipe(finalize(() => this.loading.category = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().category && this.categoryChange(slug)),
        finalize(() => this.loading.category = false));

    this.participants$ = combineLatest([ season$, this.route.queryParams ])
      .pipe(
        tap(() => this.loading.participant = true),
        switchMap(([season, { category }]) => (category
          ? this.participant.findByCategory(season.slug, category)
          : this.participant.find(season.slug))
          .pipe(finalize(() => this.loading.participant = false))),
        tapUniqueItem(({ slug }) => slug !== this.formGroup.getRawValue().category && this.participantChange(slug)),
        finalize(() => this.loading.participant = false));

    this.formGroup = this.fb.group({
      category: [],
      participant: [],
    });

    this.route.queryParams
      .pipe(takeUntil(this.destroy$), map(({ category, participant }) => ({ category, participant })))
      .subscribe(({ category, participant }) => this.formGroup.patchValue({ category, participant }));
  }

  categoryChange(category: string) {
    this.refresh$.next({ category, participant: undefined, page: undefined } as FilterPageParams);
  }

  participantChange(participant: string) {
    this.refresh$.next({ participant, page: undefined } as FilterPageParams);
  }

  protected delete$(item: Team): Observable<void> {
    return this.team.delete(this.currentSeason.get().slug, item.slug);
  }

  protected find$(params: PageParams): Observable<Page<Team>> {
    return combineLatest([
      this.currentSeason.get$().pipe(take(1)),
      this.route.queryParams.pipe(map(({ category, participant }) => ({ category, participant })))
    ])
      .pipe(
        map(([season, filterParams]) => ({ season, ...filterParams })),
        map(({ season, ...filterParams }) => Object.entries(filterParams)
          .filter(([, value]) => !!value)
          .reduce((acc, [key, value]) => ({ ...acc, [key]: value}), { season })),
        distinctUntilChanged(isEqual),
        switchMap(({ season, ...filterParams }) => this.team.find(season.slug, { ...filterPageParams(params), ...filterParams })));
  }

  protected getItemLabel(item: Team): string {
    return [
      item.name,
      item.index,
      item.categoryName
    ].join(' ');
  }

  protected getItemSlug(item: Team): string {
    return item.slug;
  }
}
